
// Class definition

const KTUsersListDatatable = (function () {
  // variables
  let datatable;

  // init
  const init = function () {
    // init the datatables. Learn more: https://keenthemes.com/keen/?page=docs&section=datatable
    datatable = $('#kt_apps_user_list_datatable').KTDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: '/admin/users/list',
        pageSize: 10,
      },
      // layout definition
      layout: {
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false, // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [{
        field: 'firstName',
        title: 'Nom',
        //width: 200,
        // callback function support for column rendering
        template(data) {
          return `${data.firstName} ${data.lastName}`;
        }
      }, 
      {
        field: 'username',
        title: "Nom d'utilisateur",
      },
      {
        field: 'email',
        title: 'Email',
      },
      {
        field: 'phoneNumber',
        title: 'Téléphone',
      },
      {
        field: 'job',
        title: 'Fonction',
      },
      {
        field: 'activated',
        title: 'Statut',
        autoHide: false,
        template: function(data) {
          if (data.activated) {
            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill font-weight-bolder">Activé</span>';
          }
          
          return '<span class="kt-badge kt-badge--metal kt-badge--inline kt-badge--pill font-weight-bolder">Désactivé</span>';
        }
      }, 
      {
        field: 'createdAtFormatted',
        title: 'Date de création',
        type: 'date',
        format: 'DD/MM/YYYY',
        autoHide: false
      },
      {
        field: 'Actions',
        title: 'Actions',
        sortable: false,
        width: 110,
        autoHide: false,
        textAlign: 'center',
        overflow: 'visible',
        template: function(data) {
          return '<a href="/admin/users/edit/' + data._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Modifier">' +
            '<i class="la la-edit"></i></a>';
        },
      }
      ]
    });
  };

  const updateTotal = function () {
    datatable.on('kt-datatable--on-layout-updated', () => {
      $('#kt_subheader_total').html('Total: ' + datatable.getTotalRows());
    });
  };

  return {
    // public functions
    init() {
      init();
      updateTotal();
    },
  };
}());

// On document ready
KTUtil.ready(() => {
  KTUsersListDatatable.init();
});
