
// Class definition

const KTMenusCategoriesListDatatable = (function () {
  // variables
  let datatable;

  // init
  const init = function () {
    // init the datatables. Learn more: https://keenthemes.com/keen/?page=docs&section=datatable
    datatable = $('#kt_apps_menus_categories_list_datatable').KTDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: '/admin/menus/categories/list',
        pageSize: 10,
      },
      // layout definition
      layout: {
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false, // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [{
        field: 'title',
        title: 'Titre',
        template: function(data) {
          return '<span class="font-weight-bolder">' + data.title + '</span>';
        }
      }, 
      {
        field: 'subtitle',
        title: "Sous-titre",
      },
      {
        field: 'photo',
        title: 'Photo',
        template: function(data) {
          if (data.photo) {
            return '<div class="kt-media"><img src="' + data.photo + '" class="" alt="photo"></img></div>';
          }
        }
      },
      {
        field: 'order',
        title: 'Ordre',
      },
      {
        field: 'activated',
        title: 'Statut',
        autoHide: false,
        template: function(data) {
          if (data.activated) {
            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill font-weight-bolder">Activé</span>';
          }
          
          return '<span class="kt-badge kt-badge--metal kt-badge--inline kt-badge--pill font-weight-bolder">Désactivé</span>';
        }
      },
      {
        field: 'Actions',
        title: 'Actions',
        sortable: false,
        width: 110,
        autoHide: false,
        textAlign: 'center',
        overflow: 'visible',
        template: function(data) {
          return '<a href="/admin/menus/categories/edit/' + data._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Modifier">' +
            '<i class="la la-edit"></i></a>';
        },
      }
      ]
    });
  };

  const updateTotal = function () {
    datatable.on('kt-datatable--on-layout-updated', () => {
      $('#kt_subheader_total').html('Total: ' + datatable.getTotalRows());
    });
  };

  return {
    // public functions
    init() {
      init();
      updateTotal();
    },
  };
}());

// On document ready
KTUtil.ready(() => {
  KTMenusCategoriesListDatatable.init();
});
