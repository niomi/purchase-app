// Class definition
const KTMenusDishesAdd = (function () {
  let validator;
  let formEl;

  const initValidation = function () {
    validator = formEl.validate({
      rules: {
        title: {
          required: true
        },
        type: {
          required: true
        },
      },
      messages: {
        title: {
          required: "Ce champ est obligatoire"
        },
        order: {
          required: "Ce champ est obligatoire",
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const dishId = document.getElementById('dishId').value;
        formEl.ajaxSubmit({
          url: '/admin/menus/dishes/update/' + dishId,
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Les modifications ont été enregistrées!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formEl = $('#kt_form');

      initValidation();
      initSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusDishesAdd.init();
});
