// Class definition
const KTMenusItemsCategoryCombination = (function () {
  // Base element
  let validator;
  let formEl;
  let modalFormAddMeal;
  let modalFormEditMealCollection;

  const initValidation = function () {
    validator = formEl.validate({
      // Validation rules
      rules: {
        order: {
          required: true,
          number: true
        },
        title: {
          required: true,
        },
      },
      messages: {
        order: {
          required: "Ce champ est obligatoire",
          number: "Veuillez saisir un chiffre"
        },
        title: {
          required: "Ce champ est obligatoire",
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const menuItemId = document.getElementById('menuItemId').value;
        const menuItemCategoryId = document.getElementById('menuItemCategoryId').value;
        const menuCategoryItemId = document.getElementById('menuCategoryItemId').value;
        const mealId = document.getElementById('mealId').value;

        formEl.ajaxSubmit({
          url: '/admin/menus/items/' + menuItemId + '/categories/' + menuItemCategoryId + '/offer/edit/' + menuCategoryItemId + '/combination/edit/' + mealId,
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Les modifications ont été enregistrées!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });
            }
          }
        });
      }
    });
  };

  const modalFormAddMealSubmit = function () {
    const btn = modalFormAddMeal.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      const menuItemId = document.getElementById('menuItemId').value;
      const menuItemCategoryId = document.getElementById('menuItemCategoryId').value;
      const menuCategoryItemId = document.getElementById('menuCategoryItemId').value;
      const mealId = document.getElementById('mealId').value;

      modalFormAddMeal.ajaxSubmit({
        url: '/admin/menus/items/' + menuItemId + '/categories/' + menuItemCategoryId + '/offer/edit/' + menuCategoryItemId + '/combination/edit/' + mealId + '/dishes/add',
        type: 'POST',
        success: function (response, status, xhr, $form) {
          KTApp.unprogress(btn);
          
          if (response.error) {
            swal.fire({
              "title": "", 
              "text": response.error.message, 
              "type": "error",
              "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });
          } else {
            swal.fire({
              title: '',
              text: "Le plat a été ajouté",
              type: 'success',
              confirmButtonClass: 'btn btn-secondary'
            });

            $("#add-meal-modal").modal('hide');
            document.location.reload(true);
          }
        }
      });

    }); 
  };

  const modalFormEditMealSubmit = function () {
    const btn = modalFormEditMealCollection.find('button[type="submit"]');

    modalFormEditMealCollection.each(function( index ) {
      const dishId = $(this).find('input[name="dishId"]').val();

      $(this).find('button[type="submit"]').on('click', (e) => {
        e.preventDefault();

        const menuItemId = document.getElementById('menuItemId').value;
        const menuItemCategoryId = document.getElementById('menuItemCategoryId').value;
        const menuCategoryItemId = document.getElementById('menuCategoryItemId').value;
        const mealId = document.getElementById('mealId').value;

        modalFormEditMealCollection.ajaxSubmit({
          url: '/admin/menus/items/' + menuItemId + '/categories/' + menuItemCategoryId + '/offer/edit/' + menuCategoryItemId + '/combination/edit/' + mealId + '/dishes/edit/' + dishId,
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Le plat a été modifié",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });

              $(".add-meal-modal").modal('hide');
              document.location.reload(true);
            }
          }
        });

      }); 
    });

  };
  return {
    init() {
      formEl = $('#add-menu-items-category-offer-combination-form');
      modalFormAddMeal = $('#form-handle-dish');
      modalFormEditMealCollection = $('.form-handle-dish');

      initValidation();
      initSubmit();
      modalFormAddMealSubmit();
      modalFormEditMealSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusItemsCategoryCombination.init();
});
