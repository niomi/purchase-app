// Class definition
const KTMenusItemsCategoryItemAdd = (function () {
  // Base element
  let validator;
  let formEl;

  const initValidation = function () {
    validator = formEl.validate({
      // Validation rules
      rules: {
        order: {
          required: true,
        },
        title: {
          required: true,
        },
        type: {
          required: true,
        },
      },
      messages: {
        order: {
          required: "Ce champ est obligatoire",
        },
        title: {
          required: "Ce champ est obligatoire",
        },
        type: {
          required: "Ce champ est obligatoire",
        },
      },
    });
  };


  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const menuItemId = document.getElementById('menuItemId').value;
        const menuItemCategoryId = document.getElementById('menuItemCategoryId').value;
        const menuCategoryItemId = document.getElementById('menuCategoryItemId').value;

        formEl.ajaxSubmit({
          url: '/admin/menus/items/' + menuItemId + '/categories/' + menuItemCategoryId + '/offer/edit/' + menuCategoryItemId + '/combination/add',
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "La combinaison a été crée",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formEl = $('#add-menu-items-category-offer-combination-form');

      initSubmit();
      initValidation();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusItemsCategoryItemAdd.init();
});
