// Class definition
const KTMenusItemsEdit = (function () {
  let validator;
  let formAddCategory;
  let fieldOrder;
  let modalAddCategory;

  const initFieldOrder = function () {
    fieldOrder.TouchSpin({
      initval: 1,
      min: 1,
      buttondown_class: 'btn btn-secondary',
      buttonup_class: 'btn btn-secondary',
      verticalbuttons: false,
    });
  };

  const initValidation = function () {
    validator = formAddCategory.validate({
      rules: {
      },
      messages: {
      },
    });
  };

  const initSubmit = function () {
    const btn = formAddCategory.find('button[type="submit"]');
    console.log(btn);
    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const menuItemId = document.getElementById('menuItemId').value;
        formAddCategory.ajaxSubmit({
          url: '/admin/menus/items/' + menuItemId + '/categories/add',
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Les modifications ont été enregistrées!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });
              modalAddCategory.modal('hide');
              document.location.reload(true);
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formAddCategory = $('#add-category-form');
      fieldOrder = $('#order');
      modalAddCategory = $('#add-category-modal');

      initFieldOrder();
      initValidation();
      initSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusItemsEdit.init();
});
