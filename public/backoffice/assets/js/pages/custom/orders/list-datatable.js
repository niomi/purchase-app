
// Class definition

const KTOrdersListDatatable = (function () {
  // variables
  let datatable;

  // init
  const init = function () {
    // init the datatables. Learn more: https://keenthemes.com/keen/?page=docs&section=datatable
    datatable = $('#kt_apps_order_list_datatable').KTDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: '/admin/orders/list',
        pageSize: 10,
      },
      // layout definition
      layout: {
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false, // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [
        {
					field: '_id',
					title: '#',
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
        },	
      {
        field: 'Name',
        title: 'Nom',
        //width: 200,
        // callback function support for column rendering
        template(data) {
          return `${data.deliveryInfo.firstName} ${data.deliveryInfo.lastName}`;
        }
      },
      {
        field: 'deliveryInfo.phoneNumber',
        title: 'Téléphone',
      },
      {
        field: 'deliveryInfo.address',
        title: "Adresse",
      },
      {
        field: 'deliveryAtFormatted',
        title: 'Plage de livraison',
      },
      {
        field: 'totalQuantity',
        title: "Quantité",
      },
      {
        field: 'totalAmount',
        title: "Montant total",
        template(data) {
          return data.deliveryAmount + data.subTotalAmount;
        }
      },
      {
        field: 'status',
        title: "Statut de la transaction",
      },
      {
        field: 'payment.method',
        title: "Méthode de paiement",
      },
      {
        field: 'payment.provider',
        title: "Prestataire de paiement",
      },
      {
        field: 'paymentStatus',
        title: "Statut du paiement",
      }, 
      {
        field: 'createdAtFormatted',
        title: 'Date de création',
        type: 'date',
        format: 'DD/MM/YYYY',
        autoHide: false
      },
      {
        field: 'Actions',
        title: 'Actions',
        sortable: false,
        width: 110,
        autoHide: false,
        textAlign: 'center',
        overflow: 'visible',
        template: function(data) {
          return '<a href="/admin/orders/details/' + data._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Voir">' +
            '<i class="la la-eye"></i></a>';
        },
      }
      ]
    });
  };

  const updateTotal = function () {
    datatable.on('kt-datatable--on-layout-updated', () => {
      $('#kt_subheader_total').html('Total: ' + datatable.getTotalRows());
    });
  };

  return {
    // public functions
    init() {
      init();
      updateTotal();
    },
  };
}());

// On document ready
KTUtil.ready(() => {
  KTOrdersListDatatable.init();
});
