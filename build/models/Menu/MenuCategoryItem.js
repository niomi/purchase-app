"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var types = Object.freeze({
  SIMPLE: 'SIMPLE',
  COMBINED: 'COMBINED'
});
var linkTypes = Object.freeze({
  EXCLUSIVE: 'EXCLUSIVE',
  INCLUSIVE: 'INCLUSIVE',
  UNIQUE: 'UNIQUE'
});
var menuCategoryItemSchema = new _mongoose["default"].Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  photo: {
    type: String
  },
  price: {
    type: Number
  },
  isActive: {
    type: Boolean,
    "default": true,
    required: [true]
  },
  meals: [{
    title: {
      type: String
    },
    order: {
      type: Number
    },
    link: {
      type: String,
      "enum": Object.values(linkTypes),
      "default": 'UNIQUE',
      required: true
    },
    dishes: [{
      dish: {
        type: _mongoose["default"].Types.ObjectId,
        ref: 'Dish'
      },
      additionalPrice: {
        type: Number
      }
    }]
  }],
  type: {
    type: String,
    "enum": Object.values(types),
    "default": 'SIMPLE',
    required: true
  }
}, {
  timestamps: true
});

var MenuCategoryItemSchema = _mongoose["default"].model('MenuCategoryItem', menuCategoryItemSchema);

var _default = MenuCategoryItemSchema;
exports["default"] = _default;