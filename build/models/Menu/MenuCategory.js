"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var menuCategorySchema = new _mongoose["default"].Schema({
  title: {
    type: String,
    unique: true
  },
  subtitle: {
    type: String
  },
  photo: {
    type: String
  },
  order: {
    type: Number
  },
  isActive: {
    type: Boolean,
    "default": true,
    required: [true]
  },
  items: [{
    item: {
      type: _mongoose["default"].Types.ObjectId,
      ref: 'MenuCategoryItem'
    },
    order: {
      type: Number
    }
  }]
}, {
  timestamps: true
});

var MenuCategory = _mongoose["default"].model('MenuCategory', menuCategorySchema);

var _default = MenuCategory;
exports["default"] = _default;