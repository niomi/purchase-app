"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var statutes = Object.freeze({
  IN_PROGRESS: 'IN_PROGRESS',
  ORDERED: 'ORDERED',
  CANCELED: 'CANCELED'
});
var shoppingCartSchema = new _mongoose["default"].Schema({
  items: [{
    menuCategoryItemId: {
      type: String
    },
    quantity: {
      type: Number
    },
    amount: {
      type: Number
    },
    items: [{
      type: String
    }]
  }],
  status: {
    type: String,
    "enum": Object.values(statutes),
    "default": 'IN_PROGRESS',
    required: true
  },
  user: {
    type: String
  }
}, {
  timestamps: true
});

var ShoppingCartSchema = _mongoose["default"].model('ShoppingCart', shoppingCartSchema);

var _default = ShoppingCartSchema;
exports["default"] = _default;