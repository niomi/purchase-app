"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var deliveryAddressesTypes = Object.freeze({
  WORK: 'work',
  HOME: 'home'
});
var userSchema = new _mongoose["default"].Schema({
  email: {
    type: String,
    unique: true
  },
  phoneNumber: {
    type: String
  },
  password: String,
  passwordResetToken: String,
  emailVerificationToken: String,
  emailVerified: Boolean,
  facebook: String,
  google: String,
  tokens: Array,
  profile: {
    firstName: {
      type: String,
      required: true
    },
    lastName: String,
    gender: String
  },
  deliveryAddresses: [{
    type: {
      type: String,
      "enum": Object.values(deliveryAddressesTypes)
    },
    address: {
      type: String
    },
    additionalInformation: {
      type: String
    },
    companyName: {
      type: String
    }
  }]
}, {
  timestamps: true
});
/**
 * Password hash middleware.
 */

userSchema.pre('save', function save(next) {
  var user = this;

  if (!user.isModified('password')) {
    return next();
  }

  _bcrypt["default"].genSalt(10, function (err, salt) {
    if (err) {
      return next(err);
    }

    _bcrypt["default"].hash(user.password, salt, function (err, hash) {
      if (err) {
        return next(err);
      }

      user.password = hash;
      next();
    });
  });
});
/**
 * Helper method for validating user's password.
 */

userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  _bcrypt["default"].compare(candidatePassword, this.password, function (err, isMatch) {
    cb(err, isMatch);
  });
};

var User = _mongoose["default"].model('User', userSchema);

var _default = User;
exports["default"] = _default;