"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var types = Object.freeze({
  STARTER: 'STARTER',
  MAIN_COURSE: 'MAIN_COURSE',
  DESSERT: 'DESSERT',
  DRINK: 'DRINK',
  SPICE: 'SPICE',
  EXTRA: 'EXTRA'
});
var dishSchema = new _mongoose["default"].Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  photo: {
    type: String
  },
  ingredients: {
    type: String
  },
  quantity: {
    type: Number
  },
  allergens: {
    type: String
  },
  type: {
    type: String,
    "enum": Object.values(types),
    "default": 'MAIN_COURSE',
    required: true
  }
}, {
  timestamps: true
});

var DishSchema = _mongoose["default"].model('Dish', dishSchema);

var _default = DishSchema;
exports["default"] = _default;