"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkQuantityByDish = exports.addMeal = exports.findMenuCategoryItemByConditions = exports.updateMenuCategoryItem = exports.getMenuCategoryItems = exports.createMenuCategoryItem = void 0;

var _MenuCategoryItem = _interopRequireDefault(require("../../models/Menu/MenuCategoryItem"));

var _Dish = _interopRequireDefault(require("../../models/Dish"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var createMenuCategoryItem = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(inputData) {
    var menuCategoryItem;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            menuCategoryItem = new _MenuCategoryItem["default"]({
              title: inputData.title,
              description: inputData.description,
              photo: inputData.photo,
              price: inputData.price
            });
            _context.prev = 1;
            _context.next = 4;
            return menuCategoryItem.save();

          case 4:
            return _context.abrupt("return", _context.sent);

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](1);
            console.error(_context.t0);
            return _context.abrupt("return", _context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 7]]);
  }));

  return function createMenuCategoryItem(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.createMenuCategoryItem = createMenuCategoryItem;

var updateMenuCategoryItem = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(inputData) {
    var update, menuCategoryItem;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            update = {};

            if (inputData.title) {
              update = _objectSpread({}, update, {
                title: inputData.title
              });
            }

            if (inputData.description) {
              update = _objectSpread({}, update, {
                description: inputData.description
              });
            }

            if (inputData.photo) {
              update = _objectSpread({}, update, {
                photo: inputData.photo
              });
            }

            if (inputData.price) {
              update = _objectSpread({}, update, {
                price: inputData.price
              });
            }

            _context2.prev = 5;
            _context2.next = 8;
            return _MenuCategoryItem["default"].findOneAndUpdate({
              _id: inputData.id
            }, update, {
              "new": true
            });

          case 8:
            menuCategoryItem = _context2.sent;

            if (menuCategoryItem) {
              _context2.next = 11;
              break;
            }

            throw new Error("Impossible de trouver l'element de categorie de menu avec id: ".concat(inputData.id));

          case 11:
            return _context2.abrupt("return", menuCategoryItem);

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](5);
            console.error(_context2.t0);
            return _context2.abrupt("return", _context2.t0);

          case 18:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[5, 14]]);
  }));

  return function updateMenuCategoryItem(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.updateMenuCategoryItem = updateMenuCategoryItem;

var getMenuCategoryItems = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(limit, sortField, sortOrder) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _MenuCategoryItem["default"].find().sort(_defineProperty({}, sortField, sortOrder)).limit(limit);

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 6:
            _context3.prev = 6;
            _context3.t0 = _context3["catch"](0);
            console.error(_context3.t0);
            return _context3.abrupt("return", _context3.t0);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 6]]);
  }));

  return function getMenuCategoryItems(_x3, _x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getMenuCategoryItems = getMenuCategoryItems;

var findMenuCategoryItemByConditions = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(conditions) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _MenuCategoryItem["default"].findOne(conditions);

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 6:
            _context4.prev = 6;
            _context4.t0 = _context4["catch"](0);
            console.error(_context4.t0);
            return _context4.abrupt("return", _context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 6]]);
  }));

  return function findMenuCategoryItemByConditions(_x6) {
    return _ref4.apply(this, arguments);
  };
}();

exports.findMenuCategoryItemByConditions = findMenuCategoryItemByConditions;

var checkQuantityByDish = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(id, quantity) {
    var menuCategoryItem, result;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _MenuCategoryItem["default"].findById(id).populate({
              path: 'meals.dishes.dish',
              model: _Dish["default"]
            });

          case 2:
            menuCategoryItem = _context5.sent;

            if (menuCategoryItem) {
              _context5.next = 5;
              break;
            }

            throw new Error("Impossible de trouver l'\xE9l\xE9ment de categorie du menu avec id: ".concat(id));

          case 5:
            result = [];
            menuCategoryItem.meals.forEach(function (meal) {
              meal.dishes.forEach(function (dish) {
                var data = {
                  id: dish.dish.id,
                  title: dish.dish.title,
                  isQuantityEnough: dish.dish.quantity >= quantity,
                  quantityLeft: dish.dish.quantity
                };
                result.push(data);
              });
            });
            return _context5.abrupt("return", result);

          case 8:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function checkQuantityByDish(_x7, _x8) {
    return _ref5.apply(this, arguments);
  };
}();

exports.checkQuantityByDish = checkQuantityByDish;

var addMeal = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(id, meal) {
    var menuCategoryItem, meals;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return _MenuCategoryItem["default"].findById(id);

          case 2:
            menuCategoryItem = _context6.sent;

            if (menuCategoryItem) {
              _context6.next = 5;
              break;
            }

            throw new Error("Impossible de trouver l'\xE9l\xE9ment de categorie du menu avec id: ".concat(id));

          case 5:
            meals = menuCategoryItem.meals;
            meals.push({
              title: meal.title,
              order: meal.order,
              link: meal.link,
              dishes: meal.dishes
            });
            menuCategoryItem.meals = meals;
            _context6.prev = 8;
            _context6.next = 11;
            return menuCategoryItem.save();

          case 11:
            return _context6.abrupt("return", _context6.sent);

          case 14:
            _context6.prev = 14;
            _context6.t0 = _context6["catch"](8);
            console.error(_context6.t0);
            return _context6.abrupt("return", _context6.t0);

          case 18:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[8, 14]]);
  }));

  return function addMeal(_x9, _x10) {
    return _ref6.apply(this, arguments);
  };
}();

exports.addMeal = addMeal;