"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findUserByConditions = exports.updateUser = exports.getUsers = exports.createUser = exports.newUser = void 0;

var _User = _interopRequireDefault(require("../models/User"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var newUser = function newUser(inputData) {
  var user = new _User["default"]({
    email: inputData.email,
    phoneNumber: inputData.phoneNumber,
    password: inputData.password,
    profile: {
      firstName: inputData.firstName,
      lastName: inputData.lastName
    }
  });
  return user;
};

exports.newUser = newUser;

var createUser = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(inputData) {
    var user;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            user = newUser(inputData);
            _context.prev = 1;
            _context.next = 4;
            return user.save();

          case 4:
            return _context.abrupt("return", _context.sent);

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](1);
            console.error(_context.t0);
            return _context.abrupt("return", _context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 7]]);
  }));

  return function createUser(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.createUser = createUser;

var updateUser = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(inputData) {
    var update, user;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            update = {};

            if (inputData.lastName) {
              update = _objectSpread({}, update, {
                lastName: inputData.lastName
              });
            }

            if (inputData.firstName) {
              update = _objectSpread({}, update, {
                firstName: inputData.firstName
              });
            }

            if (inputData.email) {
              update = _objectSpread({}, update, {
                email: inputData.email
              });
            }

            if (inputData.phone) {
              update = _objectSpread({}, update, {
                phone: inputData.phone
              });
            }

            if (inputData.role) {
              update = _objectSpread({}, update, {
                role: inputData.role
              });
            }

            if (inputData.zones) {
              update = _objectSpread({}, update, {
                zones: inputData.zones
              });
            }

            if (inputData.buildings) {
              update = _objectSpread({}, update, {
                buildings: inputData.buildings
              });
            }

            _context2.prev = 8;
            _context2.next = 11;
            return _User["default"].findOneAndUpdate({
              _id: inputData.id
            }, update, {
              "new": true
            });

          case 11:
            user = _context2.sent;

            if (user) {
              _context2.next = 14;
              break;
            }

            throw new Error("Impossible de trouver le collaborateur avec id: ".concat(inputData.id));

          case 14:
            return _context2.abrupt("return", user);

          case 17:
            _context2.prev = 17;
            _context2.t0 = _context2["catch"](8);
            console.error(_context2.t0);
            return _context2.abrupt("return", _context2.t0);

          case 21:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[8, 17]]);
  }));

  return function updateUser(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.updateUser = updateUser;

var getUsers = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(limit, sortField, sortOrder) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _User["default"].find().sort(_defineProperty({}, sortField, sortOrder)).limit(limit);

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 6:
            _context3.prev = 6;
            _context3.t0 = _context3["catch"](0);
            console.error(_context3.t0);
            return _context3.abrupt("return", _context3.t0);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 6]]);
  }));

  return function getUsers(_x3, _x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getUsers = getUsers;

var findUserByConditions = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(conditions) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _User["default"].findOne(conditions);

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 6:
            _context4.prev = 6;
            _context4.t0 = _context4["catch"](0);
            console.error(_context4.t0);
            return _context4.abrupt("return", _context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 6]]);
  }));

  return function findUserByConditions(_x6) {
    return _ref4.apply(this, arguments);
  };
}();

exports.findUserByConditions = findUserByConditions;