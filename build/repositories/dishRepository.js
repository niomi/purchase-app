"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findDishByConditions = exports.updateDish = exports.getMenuCategories = exports.createDish = void 0;

var _Dish = _interopRequireDefault(require("../models/Dish"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var createDish = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(inputData) {
    var dish;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            dish = new _Dish["default"]({
              title: inputData.title,
              description: inputData.description,
              photo: inputData.photo,
              ingredients: inputData.ingredients,
              allergens: inputData.allergens,
              type: inputData.type
            });
            _context.prev = 1;
            _context.next = 4;
            return dish.save();

          case 4:
            return _context.abrupt("return", _context.sent);

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](1);
            console.error(_context.t0);
            return _context.abrupt("return", _context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 7]]);
  }));

  return function createDish(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.createDish = createDish;

var updateDish = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(inputData) {
    var update, dish;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            update = {};

            if (inputData.title) {
              update = _objectSpread({}, update, {
                title: inputData.title
              });
            }

            if (inputData.description) {
              update = _objectSpread({}, update, {
                description: inputData.description
              });
            }

            if (inputData.photo) {
              update = _objectSpread({}, update, {
                photo: inputData.photo
              });
            }

            if (inputData.ingredients) {
              update = _objectSpread({}, update, {
                ingredients: inputData.ingredients
              });
            }

            if (inputData.allergens) {
              update = _objectSpread({}, update, {
                allergens: inputData.allergens
              });
            }

            if (inputData.type) {
              update = _objectSpread({}, update, {
                type: inputData.type
              });
            }

            _context2.prev = 7;
            _context2.next = 10;
            return _Dish["default"].findOneAndUpdate({
              _id: inputData.id
            }, update, {
              "new": true
            });

          case 10:
            dish = _context2.sent;

            if (dish) {
              _context2.next = 13;
              break;
            }

            throw new Error("Impossible de trouver la categorie de menu avec id: ".concat(inputData.id));

          case 13:
            return _context2.abrupt("return", dish);

          case 16:
            _context2.prev = 16;
            _context2.t0 = _context2["catch"](7);
            console.error(_context2.t0);
            return _context2.abrupt("return", _context2.t0);

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[7, 16]]);
  }));

  return function updateDish(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.updateDish = updateDish;

var getMenuCategories = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(limit, sortField, sortOrder) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _Dish["default"].find().sort(_defineProperty({}, sortField, sortOrder)).limit(limit);

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 6:
            _context3.prev = 6;
            _context3.t0 = _context3["catch"](0);
            console.error(_context3.t0);
            return _context3.abrupt("return", _context3.t0);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 6]]);
  }));

  return function getMenuCategories(_x3, _x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getMenuCategories = getMenuCategories;

var findDishByConditions = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(conditions) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _Dish["default"].findOne(conditions);

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 6:
            _context4.prev = 6;
            _context4.t0 = _context4["catch"](0);
            console.error(_context4.t0);
            return _context4.abrupt("return", _context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 6]]);
  }));

  return function findDishByConditions(_x6) {
    return _ref4.apply(this, arguments);
  };
}();

exports.findDishByConditions = findDishByConditions;