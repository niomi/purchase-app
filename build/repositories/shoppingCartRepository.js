"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeItemToShoppingCartFromIndex = exports.addItemToShoppingCart = exports.createShoppingCart = void 0;

var _ShoppingCart = _interopRequireDefault(require("../models/ShoppingCart"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var createShoppingCart = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(newItemCart, shoppingCartId) {
    var shoppingCart, shoppingCartItems;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (shoppingCartId) {
              _context.next = 4;
              break;
            }

            shoppingCart = new _ShoppingCart["default"]();
            _context.next = 9;
            break;

          case 4:
            _context.next = 6;
            return _ShoppingCart["default"].findById(shoppingCartId);

          case 6:
            shoppingCart = _context.sent;

            if (shoppingCart) {
              _context.next = 9;
              break;
            }

            throw new Error("Impossible de trouver le panier: ".concat(shoppingCartId));

          case 9:
            shoppingCartItems = shoppingCart.items;
            shoppingCartItems.push(newItemCart);
            shoppingCart.items = shoppingCartItems;
            _context.prev = 12;
            _context.next = 15;
            return shoppingCart.save();

          case 15:
            return _context.abrupt("return", _context.sent);

          case 18:
            _context.prev = 18;
            _context.t0 = _context["catch"](12);
            console.error(_context.t0);
            return _context.abrupt("return", _context.t0);

          case 22:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[12, 18]]);
  }));

  return function createShoppingCart(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.createShoppingCart = createShoppingCart;

var addItemToShoppingCart = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(shoppingCartId, newItemCart, index) {
    var shoppingCart, shoppingCartItems;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _ShoppingCart["default"].findById(shoppingCartId);

          case 2:
            shoppingCart = _context2.sent;

            if (shoppingCart) {
              _context2.next = 5;
              break;
            }

            throw new Error("Impossible de trouver le panier: ".concat(shoppingCartId));

          case 5:
            shoppingCartItems = shoppingCart.items;
            shoppingCartItems.splice(index, 1, newItemCart);
            shoppingCart.items = shoppingCartItems;
            _context2.prev = 8;
            _context2.next = 11;
            return shoppingCart.save();

          case 11:
            return _context2.abrupt("return", _context2.sent);

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](8);
            return _context2.abrupt("return", _context2.t0);

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[8, 14]]);
  }));

  return function addItemToShoppingCart(_x3, _x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}();

exports.addItemToShoppingCart = addItemToShoppingCart;

var removeItemToShoppingCartFromIndex = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(shoppingCartId, index) {
    var shoppingCart, shoppingCartItems;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _ShoppingCart["default"].findById(shoppingCartId);

          case 2:
            shoppingCart = _context3.sent;

            if (shoppingCart) {
              _context3.next = 5;
              break;
            }

            throw new Error("Impossible de trouver le panier: ".concat(shoppingCartId));

          case 5:
            shoppingCartItems = shoppingCart.items;
            shoppingCartItems.splice(index, 1);
            shoppingCart.items = shoppingCartItems;
            _context3.prev = 8;

            if (!(!shoppingCart.items || shoppingCart.items.length < 1)) {
              _context3.next = 12;
              break;
            }

            _ShoppingCart["default"].deleteOne({
              _id: shoppingCart._id
            });

            return _context3.abrupt("return", {});

          case 12:
            _context3.next = 14;
            return shoppingCart.save();

          case 14:
            return _context3.abrupt("return", _context3.sent);

          case 17:
            _context3.prev = 17;
            _context3.t0 = _context3["catch"](8);
            return _context3.abrupt("return", _context3.t0);

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[8, 17]]);
  }));

  return function removeItemToShoppingCartFromIndex(_x6, _x7) {
    return _ref3.apply(this, arguments);
  };
}();

exports.removeItemToShoppingCartFromIndex = removeItemToShoppingCartFromIndex;