"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateBasketQuantity = exports.checkout = exports.checkMenuCategoryItemQuantity = exports.addToCart = exports.addMenuQuantity = void 0;

var _path = _interopRequireDefault(require("path"));

var _menuCategoryItemRepository = require("../../../repositories/Menu/menuCategoryItemRepository");

var _MenuCategoryItem = _interopRequireDefault(require("../../../models/Menu/MenuCategoryItem"));

var _Dish = _interopRequireDefault(require("../../../models/Dish"));

var _shoppingCartRepository = require("../../../repositories/shoppingCartRepository");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var templateDir = 'modules/front/cart/views';
/**
 * GET /checkout
 * Checkout page
 */

var checkout = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var shoppingCart, orders, subTotalAmount, _loop, i;

    return regeneratorRuntime.wrap(function _callee$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            shoppingCart = req.session.shoppingCart;

            if (req.user) {
              _context2.next = 3;
              break;
            }

            return _context2.abrupt("return", res.redirect('/login'));

          case 3:
            if (!(!shoppingCart || !shoppingCart.items || shoppingCart.items.length < 1)) {
              _context2.next = 5;
              break;
            }

            return _context2.abrupt("return", res.render(_path["default"].join(templateDir, 'checkout.twig'), {
              title: 'Livraison',
              user: req.user
            }));

          case 5:
            orders = [];
            subTotalAmount = 0;
            _loop = /*#__PURE__*/regeneratorRuntime.mark(function _loop(i) {
              var shoppingCartItem, menuCategoryItem, amount, items, order;
              return regeneratorRuntime.wrap(function _loop$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      shoppingCartItem = shoppingCart.items[i]; // To refactor
                      // eslint-disable-next-line no-await-in-loop

                      _context.next = 3;
                      return _MenuCategoryItem["default"].findById(shoppingCartItem.menuCategoryItemId).populate({
                        path: 'meals.dishes.dish',
                        model: _Dish["default"]
                      });

                    case 3:
                      menuCategoryItem = _context.sent;
                      amount = shoppingCartItem.amount;
                      subTotalAmount += amount;
                      items = [];
                      menuCategoryItem.meals.forEach(function (meal) {
                        meal.dishes.forEach(function (dish) {
                          if (shoppingCartItem.items.includes(String(dish._id))) {
                            var item = {
                              title: dish.dish.title
                            };
                            items.push(item);
                          }
                        });
                      });
                      order = {
                        id: shoppingCartItem._id,
                        quantity: shoppingCartItem.quantity,
                        title: menuCategoryItem.title,
                        amount: amount,
                        items: items
                      };
                      orders.push(order);

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _loop);
            });
            i = 0;

          case 9:
            if (!(i < shoppingCart.items.length)) {
              _context2.next = 14;
              break;
            }

            return _context2.delegateYield(_loop(i), "t0", 11);

          case 11:
            i++;
            _context2.next = 9;
            break;

          case 14:
            res.render(_path["default"].join(templateDir, 'checkout.twig'), {
              title: 'Livraison',
              user: req.user,
              orders: orders,
              deliveryAmount: 1.5,
              subTotalAmount: subTotalAmount
            });

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee);
  }));

  return function checkout(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * POST /checkout/update-basket-quantity
 * Update basket quantity
 */


exports.checkout = checkout;

var updateBasketQuantity = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var shoppingCart, _req$body, itemId, quantity, index, item, itemAmount, subTotalAmount, totalQuantity, i, shoppingCartItem, amount, result;

    return regeneratorRuntime.wrap(function _callee2$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            shoppingCart = req.session.shoppingCart;
            _req$body = req.body, itemId = _req$body.itemId, quantity = _req$body.quantity;
            index = shoppingCart.items.findIndex(function (item) {
              return item._id === itemId;
            });
            item = shoppingCart.items[index];
            itemAmount = 0;

            if (!(quantity > 0)) {
              _context3.next = 14;
              break;
            }

            itemAmount = item.amount * quantity / item.quantity;
            item.quantity = quantity;
            item.amount = itemAmount;
            _context3.next = 11;
            return (0, _shoppingCartRepository.addItemToShoppingCart)(shoppingCart._id, item, index);

          case 11:
            shoppingCart = _context3.sent;
            _context3.next = 18;
            break;

          case 14:
            _context3.next = 16;
            return (0, _shoppingCartRepository.removeItemToShoppingCartFromIndex)(shoppingCart._id, index);

          case 16:
            shoppingCart = _context3.sent;
            console.log('shoppingCart ', shoppingCart);

          case 18:
            subTotalAmount = 0;
            totalQuantity = 0;

            if (shoppingCart && shoppingCart.items && shoppingCart.items.length > 0) {
              for (i = 0; i < shoppingCart.items.length; i++) {
                shoppingCartItem = shoppingCart.items[i];
                amount = shoppingCartItem.amount;
                totalQuantity += shoppingCartItem.quantity;
                subTotalAmount += amount;
              }
            }

            req.session.shoppingCart = shoppingCart;
            result = {
              newAmount: itemAmount,
              deliveryAmount: 1.5,
              subTotalAmount: subTotalAmount,
              totalQuantity: totalQuantity
            };
            return _context3.abrupt("return", res.json(result));

          case 24:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee2);
  }));

  return function updateBasketQuantity(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();
/**
 * POST /cart/checkMenuCategoryItemQuantity
 * Check menu category item quantity
 */


exports.updateBasketQuantity = updateBasketQuantity;

var checkMenuCategoryItemQuantity = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body2, menuCategoryItemId, quantity, result;

    return regeneratorRuntime.wrap(function _callee3$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _req$body2 = req.body, menuCategoryItemId = _req$body2.menuCategoryItemId, quantity = _req$body2.quantity;
            _context4.next = 3;
            return (0, _menuCategoryItemRepository.checkQuantityByDish)(menuCategoryItemId, quantity);

          case 3:
            result = _context4.sent;
            return _context4.abrupt("return", res.json(result));

          case 5:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee3);
  }));

  return function checkMenuCategoryItemQuantity(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();
/**
 * POST /cart/addMenuQuantity
 * Check menu category item quantity
 */


exports.checkMenuCategoryItemQuantity = checkMenuCategoryItemQuantity;

var addMenuQuantity = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _req$body3, menuCategoryItemId, quantity, additionalPrice, menuCategoryItem, price, result;

    return regeneratorRuntime.wrap(function _callee4$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _req$body3 = req.body, menuCategoryItemId = _req$body3.menuCategoryItemId, quantity = _req$body3.quantity, additionalPrice = _req$body3.additionalPrice;
            _context5.next = 3;
            return _MenuCategoryItem["default"].findById(menuCategoryItemId);

          case 3:
            menuCategoryItem = _context5.sent;

            if (menuCategoryItem) {
              _context5.next = 6;
              break;
            }

            throw new Error("Impossible de trouver l'\xE9l\xE9ment de categorie du menu avec id: ".concat(menuCategoryItemId));

          case 6:
            price = (menuCategoryItem.price + parseInt(additionalPrice, 10)) * quantity;
            result = {
              price: price
            };
            return _context5.abrupt("return", res.json(result));

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee4);
  }));

  return function addMenuQuantity(_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}(); // todo: move to utils


exports.addMenuQuantity = addMenuQuantity;

var arrayMatch = function arrayMatch(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }

  for (var i = 0; i < arr1.length; i++) {
    if (!arr2.includes(arr1[i])) {
      return false;
    }
  }

  return true;
};

var checkItemCart = function checkItemCart(cart, newItem) {
  for (var i = 0; i < cart.length; i++) {
    if (newItem.menuCategoryItemId === cart[i].menuCategoryItemId) {
      if (!newItem.items && (!cart[i].items || cart[i].items.length === 0)) {
        return {
          isNew: false,
          index: i,
          quantity: cart[i].quantity,
          amount: cart[i].amount
        };
      }

      if (newItem.items && cart[i].items && newItem.items.length === cart[i].items.length) {
        var isArrayMatch = arrayMatch(newItem.items, cart[i].items);

        if (isArrayMatch) {
          return {
            isNew: false,
            index: i,
            quantity: cart[i].quantity,
            amount: cart[i].amount
          };
        }
      }
    }
  }

  return {
    isNew: true
  };
};
/**
 * POST /cart/addToCart
 * Handle add cart
 */


var addToCart = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _req$body4, menuCategoryItemId, quantity, amount, items, newItemCart, shoppingCart, isNewItemCart, newItemCartIndex, newItemCartQuantity, newItemCartAmount, checkItemCartRest, id;

    return regeneratorRuntime.wrap(function _callee5$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _req$body4 = req.body, menuCategoryItemId = _req$body4.menuCategoryItemId, quantity = _req$body4.quantity, amount = _req$body4.amount, items = _req$body4.items;
            newItemCart = {
              menuCategoryItemId: menuCategoryItemId,
              quantity: quantity,
              amount: amount,
              items: items
            };
            shoppingCart = req.session.shoppingCart;
            isNewItemCart = true;

            if (shoppingCart && shoppingCart.items && shoppingCart.items.length > 0) {
              checkItemCartRest = checkItemCart(shoppingCart.items, newItemCart);
              isNewItemCart = checkItemCartRest.isNew;
              newItemCartIndex = checkItemCartRest.index;
              newItemCartAmount = checkItemCartRest.amount;
              newItemCartQuantity = checkItemCartRest.quantity;
            }

            if (!isNewItemCart) {
              _context6.next = 12;
              break;
            }

            id = shoppingCart ? shoppingCart._id : '';
            _context6.next = 9;
            return (0, _shoppingCartRepository.createShoppingCart)(newItemCart, id);

          case 9:
            shoppingCart = _context6.sent;
            _context6.next = 17;
            break;

          case 12:
            newItemCart.quantity = parseInt(newItemCart.quantity, 10) + parseInt(newItemCartQuantity, 10);
            newItemCart.amount = parseFloat(newItemCart.amount, 10) + parseFloat(newItemCartAmount, 10);
            _context6.next = 16;
            return (0, _shoppingCartRepository.addItemToShoppingCart)(shoppingCart._id, newItemCart, newItemCartIndex);

          case 16:
            shoppingCart = _context6.sent;

          case 17:
            req.session.shoppingCart = shoppingCart;
            return _context6.abrupt("return", res.json(req.session.shoppingCart.items));

          case 19:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee5);
  }));

  return function addToCart(_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}();

exports.addToCart = addToCart;