"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProfileAccount = exports.getLogout = exports.getSignup = exports.postSignup = exports.postLogin = exports.getLogin = void 0;

var _path = _interopRequireDefault(require("path"));

var _util = require("util");

var _crypto = _interopRequireDefault(require("crypto"));

var _nodemailer = _interopRequireDefault(require("nodemailer"));

var _passport = _interopRequireDefault(require("passport"));

var _validator = _interopRequireDefault(require("validator"));

var _mailchecker = _interopRequireDefault(require("mailchecker"));

var _User = _interopRequireDefault(require("../../../models/User"));

var _userRepository = require("../../../repositories/userRepository");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _ = require('lodash');

var templateDir = 'modules/front/account/views';
var randomBytesAsync = (0, _util.promisify)(_crypto["default"].randomBytes);
/**
 * GET /login
 * Login page
 */

var getLogin = function getLogin(req, res) {
  if (req.user) {
    return res.redirect('/');
  }

  res.render(_path["default"].join(templateDir, 'login.twig'), {
    title: 'Connexion'
  });
};
/**
 * POST /login
 * Sign in using email and password.
 */


exports.getLogin = getLogin;

var postLogin = function postLogin(req, res, next) {
  var validationErrors = [];
  if (!_validator["default"].isEmail(req.body.email)) validationErrors.push({
    msg: 'Veuillez saisir une adresse email valide.'
  });
  if (_validator["default"].isEmpty(req.body.password)) validationErrors.push({
    msg: 'Veuillez saisir votre mot de passe.'
  });

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);
    return res.redirect('/login');
  }

  req.body.email = _validator["default"].normalizeEmail(req.body.email, {
    gmail_remove_dots: false
  });

  _passport["default"].authenticate('local', function (err, user, info) {
    if (err) {
      return next(err);
    }

    if (!user) {
      console.error(info);
      req.flash('formErrors', info);
      return res.redirect('/login');
    }

    req.logIn(user, function (err) {
      if (err) {
        return next(err);
      }

      console.info('Success! You are logged in.');
      res.redirect(req.session.returnTo || '/');
    });
  })(req, res, next);
};
/**
 * GET /signup
 * Signup page.
 */


exports.postLogin = postLogin;

var getSignup = function getSignup(req, res) {
  if (req.user) {
    return res.redirect('/');
  }

  res.render(_path["default"].join(templateDir, 'signup.twig'), {
    title: 'Inscription'
  });
};
/**
 * GET /account/profile
 */


exports.getSignup = getSignup;

var getProfileAccount = function getProfileAccount(req, res) {
  if (!req.user) {
    return res.redirect('/login');
  }

  res.render(_path["default"].join(templateDir, 'account/profile.twig'), {
    title: 'Mon compte'
  });
};
/**
 * POST /signup
 * Create a new local account.
 */


exports.getProfileAccount = getProfileAccount;

var postSignup = function postSignup(req, res, next) {
  var validationErrors = [];
  if (_validator["default"].isEmpty(req.body.firstName)) validationErrors.push({
    msg: 'Veuillez saisir votre prénom.'
  });
  if (_validator["default"].isEmpty(req.body.phoneNumber)) validationErrors.push({
    msg: 'Veuillez saisir votre numéro de téléphone mobile.'
  });
  if (!_validator["default"].isEmail(req.body.email)) validationErrors.push({
    msg: 'Veuillez saisir une adresse email valide.'
  });
  if (!_validator["default"].isLength(req.body.password, {
    min: 6
  })) validationErrors.push({
    msg: 'Votre mot de passe doit avoir au minimum 6 caractères.'
  });

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);
    return res.redirect('/signup');
  }

  req.body.email = _validator["default"].normalizeEmail(req.body.email, {
    gmail_remove_dots: false
  });
  console.log('Create new user :', req.body);
  var user = (0, _userRepository.newUser)({
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  });

  _User["default"].findOne({
    email: req.body.email
  }, function (err, existingUser) {
    if (err) {
      return next(err);
    }

    if (existingUser) {
      console.warn('Un compte existe déjà avec cette adresse email.');
      req.flash('formErrors', {
        msg: 'Un compte existe déjà avec cette adresse email.'
      });
      return res.redirect('/signup');
    }

    user.save(function (err) {
      if (err) {
        return next(err);
      }

      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        }

        res.redirect('/');
      });
    });
  });
};
/**
 * GET /logout
 * Log out.
 */


exports.postSignup = postSignup;

var getLogout = function getLogout(req, res) {
  req.logout();
  req.session.destroy(function (err) {
    if (err) console.log('Error : Failed to destroy the session during logout.', err);
    req.user = null;
    res.redirect('/');
  });
};
/**
 * POST /account/profile
 * Update profile information.
 */


exports.getLogout = getLogout;

exports.postUpdateProfile = function (req, res, next) {
  var validationErrors = [];
  if (!_validator["default"].isEmail(req.body.email)) validationErrors.push({
    msg: 'Please enter a valid email address.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/account');
  }

  req.body.email = _validator["default"].normalizeEmail(req.body.email, {
    gmail_remove_dots: false
  });

  _User["default"].findById(req.user.id, function (err, user) {
    if (err) {
      return next(err);
    }

    if (user.email !== req.body.email) user.emailVerified = false;
    user.email = req.body.email || '';
    user.profile.name = req.body.name || '';
    user.profile.gender = req.body.gender || '';
    user.profile.location = req.body.location || '';
    user.profile.website = req.body.website || '';
    user.save(function (err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', {
            msg: 'The email address you have entered is already associated with an account.'
          });
          return res.redirect('/account');
        }

        return next(err);
      }

      req.flash('success', {
        msg: 'Profile information has been updated.'
      });
      res.redirect('/account');
    });
  });
};
/**
 * POST /account/password
 * Update current password.
 */


exports.postUpdatePassword = function (req, res, next) {
  var validationErrors = [];
  if (!_validator["default"].isLength(req.body.password, {
    min: 8
  })) validationErrors.push({
    msg: 'Password must be at least 8 characters long'
  });
  if (req.body.password !== req.body.confirmPassword) validationErrors.push({
    msg: 'Passwords do not match'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/account');
  }

  _User["default"].findById(req.user.id, function (err, user) {
    if (err) {
      return next(err);
    }

    user.password = req.body.password;
    user.save(function (err) {
      if (err) {
        return next(err);
      }

      req.flash('success', {
        msg: 'Password has been changed.'
      });
      res.redirect('/account');
    });
  });
};
/**
 * POST /account/delete
 * Delete user account.
 */


exports.postDeleteAccount = function (req, res, next) {
  _User["default"].deleteOne({
    _id: req.user.id
  }, function (err) {
    if (err) {
      return next(err);
    }

    req.logout();
    req.flash('info', {
      msg: 'Your account has been deleted.'
    });
    res.redirect('/');
  });
};
/**
 * GET /account/unlink/:provider
 * Unlink OAuth provider.
 */


exports.getOauthUnlink = function (req, res, next) {
  var provider = req.params.provider;

  _User["default"].findById(req.user.id, function (err, user) {
    if (err) {
      return next(err);
    }

    user[provider.toLowerCase()] = undefined;
    var tokensWithoutProviderToUnlink = user.tokens.filter(function (token) {
      return token.kind !== provider.toLowerCase();
    }); // Some auth providers do not provide an email address in the user profile.
    // As a result, we need to verify that unlinking the provider is safe by ensuring
    // that another login method exists.

    if (!(user.email && user.password) && tokensWithoutProviderToUnlink.length === 0) {
      req.flash('errors', {
        msg: "The ".concat(_.startCase(_.toLower(provider)), " account cannot be unlinked without another form of login enabled.") + ' Please link another account or add an email address and password.'
      });
      return res.redirect('/account');
    }

    user.tokens = tokensWithoutProviderToUnlink;
    user.save(function (err) {
      if (err) {
        return next(err);
      }

      req.flash('info', {
        msg: "".concat(_.startCase(_.toLower(provider)), " account has been unlinked.")
      });
      res.redirect('/account');
    });
  });
};
/**
 * GET /reset/:token
 * Reset Password page.
 */


exports.getReset = function (req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }

  var validationErrors = [];
  if (!_validator["default"].isHexadecimal(req.params.token)) validationErrors.push({
    msg: 'Invalid Token.  Please retry.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/forgot');
  }

  _User["default"].findOne({
    passwordResetToken: req.params.token
  }).where('passwordResetExpires').gt(Date.now()).exec(function (err, user) {
    if (err) {
      return next(err);
    }

    if (!user) {
      req.flash('errors', {
        msg: 'Password reset token is invalid or has expired.'
      });
      return res.redirect('/forgot');
    }

    res.render('account/reset', {
      title: 'Password Reset'
    });
  });
};
/**
 * GET /account/verify/:token
 * Verify email address
 */


exports.getVerifyEmailToken = function (req, res, next) {
  if (req.user.emailVerified) {
    req.flash('info', {
      msg: 'The email address has been verified.'
    });
    return res.redirect('/account');
  }

  var validationErrors = [];
  if (req.params.token && !_validator["default"].isHexadecimal(req.params.token)) validationErrors.push({
    msg: 'Invalid Token.  Please retry.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/account');
  }

  if (req.params.token === req.user.emailVerificationToken) {
    _User["default"].findOne({
      email: req.user.email
    }).then(function (user) {
      if (!user) {
        req.flash('errors', {
          msg: 'There was an error in loading your profile.'
        });
        return res.redirect('back');
      }

      user.emailVerificationToken = '';
      user.emailVerified = true;
      user = user.save();
      req.flash('info', {
        msg: 'Thank you for verifying your email address.'
      });
      return res.redirect('/account');
    })["catch"](function (error) {
      console.log('Error saving the user profile to the database after email verification', error);
      req.flash('errors', {
        msg: 'There was an error when updating your profile.  Please try again later.'
      });
      return res.redirect('/account');
    });
  } else {
    req.flash('errors', {
      msg: 'The verification link was invalid, or is for a different account.'
    });
    return res.redirect('/account');
  }
};
/**
 * GET /account/verify
 * Verify email address
 */


exports.getVerifyEmail = function (req, res, next) {
  if (req.user.emailVerified) {
    req.flash('info', {
      msg: 'The email address has been verified.'
    });
    return res.redirect('/account');
  }

  if (!_mailchecker["default"].isValid(req.user.email)) {
    req.flash('errors', {
      msg: 'The email address is invalid or disposable and can not be verified.  Please update your email address and try again.'
    });
    return res.redirect('/account');
  }

  var createRandomToken = randomBytesAsync(16).then(function (buf) {
    return buf.toString('hex');
  });

  var setRandomToken = function setRandomToken(token) {
    _User["default"].findOne({
      email: req.user.email
    }).then(function (user) {
      user.emailVerificationToken = token;
      user = user.save();
    });

    return token;
  };

  var sendVerifyEmail = function sendVerifyEmail(token) {
    var transporter = _nodemailer["default"].createTransport({
      service: 'SendGrid',
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD
      }
    });

    var mailOptions = {
      to: req.user.email,
      from: 'hackathon@starter.com',
      subject: 'Please verify your email address on Hackathon Starter',
      text: "Thank you for registering with hackathon-starter.\n\n\n        This verify your email address please click on the following link, or paste this into your browser:\n\n\n        http://".concat(req.headers.host, "/account/verify/").concat(token, "\n\n\n        \n\n\n        Thank you!")
    };
    return transporter.sendMail(mailOptions).then(function () {
      req.flash('info', {
        msg: "An e-mail has been sent to ".concat(req.user.email, " with further instructions.")
      });
    })["catch"](function (err) {
      if (err.message === 'self signed certificate in certificate chain') {
        console.log('WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.');
        transporter = _nodemailer["default"].createTransport({
          service: 'SendGrid',
          auth: {
            user: process.env.SENDGRID_USER,
            pass: process.env.SENDGRID_PASSWORD
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        return transporter.sendMail(mailOptions).then(function () {
          req.flash('info', {
            msg: "An e-mail has been sent to ".concat(req.user.email, " with further instructions.")
          });
        });
      }

      console.log('ERROR: Could not send verifyEmail email after security downgrade.\n', err);
      req.flash('errors', {
        msg: 'Error sending the email verification message. Please try again shortly.'
      });
      return err;
    });
  };

  createRandomToken.then(setRandomToken).then(sendVerifyEmail).then(function () {
    return res.redirect('/account');
  })["catch"](next);
};
/**
 * POST /reset/:token
 * Process the reset password request.
 */


exports.postReset = function (req, res, next) {
  var validationErrors = [];
  if (!_validator["default"].isLength(req.body.password, {
    min: 8
  })) validationErrors.push({
    msg: 'Password must be at least 8 characters long'
  });
  if (req.body.password !== req.body.confirm) validationErrors.push({
    msg: 'Passwords do not match'
  });
  if (!_validator["default"].isHexadecimal(req.params.token)) validationErrors.push({
    msg: 'Invalid Token.  Please retry.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('back');
  }

  var resetPassword = function resetPassword() {
    return _User["default"].findOne({
      passwordResetToken: req.params.token
    }).where('passwordResetExpires').gt(Date.now()).then(function (user) {
      if (!user) {
        req.flash('errors', {
          msg: 'Password reset token is invalid or has expired.'
        });
        return res.redirect('back');
      }

      user.password = req.body.password;
      user.passwordResetToken = undefined;
      user.passwordResetExpires = undefined;
      return user.save().then(function () {
        return new Promise(function (resolve, reject) {
          req.logIn(user, function (err) {
            if (err) {
              return reject(err);
            }

            resolve(user);
          });
        });
      });
    });
  };

  var sendResetPasswordEmail = function sendResetPasswordEmail(user) {
    if (!user) {
      return;
    }

    var transporter = _nodemailer["default"].createTransport({
      service: 'SendGrid',
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD
      }
    });

    var mailOptions = {
      to: user.email,
      from: 'hackathon@starter.com',
      subject: 'Your Hackathon Starter password has been changed',
      text: "Hello,\n\nThis is a confirmation that the password for your account ".concat(user.email, " has just been changed.\n")
    };
    return transporter.sendMail(mailOptions).then(function () {
      req.flash('success', {
        msg: 'Success! Your password has been changed.'
      });
    })["catch"](function (err) {
      if (err.message === 'self signed certificate in certificate chain') {
        console.log('WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.');
        transporter = _nodemailer["default"].createTransport({
          service: 'SendGrid',
          auth: {
            user: process.env.SENDGRID_USER,
            pass: process.env.SENDGRID_PASSWORD
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        return transporter.sendMail(mailOptions).then(function () {
          req.flash('success', {
            msg: 'Success! Your password has been changed.'
          });
        });
      }

      console.log('ERROR: Could not send password reset confirmation email after security downgrade.\n', err);
      req.flash('warning', {
        msg: 'Your password has been changed, however we were unable to send you a confirmation email. We will be looking into it shortly.'
      });
      return err;
    });
  };

  resetPassword().then(sendResetPasswordEmail).then(function () {
    if (!res.finished) res.redirect('/');
  })["catch"](function (err) {
    return next(err);
  });
};
/**
 * GET /forgot
 * Forgot Password page.
 */


exports.getForgot = function (req, res) {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render('account/forgot', {
    title: 'Forgot Password'
  });
};
/**
 * POST /forgot
 * Create a random token, then the send user an email with a reset link.
 */


exports.postForgot = function (req, res, next) {
  var validationErrors = [];
  if (!_validator["default"].isEmail(req.body.email)) validationErrors.push({
    msg: 'Please enter a valid email address.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/forgot');
  }

  req.body.email = _validator["default"].normalizeEmail(req.body.email, {
    gmail_remove_dots: false
  });
  var createRandomToken = randomBytesAsync(16).then(function (buf) {
    return buf.toString('hex');
  });

  var setRandomToken = function setRandomToken(token) {
    return _User["default"].findOne({
      email: req.body.email
    }).then(function (user) {
      if (!user) {
        req.flash('errors', {
          msg: 'Account with that email address does not exist.'
        });
      } else {
        user.passwordResetToken = token;
        user.passwordResetExpires = Date.now() + 3600000; // 1 hour

        user = user.save();
      }

      return user;
    });
  };

  var sendForgotPasswordEmail = function sendForgotPasswordEmail(user) {
    if (!user) {
      return;
    }

    var token = user.passwordResetToken;

    var transporter = _nodemailer["default"].createTransport({
      service: 'SendGrid',
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD
      }
    });

    var mailOptions = {
      to: user.email,
      from: 'hackathon@starter.com',
      subject: 'Reset your password on Hackathon Starter',
      text: "You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n\n        Please click on the following link, or paste this into your browser to complete the process:\n\n\n        http://".concat(req.headers.host, "/reset/").concat(token, "\n\n\n        If you did not request this, please ignore this email and your password will remain unchanged.\n")
    };
    return transporter.sendMail(mailOptions).then(function () {
      req.flash('info', {
        msg: "An e-mail has been sent to ".concat(user.email, " with further instructions.")
      });
    })["catch"](function (err) {
      if (err.message === 'self signed certificate in certificate chain') {
        console.log('WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.');
        transporter = _nodemailer["default"].createTransport({
          service: 'SendGrid',
          auth: {
            user: process.env.SENDGRID_USER,
            pass: process.env.SENDGRID_PASSWORD
          },
          tls: {
            rejectUnauthorized: false
          }
        });
        return transporter.sendMail(mailOptions).then(function () {
          req.flash('info', {
            msg: "An e-mail has been sent to ".concat(user.email, " with further instructions.")
          });
        });
      }

      console.log('ERROR: Could not send forgot password email after security downgrade.\n', err);
      req.flash('errors', {
        msg: 'Error sending the password reset message. Please try again shortly.'
      });
      return err;
    });
  };

  createRandomToken.then(setRandomToken).then(sendForgotPasswordEmail).then(function () {
    return res.redirect('/forgot');
  })["catch"](next);
};