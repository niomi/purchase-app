"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _compression = _interopRequireDefault(require("compression"));

var _expressSession = _interopRequireDefault(require("express-session"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _morgan = _interopRequireDefault(require("morgan"));

var _chalk = _interopRequireDefault(require("chalk"));

var _errorhandler = _interopRequireDefault(require("errorhandler"));

var _lusca = _interopRequireDefault(require("lusca"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _connectMongo = _interopRequireDefault(require("connect-mongo"));

var _expressFlash = _interopRequireDefault(require("express-flash"));

var _path = _interopRequireDefault(require("path"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _passport = _interopRequireDefault(require("passport"));

var _expressStatusMonitor = _interopRequireDefault(require("express-status-monitor"));

var _nodeSassMiddleware = _interopRequireDefault(require("node-sass-middleware"));

var _twig = _interopRequireDefault(require("twig"));

var _twig2 = _interopRequireDefault(require("./config/twig"));

require("core-js/stable");

require("regenerator-runtime/runtime");

var _userController = require("./modules/front/account/userController");

var _homeController = require("./modules/front/home/homeController");

var _cartController = require("./modules/front/cart/cartController");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var MongoStore = (0, _connectMongo["default"])(_expressSession["default"]);
/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */

_dotenv["default"].config({
  path: '.env'
});
/**
 * API keys and Passport configuration.
 */


var passportConfig = require('./config/passport');
/**
 * Configure twig with extends filters
 */


(0, _twig2["default"])();
/**
 * Create Express server.
 */

var app = (0, _express["default"])();
/**
 * Connect to MongoDB.
 */

_mongoose["default"].set('useFindAndModify', false);

_mongoose["default"].set('useCreateIndex', true);

_mongoose["default"].set('useNewUrlParser', true);

_mongoose["default"].connect(process.env.MONGODB_URI, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});

_mongoose["default"].connection.on('error', function (err) {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', _chalk["default"].red('✗'));
  process.exit();
});
/**
 * Express configuration.
 */


app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('views', __dirname);
app.set('twig options', {
  allow_async: true,
  strict_variables: false
});
app.use((0, _expressStatusMonitor["default"])());
app.use((0, _compression["default"])());
app.use((0, _nodeSassMiddleware["default"])({
  src: _path["default"].join(__dirname, 'public'),
  dest: _path["default"].join(__dirname, 'public')
}));
app.use((0, _morgan["default"])('dev'));
app.use(_bodyParser["default"].json());
app.use(_bodyParser["default"].urlencoded({
  extended: true
}));
app.use((0, _expressSession["default"])({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: {
    maxAge: 1209600000
  },
  // two weeks in milliseconds
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true
  })
}));
app.use(_passport["default"].initialize());
app.use(_passport["default"].session());
app.use((0, _expressFlash["default"])());
app.use(function (req, res, next) {
  //lusca.csrf()(req, res, next);
  next();
});
app.use(_lusca["default"].xframe('SAMEORIGIN'));
app.use(_lusca["default"].xssProtection(true));
app.disable('x-powered-by');
app.use(function (req, res, next) {
  res.locals.user = req.user;
  res.locals.session = req.session;
  next();
});
app.use(function (req, res, next) {
  // After successful login, redirect back to the intended page

  /* if (!req.user
    && req.path !== '/login'
    && req.path !== '/signup'
    && !req.path.match(/^\/auth/)
    && !req.path.match(/\./)) {
    req.session.returnTo = req.originalUrl;
  } else if (req.user
    && (req.path === '/account' || req.path.match(/^\/api/))) {
    req.session.returnTo = req.originalUrl;
  } */
  next();
});
console.log('public file', _path["default"].join(__dirname, 'public')); //app.use('/', express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

app.use('/', _express["default"]["static"](_path["default"].join(__dirname, 'public')));
/**
 * Primary app routes.
 */

app.get('/login', _userController.getLogin);
app.post('/login', _userController.postLogin);
app.get('/signup', _userController.getSignup);
app.post('/signup', _userController.postSignup);
app.get('/logout', _userController.getLogout);
app.get('/account/profile', _userController.getProfileAccount);
/**
 * Cart calls.
 */

app.post('/cart/checkMenuCategoryItemQuantity', _cartController.checkMenuCategoryItemQuantity);
app.post('/cart/addToCart', _cartController.addToCart);
app.post('/cart/addMenuQuantity', _cartController.addMenuQuantity);
/**
 * Checkout calls
 */

app.get('/checkout', _cartController.checkout);
app.post('/checkout/update-basket-quantity', _cartController.updateBasketQuantity);
/**
 * OAuth authentication routes. (Sign in)
 */

app.get('/auth/facebook', _passport["default"].authenticate('facebook', {
  scope: ['email', 'public_profile']
}));
app.get('/auth/facebook/callback', _passport["default"].authenticate('facebook', {
  failureRedirect: '/login'
}), function (req, res) {
  res.redirect(req.session.returnTo || '/');
});
app.get('/auth/google', _passport["default"].authenticate('google', {
  scope: ['profile', 'email'],
  accessType: 'offline',
  prompt: 'consent'
}));
app.get('/auth/google/callback', _passport["default"].authenticate('google', {
  failureRedirect: '/login'
}), function (req, res) {
  res.redirect(req.session.returnTo || '/');
});
/**
 * Home routes.
 */

app.get('/', _homeController.home);
/**
 * Error Handler.
 */

if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use((0, _errorhandler["default"])());
} else {
  app.use(function (err, req, res, next) {
    console.error(err);
    res.status(500).send('Server Error');
  });
}
/**
 * Start Express server.
 */


app.listen(app.get('port'), function () {
  console.log('%s App is running at http://localhost:%d in %s mode', _chalk["default"].green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});
var _default = app;
exports["default"] = _default;