"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _twig = _interopRequireDefault(require("twig"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var twigConfig = function twigConfig() {
  _twig["default"].extendFilter('sortByField', function (value, params) {
    return value.sort(function (a, b) {
      return a[params[0]] - b[params[0]];
    });
  });
};

var _default = twigConfig;
exports["default"] = _default;