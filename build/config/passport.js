"use strict";

var _passport = _interopRequireDefault(require("passport"));

var _passportOauth2Refresh = _interopRequireDefault(require("passport-oauth2-refresh"));

var _moment = _interopRequireDefault(require("moment"));

var _User = _interopRequireDefault(require("../models/User"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('passport-local'),
    LocalStrategy = _require.Strategy;

var _require2 = require('passport-facebook'),
    FacebookStrategy = _require2.Strategy;

var _require3 = require('passport-google-oauth'),
    GoogleStrategy = _require3.OAuth2Strategy;

var _ = require('lodash');

_passport["default"].serializeUser(function (user, done) {
  done(null, user.id);
});

_passport["default"].deserializeUser(function (id, done) {
  _User["default"].findById(id, function (err, user) {
    done(err, user);
  });
});
/**
 * Sign in using Email and Password.
 */


_passport["default"].use(new LocalStrategy({
  usernameField: 'email'
}, function (email, password, done) {
  _User["default"].findOne({
    email: email.toLowerCase()
  }, function (err, user) {
    if (err) {
      return done(err);
    }

    if (!user) {
      return done(null, false, {
        msg: 'Aucun compte ne correspondant à cette adresse e-mail.'
      });
    }

    if (!user.password) {
      return done(null, false, {
        msg: 'Votre compte a été enregistré via un tiers : Facebook ou Google. Pour activer la connexion par mot de passe, connectez-vous à l\'aide ce tiers, puis définissez un mot de passe sous votre profil utilisateur.'
      });
    }

    user.comparePassword(password, function (err, isMatch) {
      if (err) {
        return done(err);
      }

      if (isMatch) {
        return done(null, user);
      }

      return done(null, false, {
        msg: 'Mot de passe incorrect.'
      });
    });
  });
}));
/**
 * OAuth Strategy Overview
 *
 * - User is already logged in.
 *   - Check if there is an existing account with a provider id.
 *     - If there is, return an error message. (Account merging not supported)
 *     - Else link new OAuth account with currently logged-in user.
 * - User is not logged in.
 *   - Check if it's a returning user.
 *     - If returning user, sign in and we are done.
 *     - Else check if there is an existing account with user's email.
 *       - If there is, return an error message.
 *       - Else create a new account.
 */

/**
 * Sign in with Facebook.
 */


_passport["default"].use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_ID,
  clientSecret: process.env.FACEBOOK_SECRET,
  callbackURL: "".concat(process.env.BASE_URL, "/auth/facebook/callback"),
  profileFields: ['first_name', 'last_name', 'email'],
  passReqToCallback: true
}, function (req, accessToken, refreshToken, profile, done) {
  if (req.user) {
    _User["default"].findOne({
      facebook: profile.id
    }, function (err, existingUser) {
      if (err) {
        return done(err);
      }

      if (existingUser) {
        req.flash('authProviderErrors', {
          msg: 'Il existe déjà un compte Facebook vous appartenant. Connectez-vous avec ce compte ou supprimez-le, puis associez-le à votre compte actuel.'
        });
        done(err);
      } else {
        _User["default"].findById(req.user.id, function (err, user) {
          if (err) {
            return done(err);
          }

          user.facebook = profile.id;
          user.tokens.push({
            kind: 'facebook',
            accessToken: accessToken
          });
          user.profile.firstName = user.profile.first_name || "".concat(profile.name.first_name);
          user.profile.lastName = user.profile.last_name || "".concat(profile.name.last_name);
          user.save(function (err) {
            req.flash('info', {
              msg: 'Facebook account has been linked.'
            });
            done(err, user);
          });
        });
      }
    });
  } else {
    _User["default"].findOne({
      facebook: profile.id
    }, function (err, existingUser) {
      if (err) {
        return done(err);
      }

      if (existingUser) {
        return done(null, existingUser);
      }

      _User["default"].findOne({
        email: profile._json.email
      }, function (err, existingEmailUser) {
        if (err) {
          return done(err);
        }

        if (existingEmailUser) {
          req.flash('authProviderErrors', {
            msg: 'Il existe déjà un compte utilisant cette adresse e-mail. Veuillez-vous connectez avec ce compte.'
          });
          done(err);
        } else {
          var user = new _User["default"]();
          user.email = profile._json.email;
          user.facebook = profile.id;
          user.tokens.push({
            kind: 'facebook',
            accessToken: accessToken
          });
          user.profile.firstName = "".concat(profile._json.first_name);
          user.profile.lastName = "".concat(profile._json.last_name);
          user.save(function (err) {
            done(err, user);
          });
        }
      });
    });
  }
}));
/**
 * Sign in with Google.
 */


var googleStrategyConfig = new GoogleStrategy({
  clientID: process.env.GOOGLE_ID,
  clientSecret: process.env.GOOGLE_SECRET,
  callbackURL: '/auth/google/callback',
  passReqToCallback: true
}, function (req, accessToken, refreshToken, params, profile, done) {
  if (req.user) {
    _User["default"].findOne({
      google: profile.id
    }, function (err, existingUser) {
      if (err) {
        return done(err);
      }

      if (existingUser && existingUser.id !== req.user.id) {
        req.flash('authProviderErrors', {
          msg: 'Il existe déjà un compte Google vous appartenant. Connectez-vous avec ce compte ou supprimez-le, puis associez-le à votre compte actuel.'
        });
        done(err);
      } else {
        _User["default"].findById(req.user.id, function (err, user) {
          if (err) {
            return done(err);
          }

          user.google = profile.id;
          user.tokens.push({
            kind: 'google',
            accessToken: accessToken,
            accessTokenExpires: (0, _moment["default"])().add(params.expires_in, 'seconds').format(),
            refreshToken: refreshToken
          });
          user.profile.firstName = user.profile.name || profile.displayName;
          user.profile.gender = user.profile.gender || profile._json.gender;
          user.save(function (err) {
            req.flash('info', {
              msg: 'Google account has been linked.'
            });
            done(err, user);
          });
        });
      }
    });
  } else {
    _User["default"].findOne({
      google: profile.id
    }, function (err, existingUser) {
      if (err) {
        return done(err);
      }

      if (existingUser) {
        return done(null, existingUser);
      }

      _User["default"].findOne({
        email: profile.emails[0].value
      }, function (err, existingEmailUser) {
        if (err) {
          return done(err);
        }

        if (existingEmailUser) {
          req.flash('authProviderErrors', {
            msg: 'Il existe déjà un compte utilisant cette adresse e-mail. Veuillez-vous connectez avec ce compte.'
          });
          done(err);
        } else {
          console.log('Profile google :', profile);
          var user = new _User["default"]();
          user.email = profile.emails[0].value;
          user.google = profile.id;
          user.tokens.push({
            kind: 'google',
            accessToken: accessToken,
            accessTokenExpires: (0, _moment["default"])().add(params.expires_in, 'seconds').format(),
            refreshToken: refreshToken
          });
          user.profile.firstName = profile._json.given_name;
          user.profile.lastName = profile._json.family_name;
          user.profile.gender = profile._json.gender;
          user.save(function (err) {
            done(err, user);
          });
        }
      });
    });
  }
});

_passport["default"].use('google', googleStrategyConfig);

_passportOauth2Refresh["default"].use('google', googleStrategyConfig);
/**
 * Login Required middleware.
 */


exports.isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect('/login');
};
/**
 * Authorization Required middleware.
 */


exports.isAuthorized = function (req, res, next) {
  var provider = req.path.split('/')[2];
  var token = req.user.tokens.find(function (token) {
    return token.kind === provider;
  });

  if (token) {
    // Is there an access token expiration and access token expired?
    // Yes: Is there a refresh token?
    //     Yes: Does it have expiration and if so is it expired?
    //       Yes, Quickbooks - We got nothing, redirect to res.redirect(`/auth/${provider}`);
    //       No, Quickbooks and Google- refresh token and save, and then go to next();
    //    No:  Treat it like we got nothing, redirect to res.redirect(`/auth/${provider}`);
    // No: we are good, go to next():
    if (token.accessTokenExpires && (0, _moment["default"])(token.accessTokenExpires).isBefore((0, _moment["default"])().subtract(1, 'minutes'))) {
      if (token.refreshToken) {
        if (token.refreshTokenExpires && (0, _moment["default"])(token.refreshTokenExpires).isBefore((0, _moment["default"])().subtract(1, 'minutes'))) {
          res.redirect("/auth/".concat(provider));
        } else {
          _passportOauth2Refresh["default"].requestNewAccessToken("".concat(provider), token.refreshToken, function (err, accessToken, refreshToken, params) {
            _User["default"].findById(req.user.id, function (err, user) {
              user.tokens.some(function (tokenObject) {
                if (tokenObject.kind === provider) {
                  tokenObject.accessToken = accessToken;
                  if (params.expires_in) tokenObject.accessTokenExpires = (0, _moment["default"])().add(params.expires_in, 'seconds').format();
                  return true;
                }

                return false;
              });
              req.user = user;
              user.markModified('tokens');
              user.save(function (err) {
                if (err) console.log(err);
                next();
              });
            });
          });
        }
      } else {
        res.redirect("/auth/".concat(provider));
      }
    } else {
      next();
    }
  } else {
    res.redirect("/auth/".concat(provider));
  }
};