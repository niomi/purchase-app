"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('util'),
    promisify = _require.promisify;

var cheerio = require('cheerio');

var graph = require('fbgraph');

var _require2 = require('lastfm'),
    LastFmNode = _require2.LastFmNode;

var tumblr = require('tumblr.js');

var _require3 = require('@octokit/rest'),
    Octokit = _require3.Octokit;

var Twit = require('twit');

var stripe = require('stripe')(process.env.STRIPE_SKEY);

var twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);

var clockwork = require('clockwork')({
  key: process.env.CLOCKWORK_KEY
});

var paypal = require('paypal-rest-sdk');

var lob = require('lob')(process.env.LOB_KEY);

var ig = require('instagram-node').instagram();

var axios = require('axios');

var _require4 = require('googleapis'),
    google = _require4.google;

var Quickbooks = require('node-quickbooks');

var validator = require('validator');

Quickbooks.setOauthVersion('2.0');
/**
 * GET /api
 * List of API examples.
 */

exports.getApi = function (req, res) {
  res.render('api/index', {
    title: 'API Examples'
  });
};
/**
 * GET /api/foursquare
 * Foursquare API example.
 */


exports.getFoursquare = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
    var token, trendingVenues, venueDetail, userCheckins;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return req.user.tokens.find(function (token) {
              return token.kind === 'foursquare';
            });

          case 2:
            token = _context.sent;
            axios.all([axios.get("https://api.foursquare.com/v2/venues/trending?ll=40.7222756,-74.0022724&limit=50&oauth_token=".concat(token.accessToken, "&v=20140806")), axios.get("https://api.foursquare.com/v2/venues/49da74aef964a5208b5e1fe3?oauth_token=".concat(token.accessToken, "&v=20190113")), axios.get("https://api.foursquare.com/v2/users/self/checkins?oauth_token=".concat(token.accessToken, "&v=20190113"))]).then(axios.spread(function (trendingVenuesRes, venueDetailRes, userCheckinsRes) {
              trendingVenues = trendingVenuesRes.data.response;
              venueDetail = venueDetailRes.data.response;
              userCheckins = userCheckinsRes.data.response;
              res.render('api/foursquare', {
                title: 'Foursquare API',
                trendingVenues: trendingVenues,
                venueDetail: venueDetail,
                userCheckins: userCheckins
              });
            }))["catch"](function (error) {
              next(error);
            });

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * GET /api/tumblr
 * Tumblr API example.
 */


exports.getTumblr = function (req, res, next) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'tumblr';
  });
  var client = tumblr.createClient({
    consumer_key: process.env.TUMBLR_KEY,
    consumer_secret: process.env.TUMBLR_SECRET,
    token: token.accessToken,
    token_secret: token.tokenSecret
  });
  client.blogPosts('mmosdotcom.tumblr.com', {
    type: 'photo'
  }, function (err, data) {
    if (err) {
      return next(err);
    }

    res.render('api/tumblr', {
      title: 'Tumblr API',
      blog: data.blog,
      photoset: data.posts[0].photos
    });
  });
};
/**
 * GET /api/facebook
 * Facebook API example.
 */


exports.getFacebook = function (req, res, next) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'facebook';
  });
  graph.setAccessToken(token.accessToken);
  graph.get("".concat(req.user.facebook, "?fields=id,name,email,first_name,last_name,gender,link,locale,timezone"), function (err, profile) {
    if (err) {
      return next(err);
    }

    res.render('api/facebook', {
      title: 'Facebook API',
      profile: profile
    });
  });
};
/**
 * GET /api/scraping
 * Web scraping example using Cheerio library.
 */


exports.getScraping = function (req, res, next) {
  axios.get('https://news.ycombinator.com/').then(function (response) {
    var $ = cheerio.load(response.data);
    var links = [];
    $('.title a[href^="http"], a[href^="https"]').slice(1).each(function (index, element) {
      links.push($(element));
    });
    res.render('api/scraping', {
      title: 'Web Scraping',
      links: links
    });
  })["catch"](function (error) {
    return next(error);
  });
};
/**
 * GET /api/github
 * GitHub API Example.
 */


exports.getGithub = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
    var github, _yield$github$repos$g, repo;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            github = new Octokit();
            _context2.prev = 1;
            _context2.next = 4;
            return github.repos.get({
              owner: 'sahat',
              repo: 'hackathon-starter'
            });

          case 4:
            _yield$github$repos$g = _context2.sent;
            repo = _yield$github$repos$g.data;
            res.render('api/github', {
              title: 'GitHub API',
              repo: repo
            });
            _context2.next = 12;
            break;

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](1);
            next(_context2.t0);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 9]]);
  }));

  return function (_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

exports.getQuickbooks = function (req, res) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'quickbooks';
  });
  var qbo = new Quickbooks(process.env.QUICKBOOKS_CLIENT_ID, process.env.QUICKBOOKS_CLIENT_SECRET, token.accessToken, false, req.user.quickbooks, true, false, null, '2.0', token.refreshToken);
  qbo.findCustomers(function (_, customers) {
    res.render('api/quickbooks', {
      title: 'Quickbooks API',
      customers: customers.QueryResponse.Customer
    });
  });
};
/**
 * GET /api/nyt
 * New York Times API example.
 */


exports.getNewYorkTimes = function (req, res, next) {
  var apiKey = process.env.NYT_KEY;
  axios.get("http://api.nytimes.com/svc/books/v2/lists?list-name=young-adult&api-key=".concat(apiKey)).then(function (response) {
    var books = response.data.results;
    res.render('api/nyt', {
      title: 'New York Times API',
      books: books
    });
  })["catch"](function (err) {
    var message = JSON.stringify(err.response.data.fault);
    next(new Error("New York Times API - ".concat(err.response.status, " ").concat(err.response.statusText, " ").concat(message)));
  });
};
/**
 * GET /api/lastfm
 * Last.fm API example.
 */


exports.getLastfm = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
    var lastfm, getArtistInfo, getArtistTopTracks, getArtistTopAlbums, _yield$getArtistInfo, artistInfo, topTracks, topAlbums, artist;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            lastfm = new LastFmNode({
              api_key: process.env.LASTFM_KEY,
              secret: process.env.LASTFM_SECRET
            });

            getArtistInfo = function getArtistInfo() {
              return new Promise(function (resolve, reject) {
                lastfm.request('artist.getInfo', {
                  artist: 'Roniit',
                  handlers: {
                    success: resolve,
                    error: reject
                  }
                });
              });
            };

            getArtistTopTracks = function getArtistTopTracks() {
              return new Promise(function (resolve, reject) {
                lastfm.request('artist.getTopTracks', {
                  artist: 'Roniit',
                  handlers: {
                    success: function success(_ref4) {
                      var toptracks = _ref4.toptracks;
                      resolve(toptracks.track.slice(0, 10));
                    },
                    error: reject
                  }
                });
              });
            };

            getArtistTopAlbums = function getArtistTopAlbums() {
              return new Promise(function (resolve, reject) {
                lastfm.request('artist.getTopAlbums', {
                  artist: 'Roniit',
                  handlers: {
                    success: function success(_ref5) {
                      var topalbums = _ref5.topalbums;
                      resolve(topalbums.album.slice(0, 3));
                    },
                    error: reject
                  }
                });
              });
            };

            _context3.prev = 4;
            _context3.next = 7;
            return getArtistInfo();

          case 7:
            _yield$getArtistInfo = _context3.sent;
            artistInfo = _yield$getArtistInfo.artist;
            _context3.next = 11;
            return getArtistTopTracks();

          case 11:
            topTracks = _context3.sent;
            _context3.next = 14;
            return getArtistTopAlbums();

          case 14:
            topAlbums = _context3.sent;
            artist = {
              name: artistInfo.name,
              image: artistInfo.image ? artistInfo.image.slice(-1)[0]['#text'] : null,
              tags: artistInfo.tags ? artistInfo.tags.tag : [],
              bio: artistInfo.bio ? artistInfo.bio.summary : '',
              stats: artistInfo.stats,
              similar: artistInfo.similar ? artistInfo.similar.artist : [],
              topTracks: topTracks,
              topAlbums: topAlbums
            };
            res.render('api/lastfm', {
              title: 'Last.fm API',
              artist: artist
            });
            _context3.next = 32;
            break;

          case 19:
            _context3.prev = 19;
            _context3.t0 = _context3["catch"](4);

            if (!(_context3.t0.error !== undefined)) {
              _context3.next = 31;
              break;
            }

            console.error(_context3.t0); // see error code list: https://www.last.fm/api/errorcodes

            _context3.t1 = _context3.t0.error;
            _context3.next = _context3.t1 === 10 ? 26 : 28;
            break;

          case 26:
            // Invalid API key
            res.render('api/lastfm', {
              error: _context3.t0
            });
            return _context3.abrupt("break", 29);

          case 28:
            res.render('api/lastfm', {
              error: _context3.t0
            });

          case 29:
            _context3.next = 32;
            break;

          case 31:
            next(_context3.t0);

          case 32:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[4, 19]]);
  }));

  return function (_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
}();
/**
 * GET /api/twitter
 * Twitter API example.
 */


exports.getTwitter = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res, next) {
    var token, T, _yield$T$get, tweets;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            token = req.user.tokens.find(function (token) {
              return token.kind === 'twitter';
            });
            T = new Twit({
              consumer_key: process.env.TWITTER_KEY,
              consumer_secret: process.env.TWITTER_SECRET,
              access_token: token.accessToken,
              access_token_secret: token.tokenSecret
            });
            _context4.prev = 2;
            _context4.next = 5;
            return T.get('search/tweets', {
              q: 'nodejs since:2013-01-01',
              geocode: '40.71448,-74.00598,5mi',
              count: 10
            });

          case 5:
            _yield$T$get = _context4.sent;
            tweets = _yield$T$get.data.statuses;
            res.render('api/twitter', {
              title: 'Twitter API',
              tweets: tweets
            });
            _context4.next = 13;
            break;

          case 10:
            _context4.prev = 10;
            _context4.t0 = _context4["catch"](2);
            next(_context4.t0);

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[2, 10]]);
  }));

  return function (_x10, _x11, _x12) {
    return _ref6.apply(this, arguments);
  };
}();
/**
 * POST /api/twitter
 * Post a tweet.
 */


exports.postTwitter = function (req, res, next) {
  var validationErrors = [];
  if (validator.isEmpty(req.body.tweet)) validationErrors.push({
    msg: 'Tweet cannot be empty'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/api/twitter');
  }

  var token = req.user.tokens.find(function (token) {
    return token.kind === 'twitter';
  });
  var T = new Twit({
    consumer_key: process.env.TWITTER_KEY,
    consumer_secret: process.env.TWITTER_SECRET,
    access_token: token.accessToken,
    access_token_secret: token.tokenSecret
  });
  T.post('statuses/update', {
    status: req.body.tweet
  }, function (err) {
    if (err) {
      return next(err);
    }

    req.flash('success', {
      msg: 'Your tweet has been posted.'
    });
    res.redirect('/api/twitter');
  });
};
/**
 * GET /api/steam
 * Steam API example.
 */


exports.getSteam = /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res, next) {
    var steamId, params, makeURL, getPlayerAchievements, getPlayerSummaries, getOwnedGames, playerstats, playerSummaries, ownedGames;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            steamId = req.user.steam;
            params = {
              l: 'english',
              steamid: steamId,
              key: process.env.STEAM_KEY
            }; // makes a url with search query

            makeURL = function makeURL(baseURL, params) {
              var url = new URL(baseURL);
              var urlParams = new URLSearchParams(params);
              url.search = urlParams.toString();
              return url.toString();
            }; // get the list of the recently played games, pick the most recent one and get its achievements


            getPlayerAchievements = function getPlayerAchievements() {
              var recentGamesURL = makeURL('http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/', params);
              return axios.get(recentGamesURL).then(function (_ref8) {
                var data = _ref8.data;

                // handle if player owns no games
                if (Object.keys(data.response).length === 0) {
                  return null;
                } // handle if there are no recently played games


                if (data.response.total_count === 0) {
                  return null;
                }

                params.appid = data.response.games[0].appid;
                var achievementsURL = makeURL('http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/', params);
                return axios.get(achievementsURL).then(function (_ref9) {
                  var data = _ref9.data;

                  // handle if there are no achievements for most recent game
                  if (!data.playerstats.achievements) {
                    return null;
                  }

                  return data.playerstats;
                });
              })["catch"](function (err) {
                if (err.response) {
                  // handle private profile or invalid key
                  if (err.response.status === 403) {
                    return null;
                  }
                }

                return Promise.reject(new Error('There was an error while getting achievements'));
              });
            };

            getPlayerSummaries = function getPlayerSummaries() {
              params.steamids = steamId;
              var url = makeURL('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/', params);
              return axios.get(url).then(function (_ref10) {
                var data = _ref10.data;
                return data;
              })["catch"](function () {
                return Promise.reject(new Error('There was an error while getting player summary'));
              });
            };

            getOwnedGames = function getOwnedGames() {
              params.include_appinfo = 1;
              params.include_played_free_games = 1;
              var url = makeURL('http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/', params);
              return axios.get(url).then(function (_ref11) {
                var data = _ref11.data;
                return data;
              })["catch"](function () {
                return Promise.reject(new Error('There was an error while getting owned games'));
              });
            };

            _context5.prev = 6;
            _context5.next = 9;
            return getPlayerAchievements();

          case 9:
            playerstats = _context5.sent;
            _context5.next = 12;
            return getPlayerSummaries();

          case 12:
            playerSummaries = _context5.sent;
            _context5.next = 15;
            return getOwnedGames();

          case 15:
            ownedGames = _context5.sent;
            res.render('api/steam', {
              title: 'Steam Web API',
              ownedGames: ownedGames.response,
              playerAchievements: playerstats,
              playerSummary: playerSummaries.response.players[0]
            });
            _context5.next = 22;
            break;

          case 19:
            _context5.prev = 19;
            _context5.t0 = _context5["catch"](6);
            next(_context5.t0);

          case 22:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[6, 19]]);
  }));

  return function (_x13, _x14, _x15) {
    return _ref7.apply(this, arguments);
  };
}();
/**
 * GET /api/stripe
 * Stripe API example.
 */


exports.getStripe = function (req, res) {
  res.render('api/stripe', {
    title: 'Stripe API',
    publishableKey: process.env.STRIPE_PKEY
  });
};
/**
 * POST /api/stripe
 * Make a payment.
 */


exports.postStripe = function (req, res) {
  var _req$body = req.body,
      stripeToken = _req$body.stripeToken,
      stripeEmail = _req$body.stripeEmail;
  stripe.charges.create({
    amount: 395,
    currency: 'usd',
    source: stripeToken,
    description: stripeEmail
  }, function (err) {
    if (err && err.type === 'StripeCardError') {
      req.flash('errors', {
        msg: 'Your card has been declined.'
      });
      return res.redirect('/api/stripe');
    }

    req.flash('success', {
      msg: 'Your card has been successfully charged.'
    });
    res.redirect('/api/stripe');
  });
};
/**
 * GET /api/twilio
 * Twilio API example.
 */


exports.getTwilio = function (req, res) {
  res.render('api/twilio', {
    title: 'Twilio API'
  });
};
/**
 * POST /api/twilio
 * Send a text message using Twilio.
 */


exports.postTwilio = function (req, res, next) {
  var validationErrors = [];
  if (validator.isEmpty(req.body.number)) validationErrors.push({
    msg: 'Phone number is required.'
  });
  if (validator.isEmpty(req.body.message)) validationErrors.push({
    msg: 'Message cannot be blank.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/api/twilio');
  }

  var message = {
    to: req.body.number,
    from: '+13472235148',
    body: req.body.message
  };
  twilio.messages.create(message).then(function (sentMessage) {
    req.flash('success', {
      msg: "Text send to ".concat(sentMessage.to)
    });
    res.redirect('/api/twilio');
  })["catch"](next);
};
/**
 * Get /api/twitch
 */


exports.getTwitch = /*#__PURE__*/function () {
  var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res, next) {
    var token, twitchID, getUser, getFollowers, yourTwitchUser, otherTwitchUser, twitchFollowers;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            token = req.user.tokens.find(function (token) {
              return token.kind === 'twitch';
            });
            twitchID = req.user.twitch;

            getUser = function getUser(userID) {
              return axios.get("https://api.twitch.tv/helix/users?id=".concat(userID), {
                headers: {
                  Authorization: "Bearer ".concat(token.accessToken)
                }
              }).then(function (_ref13) {
                var data = _ref13.data;
                return data;
              })["catch"](function (err) {
                return Promise.reject(new Error("There was an error while getting user data ".concat(err)));
              });
            };

            getFollowers = function getFollowers() {
              return axios.get("https://api.twitch.tv/helix/users/follows?to_id=".concat(twitchID), {
                headers: {
                  Authorization: "Bearer ".concat(token.accessToken)
                }
              }).then(function (_ref14) {
                var data = _ref14.data;
                return data;
              })["catch"](function (err) {
                return Promise.reject(new Error("There was an error while getting followers ".concat(err)));
              });
            };

            _context6.prev = 4;
            _context6.next = 7;
            return getUser(twitchID);

          case 7:
            yourTwitchUser = _context6.sent;
            _context6.next = 10;
            return getUser(44322889);

          case 10:
            otherTwitchUser = _context6.sent;
            _context6.next = 13;
            return getFollowers();

          case 13:
            twitchFollowers = _context6.sent;
            res.render('api/twitch', {
              title: 'Twitch API',
              yourTwitchUserData: yourTwitchUser.data[0],
              otherTwitchUserData: otherTwitchUser.data[0],
              twitchFollowers: twitchFollowers
            });
            _context6.next = 20;
            break;

          case 17:
            _context6.prev = 17;
            _context6.t0 = _context6["catch"](4);
            next(_context6.t0);

          case 20:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[4, 17]]);
  }));

  return function (_x16, _x17, _x18) {
    return _ref12.apply(this, arguments);
  };
}();
/**
 * GET /api/clockwork
 * Clockwork SMS API example.
 */


exports.getClockwork = function (req, res) {
  res.render('api/clockwork', {
    title: 'Clockwork SMS API'
  });
};
/**
 * POST /api/clockwork
 * Send a text message using Clockwork SMS
 */


exports.postClockwork = function (req, res, next) {
  var message = {
    To: req.body.telephone,
    From: 'Hackathon',
    Content: 'Hello from the Hackathon Starter'
  };
  clockwork.sendSms(message, function (err, responseData) {
    if (err) {
      return next(err.errDesc);
    }

    req.flash('success', {
      msg: "Text sent to ".concat(responseData.responses[0].to)
    });
    res.redirect('/api/clockwork');
  });
};
/**
 * GET /api/chart
 * Chart example.
 */


exports.getChart = /*#__PURE__*/function () {
  var _ref15 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res, next) {
    var url;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=compact&apikey=".concat(process.env.ALPHA_VANTAGE_KEY);
            axios.get(url).then(function (response) {
              var arr = response.data['Time Series (Daily)'];
              var dates = [];
              var closing = []; // stock closing value

              var keys = Object.getOwnPropertyNames(arr);

              for (var i = 0; i < 100; i++) {
                dates.push(keys[i]);
                closing.push(arr[keys[i]]['4. close']);
              } // reverse so dates appear from left to right


              dates.reverse();
              closing.reverse();
              dates = JSON.stringify(dates);
              closing = JSON.stringify(closing);
              res.render('api/chart', {
                title: 'Chart',
                dates: dates,
                closing: closing
              });
            })["catch"](function (err) {
              next(err);
            });

          case 2:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x19, _x20, _x21) {
    return _ref15.apply(this, arguments);
  };
}();
/**
 * GET /api/instagram
 * Instagram API example.
 */


exports.getInstagram = /*#__PURE__*/function () {
  var _ref16 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res, next) {
    var token, userSelfMediaRecentAsync, myRecentMedia;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            token = req.user.tokens.find(function (token) {
              return token.kind === 'instagram';
            });
            ig.use({
              client_id: process.env.INSTAGRAM_ID,
              client_secret: process.env.INSTAGRAM_SECRET
            });
            ig.use({
              access_token: token.accessToken
            });
            _context8.prev = 3;
            userSelfMediaRecentAsync = promisify(ig.user_self_media_recent);
            _context8.next = 7;
            return userSelfMediaRecentAsync();

          case 7:
            myRecentMedia = _context8.sent;
            res.render('api/instagram', {
              title: 'Instagram API',
              myRecentMedia: myRecentMedia
            });
            _context8.next = 14;
            break;

          case 11:
            _context8.prev = 11;
            _context8.t0 = _context8["catch"](3);
            next(_context8.t0);

          case 14:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[3, 11]]);
  }));

  return function (_x22, _x23, _x24) {
    return _ref16.apply(this, arguments);
  };
}();
/**
 * GET /api/paypal
 * PayPal SDK example.
 */


exports.getPayPal = function (req, res, next) {
  paypal.configure({
    mode: 'sandbox',
    client_id: process.env.PAYPAL_ID,
    client_secret: process.env.PAYPAL_SECRET
  });
  var paymentDetails = {
    intent: 'sale',
    payer: {
      payment_method: 'paypal'
    },
    redirect_urls: {
      return_url: process.env.PAYPAL_RETURN_URL,
      cancel_url: process.env.PAYPAL_CANCEL_URL
    },
    transactions: [{
      description: 'Hackathon Starter',
      amount: {
        currency: 'USD',
        total: '1.99'
      }
    }]
  };
  paypal.payment.create(paymentDetails, function (err, payment) {
    if (err) {
      return next(err);
    }

    var links = payment.links,
        id = payment.id;
    req.session.paymentId = id;

    for (var i = 0; i < links.length; i++) {
      if (links[i].rel === 'approval_url') {
        res.render('api/paypal', {
          approvalUrl: links[i].href
        });
      }
    }
  });
};
/**
 * GET /api/paypal/success
 * PayPal SDK example.
 */


exports.getPayPalSuccess = function (req, res) {
  var paymentId = req.session.paymentId;
  var paymentDetails = {
    payer_id: req.query.PayerID
  };
  paypal.payment.execute(paymentId, paymentDetails, function (err) {
    res.render('api/paypal', {
      result: true,
      success: !err
    });
  });
};
/**
 * GET /api/paypal/cancel
 * PayPal SDK example.
 */


exports.getPayPalCancel = function (req, res) {
  req.session.paymentId = null;
  res.render('api/paypal', {
    result: true,
    canceled: true
  });
};
/**
 * GET /api/lob
 * Lob API example.
 */


exports.getLob = /*#__PURE__*/function () {
  var _ref17 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res, next) {
    var recipientName, addressTo, addressFrom, lookupZip, createAndMailLetter, uspsLetter, zipDetails;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            if (req.user) {
              recipientName = req.user.profile.name;
            } else {
              recipientName = 'John Doe';
            }

            addressTo = {
              name: recipientName,
              address_line1: '123 Main Street',
              address_city: 'New York',
              address_state: 'NY',
              address_zip: '94107'
            };
            addressFrom = {
              name: 'Hackathon Starter',
              address_line1: '123 Test Street',
              address_line2: 'Unit 200',
              address_city: 'Chicago',
              address_state: 'IL',
              address_zip: '60012',
              address_country: 'US'
            };

            lookupZip = function lookupZip() {
              return lob.usZipLookups.lookup({
                zip_code: '94107'
              }).then(function (zipdetails) {
                return zipdetails;
              })["catch"](function (error) {
                return Promise.reject(new Error("Could not get zip code details: ".concat(error)));
              });
            };

            createAndMailLetter = function createAndMailLetter() {
              return lob.letters.create({
                description: 'My First Class Letter',
                to: addressTo,
                from: addressFrom,
                // file: minified version of https://github.com/lob/lob-node/blob/master/examples/html/letter.html with slight changes as an example
                file: "<html><head><meta charset=\"UTF-8\"><style>body{width:8.5in;height:11in;margin:0;padding:0}.page{page-break-after:always;position:relative;width:8.5in;height:11in}.page-content{position:absolute;width:8.125in;height:10.625in;left:1in;top:1in}.text{position:relative;left:20px;top:3in;width:6in;font-size:14px}</style></head>\n          <body><div class=\"page\"><div class=\"page-content\"><div class=\"text\">\n          Hello ".concat(addressTo.name, ", <p> We would like to welcome you to the community! Thanks for being a part of the team! <p><p> Cheer,<br>").concat(addressFrom.name, "\n          </div></div></div></body></html>"),
                color: false
              }).then(function (letter) {
                return letter;
              })["catch"](function (error) {
                return Promise.reject(new Error("Could not create and send letter: ".concat(error)));
              });
            };

            _context9.prev = 5;
            _context9.next = 8;
            return createAndMailLetter();

          case 8:
            uspsLetter = _context9.sent;
            _context9.next = 11;
            return lookupZip();

          case 11:
            zipDetails = _context9.sent;
            res.render('api/lob', {
              title: 'Lob API',
              zipDetails: zipDetails,
              uspsLetter: uspsLetter
            });
            _context9.next = 18;
            break;

          case 15:
            _context9.prev = 15;
            _context9.t0 = _context9["catch"](5);
            next(_context9.t0);

          case 18:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[5, 15]]);
  }));

  return function (_x25, _x26, _x27) {
    return _ref17.apply(this, arguments);
  };
}();
/**
 * GET /api/upload
 * File Upload API example.
 */


exports.getFileUpload = function (req, res) {
  res.render('api/upload', {
    title: 'File Upload'
  });
};

exports.postFileUpload = function (req, res) {
  req.flash('success', {
    msg: 'File was uploaded successfully.'
  });
  res.redirect('/api/upload');
};
/**
 * GET /api/pinterest
 * Pinterest API example.
 */


exports.getPinterest = function (req, res, next) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'pinterest';
  });
  axios.get("https://api.pinterest.com/v1/me/boards?access_token=".concat(token.accessToken)).then(function (response) {
    res.render('api/pinterest', {
      title: 'Pinterest API',
      boards: response.data.data
    });
  })["catch"](function (error) {
    next(error);
  });
};
/**
 * POST /api/pinterest
 * Create a pin.
 */


exports.postPinterest = function (req, res, next) {
  var validationErrors = [];
  if (validator.isEmpty(req.body.board)) validationErrors.push({
    msg: 'Board is required.'
  });
  if (validator.isEmpty(req.body.note)) validationErrors.push({
    msg: 'Note cannot be blank.'
  });
  if (validator.isEmpty(req.body.image_url)) validationErrors.push({
    msg: 'Image URL cannot be blank.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/api/pinterest');
  }

  var token = req.user.tokens.find(function (token) {
    return token.kind === 'pinterest';
  });
  var formData = {
    board: req.body.board,
    note: req.body.note,
    link: req.body.link,
    image_url: req.body.image_url
  };
  axios.post("https://api.pinterest.com/v1/pins/?access_token=".concat(token.accessToken), formData).then(function () {
    req.flash('success', {
      msg: 'Pin created'
    });
    res.redirect('/api/pinterest');
  })["catch"](function (error) {
    req.flash('errors', {
      msg: error.response.data.message
    });
    res.redirect('/api/pinterest');
  });
};

exports.getHereMaps = function (req, res) {
  var imageMapURL = "https://image.maps.api.here.com/mia/1.6/mapview?app_id=".concat(process.env.HERE_APP_ID, "&app_code=").concat(process.env.HERE_APP_CODE, "&poix0=47.6516216,-122.3498897;white;black;15;Fremont Troll&poix1=47.6123335,-122.3314332;white;black;15;Seattle Art Museum&poix2=47.6162956,-122.3555097;white;black;15;Olympic Sculpture Park&poix3=47.6205099,-122.3514661;white;black;15;Space Needle&c=47.6176371,-122.3344637&u=1500&vt=1&&z=13&h=500&w=800&");
  res.render('api/here-maps', {
    app_id: process.env.HERE_APP_ID,
    app_code: process.env.HERE_APP_CODE,
    title: 'Here Maps API',
    imageMapURL: imageMapURL
  });
};

exports.getGoogleMaps = function (req, res) {
  res.render('api/google-maps', {
    title: 'Google Maps API',
    google_map_api_key: process.env.GOOGLE_MAP_API_KEY
  });
};

exports.getGoogleDrive = function (req, res) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'google';
  });
  var authObj = new google.auth.OAuth2({
    access_type: 'offline'
  });
  authObj.setCredentials({
    access_token: token.accessToken
  });
  var drive = google.drive({
    version: 'v3',
    auth: authObj
  });
  drive.files.list({
    fields: 'files(iconLink, webViewLink, name)'
  }, function (err, response) {
    if (err) return console.log("The API returned an error: ".concat(err));
    res.render('api/google-drive', {
      title: 'Google Drive API',
      files: response.data.files
    });
  });
};

exports.getGoogleSheets = function (req, res) {
  var token = req.user.tokens.find(function (token) {
    return token.kind === 'google';
  });
  var authObj = new google.auth.OAuth2({
    access_type: 'offline'
  });
  authObj.setCredentials({
    access_token: token.accessToken
  });
  var sheets = google.sheets({
    version: 'v4',
    auth: authObj
  });
  var url = 'https://docs.google.com/spreadsheets/d/12gm6fRAp0bC8TB2vh7sSPT3V75Ug99JaA9L0PqiWS2s/edit#gid=0';
  var re = /spreadsheets\/d\/([a-zA-Z0-9-_]+)/;
  var id = url.match(re)[1];
  sheets.spreadsheets.values.get({
    spreadsheetId: id,
    range: 'Class Data!A1:F'
  }, function (err, response) {
    if (err) return console.log("The API returned an error: ".concat(err));
    res.render('api/google-sheets', {
      title: 'Google Sheets API',
      values: response.data.values
    });
  });
};