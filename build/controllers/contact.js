"use strict";

var validator = require('validator');

var nodemailer = require('nodemailer');
/**
 * GET /contact
 * Contact form page.
 */


exports.getContact = function (req, res) {
  var unknownUser = !req.user;
  res.render('contact', {
    title: 'Contact',
    unknownUser: unknownUser
  });
};
/**
 * POST /contact
 * Send a contact form via Nodemailer.
 */


exports.postContact = function (req, res) {
  var validationErrors = [];
  var fromName;
  var fromEmail;

  if (!req.user) {
    if (validator.isEmpty(req.body.name)) validationErrors.push({
      msg: 'Please enter your name'
    });
    if (!validator.isEmail(req.body.email)) validationErrors.push({
      msg: 'Please enter a valid email address.'
    });
  }

  if (validator.isEmpty(req.body.message)) validationErrors.push({
    msg: 'Please enter your message.'
  });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/contact');
  }

  if (!req.user) {
    fromName = req.body.name;
    fromEmail = req.body.email;
  } else {
    fromName = req.user.profile.name || '';
    fromEmail = req.user.email;
  }

  var transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
      user: process.env.SENDGRID_USER,
      pass: process.env.SENDGRID_PASSWORD
    }
  });
  var mailOptions = {
    to: 'your@email.com',
    from: "".concat(fromName, " <").concat(fromEmail, ">"),
    subject: 'Contact Form | Hackathon Starter',
    text: req.body.message
  };
  return transporter.sendMail(mailOptions).then(function () {
    req.flash('success', {
      msg: 'Email has been sent successfully!'
    });
    res.redirect('/contact');
  })["catch"](function (err) {
    if (err.message === 'self signed certificate in certificate chain') {
      console.log('WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.');
      transporter = nodemailer.createTransport({
        service: 'SendGrid',
        auth: {
          user: process.env.SENDGRID_USER,
          pass: process.env.SENDGRID_PASSWORD
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      return transporter.sendMail(mailOptions);
    }

    console.log('ERROR: Could not send contact email after security downgrade.\n', err);
    req.flash('errors', {
      msg: 'Error sending the message. Please try again shortly.'
    });
    return false;
  }).then(function (result) {
    if (result) {
      req.flash('success', {
        msg: 'Email has been sent successfully!'
      });
      return res.redirect('/contact');
    }
  })["catch"](function (err) {
    console.log('ERROR: Could not send contact email.\n', err);
    req.flash('errors', {
      msg: 'Error sending the message. Please try again shortly.'
    });
    return res.redirect('/contact');
  });
};