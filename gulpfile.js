var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    del = require('del'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');


// Define reusable paths
var path = {
  src: 'src/views/front',
  dist: 'public/front',
  src_scss: 'src/views/front/scss',
  src_js: 'src/views/front/js',
  src_js_vendor: 'src/views/front/vendor/js',
  src_css_vendor: 'src/views/front/vendor/css',
  dist_js: 'public/front/js',
  dist_css: 'public/front/css',
}


// Sass compiling

// Expanded
gulp.task('sass:expanded', () => {
  const options = {
    outputStyle: 'expanded',
    precision: 10 // rounding of css color values, etc..
  };
  return gulp.src(path.src_scss + '/theme.scss')
    .pipe(sass(options).on('error', sass.logError))
    .pipe(autoprefixer({
      overrideBrowserslist: ['last 2 versions', 'ie 11'],
      cascade: false
    }))
    .pipe(gulp.dest(path.dist_css))
    .pipe(browserSync.stream()); // Inject css into browser
});

// Minified
gulp.task('sass:minified', () => {
  const options = {
    outputStyle: 'compressed',
    precision: 10 // rounding of css color values, etc..
  };
  return gulp.src(path.src_scss + '/theme.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(options).on('error', sass.logError))
    .pipe(autoprefixer({
      overrideBrowserslist: ['last 2 versions', 'ie 11'],
      cascade: false
    }))
    .pipe(rename({ suffix: '.min'}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.dist_css))
    .pipe(browserSync.stream()); // Inject css into browser
});


// Concatinate various vendor css files

gulp.task('concat:css', () => {
  return gulp.src([
    path.src_css_vendor + '/*.css'
  ])
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest(path.dist_css))
    .pipe(browserSync.stream()); // Injects css into browser
});


// Concatinate various vendor js files

gulp.task('concat:js', () => {
  return gulp.src([
      path.src_js_vendor + '/jquery.min.js',
      path.src_js_vendor + '/popper.min.js',
      path.src_js_vendor + '/*.js',
      '!' + path.src_js_vendor + '/modernizr.min.js',
      '!' + path.src_js_vendor + '/card.min.js'
    ])
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest(path.dist_js))
    .on('end', () => {
      reload(); // One time browser reload at end of concatination
    });
});


// Move some vendor js files to dist/js folder

gulp.task('move:js', () => {
  return gulp.src([
      path.src_js_vendor + '/modernizr.min.js',
      path.src_js_vendor + '/card.min.js'
    ])
    .pipe(gulp.dest(path.dist_js));
});


// Uglify (minify) our own scripts.js file

gulp.task('uglify:js', () => {
  return gulp.src(path.src_js + '/theme.js')
    .pipe(rename('theme.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(path.dist_js))
    .on('end', () => {
      reload(); // One time browser reload at end of uglification (minification)
    });
});


// Clean certain files/folders from dist directory. Runs before compilation of new files. See 'default' task at the most bottom of this file

gulp.task('clean', () => {
  return del([
    path.dist_css,
    path.dist_js,
    path.dist + '/components',
    path.dist + '/docs',
    path.dist + '/*.html'
  ]);
});


// Watcher

gulp.task('watch', () => {
  global.watch = true; // Let the pug task know that we are running in "watch" mode

  // BrowserSync
  browserSync.init({
    server: {
      baseDir: path.dist,
    },
    open: true, // or "local"
  });
  gulp.watch(path.src_css_vendor + '/*.css', gulp.series('concat:css'));
  gulp.watch(path.src_scss + '/**/*.scss', gulp.series('sass:minified', 'sass:expanded'));
  gulp.watch(path.src_js_vendor + '/*.js', gulp.series('concat:js'));
  gulp.watch(path.src_js + '/*.js', gulp.series('uglify:js'));
});


// Default task - the dependent tasks will run in parallelle

gulp.task(
  'default',
  gulp.series('clean', gulp.parallel('concat:js', 'move:js', 'concat:css', 'uglify:js', 'sass:minified', 'sass:expanded'), 'watch')
);
