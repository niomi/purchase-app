import dotenv from 'dotenv';
import Dish from '../models/Dish';

dotenv.config({ path: '.env' });

const createDish = async (inputData) => {
  const dish = new Dish({
    title: inputData.title,
    description: inputData.description,
    photo: inputData.photo ? `/${process.env.DIR_IMAGE_STORAGE}/${inputData.photo.filename}` : '',
    ingredients: inputData.ingredients,
    allergens: inputData.allergens,
    type: inputData.type
  });

  try {
    return await dish.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateDish = async (inputData) => {
  const dish = await Dish.findById(inputData.id);

  if (!dish) {
    throw new Error(`Impossible de trouver la recette avec id: ${inputData.id}`);
  }

  if (inputData.title) {
    dish.title = inputData.title;
  }

  if (inputData.description) {
    dish.description = inputData.description;
  }

  dish.photo = inputData.photo ? `/${process.env.DIR_IMAGE_STORAGE}/${inputData.photo.filename}` : '';

  if (inputData.ingredients) {
    dish.ingredients = inputData.ingredients;
  }

  if (inputData.allergens) {
    dish.allergens = inputData.allergens;
  }

  if (inputData.type) {
    dish.type = inputData.type;
  }

  if (inputData.quantity !== undefined) {
    dish.quantity = inputData.quantity;
  }

  try {
    return await dish.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getDishes = async (filters, sortField, sortOrder, limit) => {
  try {
    return await Dish.find(filters)
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findDishByConditions = async (conditions) => {
  try {
    return await Dish
      .findOne(conditions);
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createDish,
  getDishes,
  updateDish,
  findDishByConditions
};
