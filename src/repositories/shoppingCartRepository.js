import ShoppingCart from '../models/ShoppingCart';

const createShoppingCart = async (newItemCart, shoppingCartId, deliveryAmount = 1.5, discountRate = 0) => {
  let shoppingCart;
  if (!shoppingCartId) {
    shoppingCart = new ShoppingCart();
    shoppingCart.deliveryAmount = deliveryAmount;
    shoppingCart.discountRate = discountRate;
  } else {
    shoppingCart = await ShoppingCart.findById(shoppingCartId);
    if (!shoppingCart) {
      throw new Error(`Impossible de trouver le panier: ${shoppingCartId}`);
    }
  }

  const shoppingCartItems = shoppingCart.items;

  shoppingCartItems.push(newItemCart);

  shoppingCart.items = shoppingCartItems;

  try {
    return await shoppingCart.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addItemToShoppingCart = async (shoppingCartId, newItemCart, index) => {
  const shoppingCart = await ShoppingCart.findById(shoppingCartId);

  if (!shoppingCart) {
    throw new Error(`Impossible de trouver le panier: ${shoppingCartId}`);
  }

  const shoppingCartItems = shoppingCart.items;
  shoppingCartItems.splice(index, 1, newItemCart);
  shoppingCart.items = shoppingCartItems;

  try {
    return await shoppingCart.save();
  } catch (err) {
    return err;
  }
};

const removeItemToShoppingCartFromIndex = async (shoppingCartId, index) => {
  const shoppingCart = await ShoppingCart.findById(shoppingCartId);

  if (!shoppingCart) {
    throw new Error(`Impossible de trouver le panier: ${shoppingCartId}`);
  }

  const shoppingCartItems = shoppingCart.items;
  shoppingCartItems.splice(index, 1);
  shoppingCart.items = shoppingCartItems;

  try {
    if (!shoppingCart.items || shoppingCart.items.length < 1) {
      ShoppingCart.deleteOne({ _id: shoppingCart._id });

      return {};
    }

    return await shoppingCart.save();
  } catch (err) {
    return err;
  }
};

const linkOrderToShoppingCart = async (shoppingCartId, orderId) => {
  try {
    const shoppingCart = await ShoppingCart
      .findOneAndUpdate({ _id: shoppingCartId }, { orderId, status: 'ORDERED' }, { new: true });

    if (!shoppingCart) {
      throw new Error(`Impossible de trouver le panier avec id: ${shoppingCartId}`);
    }

    return shoppingCart;
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createShoppingCart,
  addItemToShoppingCart,
  removeItemToShoppingCartFromIndex,
  linkOrderToShoppingCart
};
