import User from '../models/User';

const newUser = (inputData) => {
  const user = new User({
    email: inputData.email,
    phoneNumber: inputData.phoneNumber,
    password: inputData.password,
    profile: {
      firstName: inputData.firstName,
      lastName: inputData.lastName
    }
  });

  return user;
};

const createUser = async (inputData) => {
  const user = newUser(inputData);

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateUser = async (inputData) => {
  const user = await User
    .findById(inputData.id);
  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${inputData.id}`);
  }

  if (inputData.profile) {
    user.profile = inputData.profile;
  }

  if (inputData.email) {
    user.email = inputData.email;
  }

  if (inputData.phoneNumber) {
    user.phoneNumber = inputData.phoneNumber;
  }

  if (inputData.password) {
    user.password = inputData.password;
  }

  if (inputData.deliveryAmount !== undefined) {
    user.deliveryAmount = inputData.deliveryAmount;
  }

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateAddress = async (userId, shippingAddress) => {
  const user = await User.findById(userId);
  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${userId}`);
  }

  const { shippingAddresses } = user;

  if (shippingAddress.id) {
    const oldShippingAddressIndex = shippingAddresses.findIndex((address) => String(address._id) === shippingAddress.id);
    if (oldShippingAddressIndex !== undefined) {
      shippingAddresses.splice(oldShippingAddressIndex, 1);
    }
  }

  shippingAddresses.push(shippingAddress);

  user.shippingAddresses = shippingAddresses;

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const deleteAddress = async (userId, shippingAddressId) => {
  const user = await User.findById(userId);
  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${userId}`);
  }

  const { shippingAddresses } = user;

  const addressToDeleteIndex = shippingAddresses.findIndex((address) => String(address._id) === shippingAddressId);
  if (addressToDeleteIndex !== undefined) {
    shippingAddresses.splice(addressToDeleteIndex, 1);
  }

  user.shippingAddresses = shippingAddresses;

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getUsers = async (limit, sortField, sortOrder) => {
  try {
    return await User.find()
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findUserByConditions = async (conditions) => {
  try {
    return await User
      .findOne(conditions);
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  newUser,
  createUser,
  getUsers,
  updateUser,
  findUserByConditions,
  updateAddress,
  deleteAddress
};
