import MenuCategoryItem from '../../models/Menu/MenuCategoryItem';
import Dish from '../../models/Dish';

const createMenuCategoryItem = async (inputData) => {
  const menuCategoryItem = new MenuCategoryItem({
    title: inputData.title,
    description: inputData.description,
    photo: inputData.photo,
    price: inputData.price,
    type: inputData.type,
  });

  try {
    return await menuCategoryItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateMenuCategoryItem = async (inputData) => {
  let update = {};
  if (inputData.title) {
    update = {
      ...update,
      title: inputData.title
    };
  }

  if (inputData.description) {
    update = {
      ...update,
      description: inputData.description
    };
  }

  if (inputData.photo) {
    update = {
      ...update,
      photo: inputData.photo
    };
  }

  if (inputData.price) {
    update = {
      ...update,
      price: inputData.price
    };
  }

  try {
    const menuCategoryItem = await MenuCategoryItem
      .findOneAndUpdate({ _id: inputData.id }, update, { new: true });
    if (!menuCategoryItem) {
      throw new Error(`Impossible de trouver l'element de categorie de menu avec id: ${inputData.id}`);
    }

    return menuCategoryItem;
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getMenuCategoryItems = async (limit, sortField, sortOrder) => {
  try {
    return await MenuCategoryItem.find()
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findMenuCategoryItemByConditions = async (conditions) => {
  try {
    return await MenuCategoryItem
      .findOne(conditions);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const checkQuantityByDish = async (id, quantity) => {
  const menuCategoryItem = await MenuCategoryItem
    .findById(id)
    .populate({
      path: 'meals.dishes.dish',
      model: Dish
    });

  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${id}`);
  }

  const result = [];
  menuCategoryItem.meals.forEach((meal) => {
    meal.dishes.forEach((dish) => {
      const data = {
        id: dish.dish.id,
        title: dish.dish.title,
        isQuantityEnough: dish.dish.quantity >= quantity,
        quantityLeft: dish.dish.quantity
      };
      result.push(data);
    });
  });

  return result;
};

const addMeal = async (id, meal) => {
  const menuCategoryItem = await MenuCategoryItem
    .findById(id);

  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${id}`);
  }

  const { meals } = menuCategoryItem;

  meals.push({
    title: meal.title,
    order: meal.order,
    link: meal.link,
    dishes: meal.dishes ? meal.dishes : []
  });

  menuCategoryItem.meals = meals;

  try {
    return await menuCategoryItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateMeal = async (id, mealInput) => {
  const menuCategoryItem = await MenuCategoryItem
    .findById(id);

  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${id}`);
  }

  const menuCategoryItemMeals = menuCategoryItem.meals;
  const mealIndex = menuCategoryItemMeals.findIndex((elt) => String(elt._id) === mealInput.id);

  const meal = menuCategoryItemMeals[mealIndex];

  meal.link = mealInput.type;
  meal.title = mealInput.title;
  meal.order = mealInput.order;

  menuCategoryItemMeals.splice(mealIndex, 1, meal);
  menuCategoryItem.meals = menuCategoryItemMeals;

  try {
    return await menuCategoryItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateSimpleMenuCategoryItem = async (inputData) => {
  const menuItem = await MenuItem
    .findById(inputData.id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuItemItems = menuItem.items;
  const menuItemCategoryIndex = menuItemItems.findIndex((elt) => String(elt._id) === inputData.categoryId);

  const menuItemCategory = menuItemItems[menuItemCategoryIndex];

  menuItemItems.splice(menuItemCategoryIndex, 1, menuItemCategory);
  menuItem.items = menuItemItems;

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addDish = async (id, mealId, mealInfo) => {
  const menuCategoryItem = await MenuCategoryItem
    .findById(id);

  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${id}`);
  }

  const menuCategoryItemMeals = menuCategoryItem.meals;
  const mealIndex = menuCategoryItemMeals.findIndex((elt) => String(elt._id) === mealId);

  const meal = menuCategoryItemMeals[mealIndex];

  const mealDishes = meal.dishes;

  mealDishes.push(mealInfo);

  meal.dishes = mealDishes;

  menuCategoryItemMeals.splice(mealIndex, 1, meal);

  menuCategoryItem.meals = menuCategoryItemMeals;

  try {
    return await menuCategoryItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateDish = async (id, mealId, mealInfo) => {
  const menuCategoryItem = await MenuCategoryItem
    .findById(id);

  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${id}`);
  }

  const menuCategoryItemMeals = menuCategoryItem.meals;
  const mealIndex = menuCategoryItemMeals.findIndex((elt) => String(elt._id) === mealId);

  const meal = menuCategoryItemMeals[mealIndex];

  const mealDishes = meal.dishes;
  const dishIndex = mealDishes.findIndex((elt) => String(elt._id) === mealInfo.id);

  const dish = mealDishes[dishIndex];

  dish.dish = mealInfo.dish;
  dish.additionalPrice = mealInfo.additionalPrice;

  mealDishes.splice(dishIndex, 1, dish);

  meal.dishes = mealDishes;

  menuCategoryItemMeals.splice(mealIndex, 1, meal);

  menuCategoryItem.meals = menuCategoryItemMeals;

  try {
    return await menuCategoryItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createMenuCategoryItem,
  getMenuCategoryItems,
  updateMenuCategoryItem,
  findMenuCategoryItemByConditions,
  addMeal,
  updateMeal,
  checkQuantityByDish,
  addDish,
  updateDish
};
