import dotenv from 'dotenv';
import MenuCategory from '../../models/Menu/MenuCategory';
import MenuCategoryItem from '../../models/Menu/MenuCategoryItem';
import Dish from '../../models/Dish';

dotenv.config({ path: '.env' });

const createMenuCategory = async (inputData) => {
  const menuCategory = new MenuCategory({
    title: inputData.title,
    subtitle: inputData.subtitle,
    photo: inputData.photo ? `/${process.env.DIR_IMAGE_STORAGE}/${inputData.photo.filename}` : '',
    order: inputData.order
  });

  try {
    return await menuCategory.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateMenuCategory = async (inputData) => {
  const menuCategory = await MenuCategory.findById(inputData.id);

  if (!menuCategory) {
    throw new Error(`Impossible de trouver la categorie avec id: ${inputData.id}`);
  }

  if (inputData.title) {
    menuCategory.title = inputData.title;
  }

  if (inputData.subtitle) {
    menuCategory.subtitle = inputData.subtitle;
  }

  menuCategory.photo = inputData.photo ? `/${process.env.DIR_IMAGE_STORAGE}/${inputData.photo.filename}` : '';

  if (inputData.order) {
    menuCategory.order = inputData.order;
  }

  menuCategory.activated = inputData.activated === 'on';

  try {
    return await menuCategory.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getMenuCategories = async (filters, sortField, sortOrder, limit) => {
  try {
    return await MenuCategory.find(filters)
      .populate({
        path: 'items.item',
        model: MenuCategoryItem,
        populate: {
          path: 'meals.dishes.dish',
          model: Dish
        }
      })
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findMenuCategoryByConditions = async (conditions) => {
  try {
    return await MenuCategory
      .findOne(conditions);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addMenuCategoryItem = async (id, itemId, itemOrder) => {
  const menuCategory = await MenuCategory
    .findById(id);

  if (!menuCategory) {
    throw new Error(`Impossible de trouver la categorie du menu avec id: ${id}`);
  }

  const { items } = menuCategory;
  items.push({ item: itemId, order: itemOrder });

  menuCategory.items = items;

  try {
    return await menuCategory.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createMenuCategory,
  getMenuCategories,
  updateMenuCategory,
  findMenuCategoryByConditions,
  addMenuCategoryItem
};
