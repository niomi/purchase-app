import MenuItem from '../../models/Menu/MenuItem';
import MenuCategory from '../../models/Menu/MenuCategory';
import MenuCategoryItem from '../../models/Menu/MenuCategoryItem';
import Dish from '../../models/Dish';

const getMenuItems = async (filters, sortField, sortOrder, limit) => {
  try {
    return await MenuItem.find(filters)
      .sort({ [sortField]: sortOrder })
      .limit(limit)
      .populate({
        path: 'items.category',
        model: MenuCategory,
      })
      .populate({
        path: 'items.menuCategoryItems.item',
        model: MenuCategoryItem,
        populate: {
          path: 'meals.dishes.dish',
          model: Dish
        }
      });
  } catch (err) {
    console.error(err);
    return err;
  }
};

const createMenuItem = async (inputData) => {
  const menuItem = new MenuItem({
    availableAt: inputData.availableAt
  });

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addMenuItemCategory = async (inputData) => {
  const menuItem = await MenuItem.findById(inputData.id);

  const newItem = {
    category: inputData.category,
    order: inputData.order
  };

  const { items } = menuItem;

  items.push(newItem);

  menuItem.items = items;

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const editMenuItemCategory = async (inputData) => {
  const menuItem = await MenuItem.findById(inputData.id);

  const newItem = {
    category: inputData.category,
    order: inputData.order
  };

  const { items } = menuItem;

  items.push(newItem);

  menuItem.items = items;

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateMenuItemCategory = async (inputData) => {
  const menuItem = MenuItem.find(inputData.id);

  const newItem = {
    category: inputData.category,
    order: inputData.order
  };

  const { items } = menuItem;

  items.push(newItem);

  menuItem.items = items;

  try {
    console.log('inputData ', inputData, 'menuItem ', menuItem);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addSimpleMenuCategoryItem = async (inputData) => {
  const menuItem = await MenuItem
    .findById(inputData.id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuItemItems = menuItem.items;
  const menuItemCategoryIndex = menuItemItems.findIndex((elt) => String(elt._id) === inputData.categoryId);
  const newMenuItemCategory = menuItemItems[menuItemCategoryIndex];

  const menuCategoryItem = { item: inputData.menuCategoryItemId };

  newMenuItemCategory.menuCategoryItems.push(menuCategoryItem);

  menuItemItems.splice(menuItemCategoryIndex, 1, newMenuItemCategory);
  menuItem.items = menuItemItems;

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateSimpleMenuCategoryItem = async (inputData) => {
  const menuItem = await MenuItem
    .findById(inputData.id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuItemItems = menuItem.items;
  const menuItemCategoryIndex = menuItemItems.findIndex((elt) => String(elt._id) === inputData.categoryId);

  const menuItemCategory = menuItemItems[menuItemCategoryIndex];

  menuItemItems.splice(menuItemCategoryIndex, 1, menuItemCategory);
  menuItem.items = menuItemItems;

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const deleteMenuCategoryItem = async (inputData) => {
  const menuItem = await MenuItem
    .findById(inputData.id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  console.log(inputData);

  try {
    return await menuItem.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createMenuItem,
  getMenuItems,
  addMenuItemCategory,
  updateMenuItemCategory,
  editMenuItemCategory,
  addSimpleMenuCategoryItem,
  updateSimpleMenuCategoryItem,
  deleteMenuCategoryItem
};
