import AdminUser from '../models/AdminUser';

const newAdminUser = (inputData) => {
  const adminUser = new AdminUser({
    email: inputData.email,
    username: inputData.username,
    phoneNumber: inputData.phoneNumber,
    password: inputData.password,
    firstName: inputData.firstName,
    lastName: inputData.lastName,
    job: inputData.job
  });

  return adminUser;
};

const createAdminUser = async (inputData) => {
  const adminUser = newAdminUser(inputData);

  try {
    return await adminUser.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateAdminUser = async (inputData) => {

  const adminUser = await AdminUser
    .findById(inputData.id);
  if (!adminUser) {
    throw new Error(`Impossible de trouver l'utilisateur avec id: ${inputData.id}`);
  }

  if (inputData.username) {
    adminUser.username = inputData.username;
  }

  if (inputData.email) {
    adminUser.email = inputData.email;
  }

  if (inputData.phoneNumber) {
    adminUser.phoneNumber = inputData.phoneNumber;
  }

  if (inputData.password) {
    adminUser.password = inputData.password;
  }

  if (inputData.firstName) {
    adminUser.firstName = inputData.firstName;
  }

  if (inputData.lastName) {
    adminUser.lastName = inputData.lastName;
  }

  if (inputData.job) {
    adminUser.job = inputData.job;
  }

  adminUser.activated = inputData.activated === 'on';

  try {
    return await adminUser.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getAdminUsers = async (filters, sortField, sortOrder, limit) => {
  try {
    return await AdminUser.find(filters)
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findAdminUserByConditions = async (conditions) => {
  try {
    return await AdminUser
      .findOne(conditions);
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  newAdminUser,
  createAdminUser,
  getAdminUsers,
  updateAdminUser,
  findAdminUserByConditions,
};
