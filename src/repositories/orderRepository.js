import moment from 'moment';
import Order from '../models/Order';

const createOrder = async (inputData) => {
  const order = new Order({
    menuCategoryItems: inputData.menuCategoryItems,
    subTotalAmount: inputData.subTotalAmount,
    deliveryAmount: inputData.deliveryAmount,
    discountRate: inputData.discountRate,
    discountAmount: inputData.discountAmount,
    creditAmount: inputData.creditAmount,
    totalQuantity: inputData.totalQuantity,
    deliveryInfo: inputData.deliveryInfo,
    customer: inputData.customer
  });

  try {
    return await order.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateOrder = async (inputData) => {
  let update = {};
  if (inputData.menuCategoryItems) {
    update = {
      ...update,
      menuCategoryItems: inputData.menuCategoryItems
    };
  }

  if (inputData.subTotalAmount) {
    update = {
      ...update,
      subTotalAmount: inputData.subTotalAmount
    };
  }

  if (inputData.deliveryAmount) {
    update = {
      ...update,
      deliveryAmount: inputData.deliveryAmount
    };
  }

  if (inputData.creditAmount !== undefined) {
    update = {
      ...update,
      creditAmount: inputData.creditAmount
    };
  }

  if (inputData.discountRate !== undefined) {
    update = {
      ...update,
      discountRate: inputData.discountRate
    };
  }

  if (inputData.discountAmount !== undefined) {
    update = {
      ...update,
      discountAmount: inputData.discountAmount
    };
  }

  if (inputData.totalQuantity) {
    update = {
      ...update,
      totalQuantity: inputData.totalQuantity
    };
  }

  if (inputData.deliveryInfo) {
    update = {
      ...update,
      deliveryInfo: inputData.deliveryInfo
    };
  }

  if (inputData.status) {
    update = {
      ...update,
      status: inputData.status
    };
  }

  if (inputData.couponCode !== undefined) {
    update = {
      ...update,
      couponCode: inputData.couponCode
    };
  }

  try {
    const order = await Order
      .findOneAndUpdate({ _id: inputData.id }, update, { new: true });
    if (!order) {
      throw new Error(`Impossible de trouver la commande de menu avec id: ${inputData.id}`);
    }

    return order;
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updatePaymentMethodOrder = async (id, paymentMethod) => {
  const order = await Order.findById(id);
  if (!order) {
    throw new Error(`Impossible de trouver la commande de menu avec id: ${id}`);
  }

  order.payment.method = paymentMethod;

  try {
    return await order.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const validatePaymentOrder = async (id, data, provider) => {
  const order = await Order.findById(id);
  if (!order) {
    throw new Error(`Impossible de trouver la commande de menu avec id: ${id}`);
  }

  order.payment.purchaseAt = moment();
  order.payment.data = data;
  order.payment.provider = provider;
  order.paymentStatus = 'PAID';

  try {
    return await order.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getOrders = async (filters, sortField, sortOrder, limit) => {
  try {
    return await Order.find(filters)
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    console.error(err);
    return err;
  }
};

export {
  createOrder,
  updateOrder,
  validatePaymentOrder,
  getOrders,
  updatePaymentMethodOrder
};
