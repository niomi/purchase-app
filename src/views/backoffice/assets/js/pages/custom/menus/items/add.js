// Class definition
const KTMenusItemsAdd = (function () {
  // Base element
  let validator;
  let formEl;

  const initValidation = function () {
    validator = formEl.validate({
      // Validation rules
      rules: {
        date: {
          required: true
        },
      },
      messages: {
        date: {
          required: "Ce champ est obligatoire"
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        // KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          url: '/admin/menus/items/create',
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Le menu a été créé!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "/admin/menus/items/list";
                }
              });
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formEl = $('#kt_form');

      initValidation();
      initSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusItemsAdd.init();
});
