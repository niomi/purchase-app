// Class definition
const KTMenusItemsCategoryItemAdd = (function () {
  // Base element
  let validator;
  let formEl;
  let fieldType;
  let fieldDish;
  let groupFieldDish;

  const initValidation = function () {
    validator = formEl.validate({
      // Validation rules
      rules: {
        //order: {
        //  required: true,
        //  number: true
        //},
        title: {
          required: true,
        },
        price: {
          required: true,
          number: true
        },
      },
      messages: {
        //order: {
        //  required: "Ce champ est obligatoire",
        //  number: "Veuillez saisir un chiffre"
        //},
        title: {
          required: "Ce champ est obligatoire",
        },
        price: {
          required: "Ce champ est obligatoire",
          number: "Veuillez saisir un chiffre"
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const menuItemId = document.getElementById('menuItemId').value;
        const menuItemCategoryId = document.getElementById('menuItemCategoryId').value;

        const pathSuffix = fieldType.val() === 'SIMPLE' ? '/offer/add' : '/offer/combined/add';

        formEl.ajaxSubmit({
          url: '/admin/menus/items/' + menuItemId + '/categories/' + menuItemCategoryId + pathSuffix,
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "L'offre a été créé!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "/admin/menus/items/list";
                }
              });
            }
          }
        });
      }
    });
  };

  const activeSimpleForm = function() {
    fieldDish.removeAttr('disabled');
    groupFieldDish.removeAttr('hidden');
  }

  const activeCombinedForm = function() {
    fieldDish.attr('disabled', 'disabled');
    groupFieldDish.attr('hidden', 'hidden');
  }

  const handleFormDisplay = function () {
    fieldType.on('change', function(){
      if (this.value === 'SIMPLE') {
        activeSimpleForm();
      } else {
        activeCombinedForm();
      }
    });
  };

  return {
    init() {
      formEl = $('#add-menu-items-category-offer-form');
      groupFieldDish = $('#group-dish');
      fieldDish = $('#group-dish select');
      fieldType = $('#type');

      initValidation();
      initSubmit();
      handleFormDisplay();
    }
  };
}());

jQuery(document).ready(() => {
  KTMenusItemsCategoryItemAdd.init();
});
