// Class definition
const KTUsersAdd = (function () {
  // Base element
  let validator;
  let formEl;

  const initValidation = function () {
    validator = formEl.validate({
      // Validation rules
      rules: {
        lastName: {
          required: true
        },
        firstName: {
          required: true
        },
        username: {
          required: true,
          minlength: 4
        },
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 6
        },
      },

      messages: {
        lastName: {
          required: "Ce champ est obligatoire"
        },
        firstName: {
          required: "Ce champ est obligatoire"
        },
        username: {
          required: "Ce champ est obligatoire",
          minlength: "Veuillez saisir au moins 5 caractères"
        },
        email: {
          required: "Ce champ est obligatoire",
          email: "Veuillez saisir une adresse email valide"
        },
        password: {
          required: "Ce champ est obligatoire",
          minlength: "Veuillez saisir au moins 6 caractères"
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        // KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          url: '/admin/users/create',
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "L'utilisateur a été créé!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "/admin/users/list";
                }
              });
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formEl = $('#kt_form');

      initValidation();
      initSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTUsersAdd.init();
});
