// Class definition
const KTUsersAdd = (function () {
  let validator;
  let formEl;

  const initValidation = function () {
    validator = formEl.validate({
      rules: {
        lastName: {
          required: true
        },
        firstName: {
          required: true
        },
        username: {
          required: true,
          minlength: 4
        },
        email: {
          required: true,
          email: true
        },
        password: {
          minlength: 6
        },
      },

      messages: {
        lastName: {
          required: "Ce champ est obligatoire"
        },
        firstName: {
          required: "Ce champ est obligatoire"
        },
        username: {
          required: "Ce champ est obligatoire",
          minlength: "Veuillez saisir au moins 4 caractères"
        },
        email: {
          required: "Ce champ est obligatoire",
          email: "Veuillez saisir une adresse email valide"
        },
        password: {
          minlength: "Veuillez saisir au moins 6 caractères"
        },
      },
    });
  };

  const initSubmit = function () {
    const btn = formEl.find('button[type="submit"]');

    btn.on('click', (e) => {
      e.preventDefault();

      if (validator.form()) {
        KTApp.progress(btn);
        const adminUserid = document.getElementById('adminUserid').value;
        formEl.ajaxSubmit({
          url: '/admin/users/update/' + adminUserid,
          type: 'POST',
          success: function (response, status, xhr, $form) {
            KTApp.unprogress(btn);
            
            if (response.error) {
              swal.fire({
                "title": "", 
                "text": response.error.message, 
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
            } else {
              swal.fire({
                title: '',
                text: "Les modifications ont été enregistrées!",
                type: 'success',
                confirmButtonClass: 'btn btn-secondary'
              });
            }
          }
        });
      }
    });
  };

  return {
    init() {
      formEl = $('#kt_form');

      initValidation();
      initSubmit();
    }
  };
}());

jQuery(document).ready(() => {
  KTUsersAdd.init();
});
