import express from 'express';
import secure from 'express-force-https';
import compression from 'compression';
import session from 'express-session';
import bodyParser from 'body-parser';
import logger from 'morgan';
import chalk from 'chalk';
import errorHandler from 'errorhandler';
import lusca from 'lusca';
import dotenv from 'dotenv';
import connectMongo from 'connect-mongo';
import flash from 'express-flash';
import path from 'path';
import mongoose from 'mongoose';
import passport from 'passport';
import expressStatusMonitor from 'express-status-monitor';
import sass from 'node-sass-middleware';
import twig from 'twig';
import multer from 'multer';
import twigConfig from './config/twig';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import {
  getLogin, postLogin, getSignup, postSignup, getLogout, getProfileAccount, getMaintenanceAccount, postUpdateProfileAccount, getAddressesAccount,
  postUpdateAddress, postDeleteAddress, getApiAddressesAccount, getPasswordRecovery, postForgotPassword, getResetPassword, postResetPassword,
  getOrdersAccount, getOrdersDetails, getJackpotAccount as adminGetJackpotAccount
} from './modules/front/account/userController';

import {
  home, aboutUs, ourEngagements, privacyPolicy, mailchimpSubscribersAdd, processStopCovid, blog, conseilsAlimentairesHiverArticle
} from './modules/front/home/homeController';

import {
  checkMenuCategoryItemQuantity, addToCart, addMenuQuantity, checkout, updateBasketQuantity, postPayment, getPayment, postValidatePaymentOrder, completeOrder, initPaymentOrder, paygreenPaymentNotify, paygreenPaymentReturn, completeOrderWithPayment, postCheckCouponCodeOrder
} from './modules/front/cart/cartController';

import {
  dashboard as adminDashboard
} from './modules/backoffice/dashboard/dashboardController';

import {
  getLogin as adminGetLogin, postLogin as adminPostLogin, getUsersAdd as adminGetUsersAdd, createAdminUser,
  getLogout as adminGetLogout, getUsersList as adminGetUsersList, postUsersList as adminPostUsersList,
  getUsersEdit as adminGetUsersEdit, updateAdminUser as adminUpdateAdminUser
} from './modules/backoffice/user/userController';

import {
  getCategoriesEdit as adminGetCategoriesEdit, getCategoriesAdd as adminGetCategoriesAdd, updateCategories as adminUpdateCategories,
  postCategoriesList as adminPostCategoriesList, getCategoriesList as adminGetCategoriesList, createCategories as adminCreateCategories
} from './modules/backoffice/menu/controllers/categoryController';

import {
  getDishesEdit as adminGetDishesEdit, getDishesAdd as adminGetDishesAdd, updateDishes as adminUpdateDishes,
  postDishesList as adminPostDishesList, getDishesList as adminGetDishesList, createDishes as adminCreateDishes
} from './modules/backoffice/menu/controllers/dishController';

import {
  getMenuItemsList as adminGetMenuItemsList, getMenuItemsAdd as adminGetMenuItemsAdd, createMenuItem as adminCreateMenuItem,
  getMenuItemsEdit as adminGetMenuItemsEdit, menuItemCategoriesAdd as adminMenuItemCategoriesAdd, menuItemCategoriesEdit as adminMenuItemCategoriesEdit,
  menuItemCategoriesDelete as adminMenuItemCategoriesDelete,
  menuItemCategoriesOfferAdd as adminMenuItemCategoriesOfferAdd, postMenuItemCategoriesOfferAdd as adminPostMenuItemCategoriesOfferAdd,
  menuItemCategoriesOfferEdit as adminMenuItemCategoriesOfferEdit, postMenuItemCategoriesOfferEdit as adminPostMenuItemCategoriesOfferEdit,
  postMenuItemCategoriesOfferCombinedAdd as adminPostMenuItemCategoriesOfferCombinedAdd, postMenuItemCategoriesOfferCombinedEdit as adminPostMenuItemCategoriesOfferCombinedEdit,
  menuItemCategoriesOfferEditCombination as adminMenuItemCategoriesOfferEditCombination, menuItemCategoriesOfferEditCombinationAdd as adminMenuItemCategoriesOfferEditCombinationAdd,
  postMenuItemCategoriesOfferEditCombinationAdd as adminPostMenuItemCategoriesOfferEditCombinationAdd, menuItemCategoriesOfferEditCombinationEdit as adminMenuItemCategoriesOfferEditCombinationEdit,
  postMenuItemCategoriesOfferEditCombinationEdit as adminPostMenuItemCategoriesOfferEditCombinationEdit, menuItemCategoriesOfferEditCombinationEditDishesAdd as adminMenuItemCategoriesOfferEditCombinationEditMealsAdd,
  menuItemCategoriesOfferEditCombinationEditDishesEdit as adminMenuItemCategoriesOfferEditCombinationEditDishesEdit
} from './modules/backoffice/menu/controllers/menuController';

import {
  getOrdersList as adminGetOrdersList, postOrdersList as adminPostOrdersList, getOrderDetails as adminGetOrderDetails
} from './modules/backoffice/order/orderController';

const MongoStore = connectMongo(session);

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: '.env' });

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport');

/**
 * Configure twig with extends filters
 */
twigConfig();

/**
 * Configure storage
 */
const dirToUpload = `../public/${process.env.DIR_IMAGE_STORAGE}`;
const storage = multer.diskStorage({
  destination: path.join(__dirname, dirToUpload),
  filename(req, file, cb) {
    const originalNameParse = path.parse(file.originalname);
    cb(null, `${originalNameParse.name}-${Date.now()
    }${originalNameParse.ext}`);
  }
});

const upload = multer({ storage });

/**
 * Create Express server.
 */
const app = express();
app.use(secure);

/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);

app.set('views', __dirname);
app.set('twig options', {
  allow_async: true,
  strict_variables: false
});

app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public')
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true,
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use((req, res, next) => {
  //lusca.csrf()(req, res, next);
  next();
});
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.disable('x-powered-by');
app.use((req, res, next) => {
  if (req.user && req.user.isAdmin) {
    res.locals.adminUser = req.user;
  } else {
    res.locals.user = req.user;
  }
  res.locals.session = req.session;
  res.locals.originalUrl = req.originalUrl;
  res.locals.env = process.env.NODE_ENV;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the intended page
  /* if (!req.user
    && req.path !== '/login'
    && req.path !== '/signup'
    && !req.path.match(/^\/auth/)
    && !req.path.match(/\./)) {
    req.session.returnTo = req.originalUrl;
  } else if (req.user
    && (req.path === '/account' || req.path.match(/^\/api/))) {
    req.session.returnTo = req.originalUrl;
  } */
  next();
});

app.use('/', express.static(path.join(__dirname, '/../public'), { maxAge: 31557600000 }));

/**
 * FRONT SECTION
 */
const isFrontAuthenticated = (req, res, next) => {
  if (res.locals.user && req.isAuthenticated()) {
    return next();
  }

  res.redirect('/login');
};

/**
 * Home routes.
 */
app.get('/', home);

/**
 * Primary app routes.
 */
app.get('/login', getLogin);
app.post('/login', postLogin);

app.get('/signup', getSignup);
app.post('/signup', postSignup);

app.get('/logout', getLogout);

app.get('/blog', blog);
app.get('/blog/conseils-alimentaires-hiver', conseilsAlimentairesHiverArticle);

app.get('/about-us', aboutUs);
app.get('/our-engagements', ourEngagements);
app.get('/privacy-policy', privacyPolicy);
app.post('/mailchimp/subscribers/add', mailchimpSubscribersAdd);
app.get('/process-stop-covid', processStopCovid);

/**
 * Account calls.
 */

app.get('/account/profile', isFrontAuthenticated, getProfileAccount);
app.get('/account/jackpot', isFrontAuthenticated, adminGetJackpotAccount);
app.post('/account/profile/update', isFrontAuthenticated, postUpdateProfileAccount);
app.get('/account/maintenance', isFrontAuthenticated, getMaintenanceAccount);
app.get('/account/addresses', isFrontAuthenticated, getAddressesAccount);
app.post('/account/addresses/update', isFrontAuthenticated, postUpdateAddress);
app.post('/account/addresses/delete', isFrontAuthenticated, postDeleteAddress);
app.get('/account/api/addresses/:id', isFrontAuthenticated, getApiAddressesAccount);
app.get('/password-recovery', getPasswordRecovery);
app.post('/forgot-password', postForgotPassword);
app.get('/reset-password/:token', getResetPassword);
app.post('/reset-password/:token', postResetPassword);
app.get('/account/orders', isFrontAuthenticated, getOrdersAccount);
app.get('/account/orders/:orderId', isFrontAuthenticated, getOrdersDetails);


/**
 * Cart calls.
 */
app.post('/cart/checkMenuCategoryItemQuantity', checkMenuCategoryItemQuantity);
app.post('/cart/addToCart', addToCart);
app.post('/cart/addMenuQuantity', addMenuQuantity);

/**
 * Checkout calls
 */
app.get('/checkout', isFrontAuthenticated, checkout);
app.get('/checkout/payment', isFrontAuthenticated, getPayment);
app.post('/checkout/payment', isFrontAuthenticated, postPayment);
app.post('/checkout/update-basket-quantity', isFrontAuthenticated, updateBasketQuantity);
app.post('/checkout/payment/order/validate', isFrontAuthenticated, postValidatePaymentOrder);
app.post('/checkout/payment/order/init', isFrontAuthenticated, initPaymentOrder);
app.get('/checkout/complete/order/:id', isFrontAuthenticated, completeOrder);
app.get('/checkout/complete/order/:id/:paymentMethod', isFrontAuthenticated, completeOrderWithPayment);
app.post('/checkout/paygreen/payment/return', paygreenPaymentReturn);
app.post('/checkout/paygreen/payment/notify', paygreenPaymentNotify);
app.post('/checkout/payment/order/coupon-code/check', postCheckCouponCodeOrder);

/**
 * OAuth authentication routes. (Sign in)
 */
app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email', 'public_profile'] }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), (req, res) => {
  res.redirect(req.session.returnTo || '/');
});

app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'], accessType: 'offline', prompt: 'consent' }));
app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login' }), (req, res) => {
  res.redirect(req.session.returnTo || '/');
});

/**
 * ADMIN SECTION
 */
const isAdminAuthenticated = (req, res, next) => {
  if (res.locals.adminUser && req.isAuthenticated()) {
    return next();
  }

  res.redirect('/admin/login');
};

/**
 * Admin routes.
 */
app.get('/admin', isAdminAuthenticated, (req, res) => res.redirect('/admin/dashboard'));

/**
* Dashboard route
*/
app.get('/admin/dashboard', isAdminAuthenticated, adminDashboard);

/**
 * User routes
 */
app.get('/admin/login', adminGetLogin);
app.post('/admin/login', adminPostLogin);
app.get('/admin/users/add', isAdminAuthenticated, adminGetUsersAdd);
app.post('/admin/users/create', isAdminAuthenticated, createAdminUser);
app.get('/admin/users/list', isAdminAuthenticated, adminGetUsersList);
app.post('/admin/users/update/:id', isAdminAuthenticated, adminUpdateAdminUser);
app.post('/admin/users/list', isAdminAuthenticated, adminPostUsersList);
app.get('/admin/users/edit/:id', isAdminAuthenticated, adminGetUsersEdit);
app.get('/admin/logout', adminGetLogout);

/**
 * Menu routes
 */
app.get('/admin/menus/categories/add', isAdminAuthenticated, adminGetCategoriesAdd);
app.post('/admin/menus/categories/create', upload.single('photo'), isAdminAuthenticated, adminCreateCategories);
app.get('/admin/menus/categories/list', isAdminAuthenticated, adminGetCategoriesList);
app.post('/admin/menus/categories/update/:id', upload.single('photo'), isAdminAuthenticated, adminUpdateCategories);
app.post('/admin/menus/categories/list', isAdminAuthenticated, adminPostCategoriesList);
app.get('/admin/menus/categories/edit/:id', isAdminAuthenticated, adminGetCategoriesEdit);

app.get('/admin/menus/dishes/add', isAdminAuthenticated, adminGetDishesAdd);
app.post('/admin/menus/dishes/create', upload.single('photo'), isAdminAuthenticated, adminCreateDishes);
app.get('/admin/menus/dishes/list', isAdminAuthenticated, adminGetDishesList);
app.post('/admin/menus/dishes/update/:id', upload.single('photo'), isAdminAuthenticated, adminUpdateDishes);
app.post('/admin/menus/dishes/list', isAdminAuthenticated, adminPostDishesList);
app.get('/admin/menus/dishes/edit/:id', isAdminAuthenticated, adminGetDishesEdit);


app.get('/admin/menus/items/list', isAdminAuthenticated, adminGetMenuItemsList);
app.get('/admin/menus/items/add', isAdminAuthenticated, adminGetMenuItemsAdd);
app.post('/admin/menus/items/create', isAdminAuthenticated, adminCreateMenuItem);
app.get('/admin/menus/items/edit/:id', isAdminAuthenticated, adminGetMenuItemsEdit);
app.post('/admin/menus/items/:id/categories/add', isAdminAuthenticated, adminMenuItemCategoriesAdd);

app.get('/admin/menus/items/:id/categories/edit/:categoryId', isAdminAuthenticated, adminMenuItemCategoriesEdit);
app.get('/admin/menus/items/:id/categories/delete/:categoryId', isAdminAuthenticated, adminMenuItemCategoriesDelete);

app.get('/admin/menus/items/:id/categories/:categoryId/offer/add', isAdminAuthenticated, adminMenuItemCategoriesOfferAdd);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/add', isAdminAuthenticated, adminPostMenuItemCategoriesOfferAdd);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/combined/add', isAdminAuthenticated, adminPostMenuItemCategoriesOfferCombinedAdd);
app.get('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId', isAdminAuthenticated, adminMenuItemCategoriesOfferEdit);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId', isAdminAuthenticated, adminPostMenuItemCategoriesOfferEdit);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/combined/edit/:menuCategoryItemId', isAdminAuthenticated, adminPostMenuItemCategoriesOfferCombinedEdit);
app.get('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination', isAdminAuthenticated, adminMenuItemCategoriesOfferEditCombination);
app.get('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/add', isAdminAuthenticated, adminMenuItemCategoriesOfferEditCombinationAdd);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/add', isAdminAuthenticated, adminPostMenuItemCategoriesOfferEditCombinationAdd);
app.get('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId', isAdminAuthenticated, adminMenuItemCategoriesOfferEditCombinationEdit);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId', isAdminAuthenticated, adminPostMenuItemCategoriesOfferEditCombinationEdit);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId/dishes/add', isAdminAuthenticated, adminMenuItemCategoriesOfferEditCombinationEditMealsAdd);
app.post('/admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId/dishes/edit/:dishId', isAdminAuthenticated, adminMenuItemCategoriesOfferEditCombinationEditDishesEdit);

/**
 * User routes
 */
app.get('/admin/orders/list', isAdminAuthenticated, adminGetOrdersList);
app.post('/admin/orders/list', isAdminAuthenticated, adminPostOrdersList);
app.get('/admin/orders/details/:id', isAdminAuthenticated, adminGetOrderDetails);


/**
 * Error Handler.
 */
if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorHandler());
} else {
  app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).send('Server Error');
  });
}

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

export default app;
