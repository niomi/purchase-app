import path from 'path';
import validator from 'validator';
import moment from 'moment';
import MenuCategory from '../../../../models/Menu/MenuCategory';
import {
  createMenuCategory, updateMenuCategory, getMenuCategories
} from '../../../../repositories/Menu/menuCategoryRepository';

const templateDir = 'modules/backoffice/menu/views';

/**
 * POST /admin/menus/categories/list
 * Categories list
 */
const postCategoriesList = async (req, res) => {
  const menuCategories = await getMenuCategories({}, 'order', 'asc');

  res.json(menuCategories);
};

/**
 * GET /admin/menus/categories/list
 * Categories list
 */
const getCategoriesList = async (req, res) => {
  res.render(path.join(templateDir, 'category/list.twig'), {
    title: 'Liste des categories'
  });
};

/**
 * POST /admin/menus/categories/create
 * Categories create
 */
const createCategories = async (req, res, next) => {
  const {
    title, subtitle, order, activated
  } = req.body;

  const validationErrors = [];

  if (validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  createMenuCategory({
    title,
    subtitle,
    photo: req.file,
    order,
    activated,
  });

  return res.json({ success: true });
};

/**
 * POST /admin/menus/categories/update/:id
 */
const updateCategories = async (req, res, next) => {
  const { id } = req.params;
  const {
    title, subtitle, order, activated
  } = req.body;

  updateMenuCategory({
    id,
    title,
    subtitle,
    photo: req.file,
    order,
    activated
  });

  return res.json({ success: true });
};

/**
 * GET /admin/menus/categories/edit/:id
 * Categories edit page
 */
const getCategoriesEdit = async (req, res) => {
  const { id } = req.params;

  // handle not found
  const menuCategory = await MenuCategory.findById(id);

  res.render(path.join(templateDir, 'category/add.twig'), {
    title: 'Modifier une catégorie',
    menuCategory
  });
};

/**
 * GET /admin/menus/categories/add
 * Categories add page
 */
const getCategoriesAdd = async (req, res) => {
  res.render(path.join(templateDir, 'category/add.twig'), {
    title: 'Ajouter une catégorie',
  });
};

export {
  getCategoriesEdit,
  getCategoriesAdd,
  updateCategories,
  postCategoriesList,
  getCategoriesList,
  createCategories
};
