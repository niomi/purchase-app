import path from 'path';
import validator from 'validator';
import MenuItem from '../../../../models/Menu/MenuItem';
import Dish from '../../../../models/Dish';
import MenuCategory from '../../../../models/Menu/MenuCategory';
import MenuCategoryItem from '../../../../models/Menu/MenuCategoryItem';
import {
  createMenuItem as createMenuItemRepo, getMenuItems, addMenuItemCategory, addSimpleMenuCategoryItem, updateSimpleMenuCategoryItem,
  deleteMenuCategoryItem
} from '../../../../repositories/Menu/menuItemRepository';
import {
  getMenuCategories
} from '../../../../repositories/Menu/menuCategoryRepository';
import {
  getDishes
} from '../../../../repositories/dishRepository';
import {
  createMenuCategoryItem, addMeal, updateMenuCategoryItem, updateMeal, addDish, updateDish
} from '../../../../repositories/Menu/menuCategoryItemRepository';

const templateDir = 'modules/backoffice/menu/views';

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId
 */
const postMenuItemCategoriesOfferEditCombinationEdit = async (req, res, next) => {
  const { menuCategoryItemId, mealId } = req.params;

  // todo: check order already exists
  const { title, order, type } = req.body;

  const validationErrors = [];

  if (order === undefined || validator.isEmpty(order)) validationErrors.push("L'ordre est obligatoire");
  if (title === undefined || validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const mealInput = {
    id: mealId,
    type,
    title,
    order,
  };

  updateMeal(menuCategoryItemId, mealInput);

  return res.json({ success: true });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/add
 * Categories edit
 */
const postMenuItemCategoriesOfferEditCombinationAdd = async (req, res, next) => {
  const { menuCategoryItemId } = req.params;

  // todo: check order already exists
  const { title, order, type } = req.body;

  const validationErrors = [];

  if (order === undefined || validator.isEmpty(order)) validationErrors.push("L'ordre est obligatoire");
  if (title === undefined || validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const meal = {
    link: type,
    title,
    order,
  };

  addMeal(menuCategoryItemId, meal);

  return res.json({ success: true });
};

/**
 * GET /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/add
 * Categories edit
 */
const menuItemCategoriesOfferEditCombinationAdd = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
      populate: {
        path: 'meals.dishes.dish',
        model: Dish
      }
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);
  const dishes = await getDishes({}, 'createdAt', 'desc');

  const menuCategoryItem = menuItemCategory.menuCategoryItems.find((elt) => String(elt._id) === menuCategoryItemId);

  res.render(path.join(templateDir, 'items/category/combination/add.twig'), {
    menuItem,
    menuItemCategory,
    dishes,
    menuCategoryItem
  });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId/dishes/add
 */
const menuItemCategoriesOfferEditCombinationEditDishesAdd = async (req, res, next) => {
  const { menuCategoryItemId, mealId } = req.params;
  const { dish, additionalPrice } = req.body;

  const validationErrors = [];

  if (dish === undefined || validator.isEmpty(dish)) validationErrors.push("Le plat est obligatoire");

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const mealInfo = {
    dish,
    additionalPrice
  };

  addDish(menuCategoryItemId, mealId, mealInfo);

  return res.json({ success: true });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId/dishes/edit
 */
const menuItemCategoriesOfferEditCombinationEditDishesEdit = async (req, res, next) => {
  const { menuCategoryItemId, mealId, dishId } = req.params;
  const { dish, additionalPrice } = req.body;

  const validationErrors = [];

  if (dish === undefined || validator.isEmpty(dish)) validationErrors.push("Le plat est obligatoire");

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const mealInfo = {
    id: dishId,
    dish,
    additionalPrice
  };

  console.log(mealInfo);

  updateDish(menuCategoryItemId, mealId, mealInfo);

  return res.json({ success: true });
};

/**
 * GET /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination/edit/:mealId
 * Categories edit
 */
const menuItemCategoriesOfferEditCombinationEdit = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId, mealId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
      populate: {
        path: 'meals.dishes.dish',
        model: Dish
      }
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);

  const menuCategoryItem = menuItemCategory.menuCategoryItems.find((elt) => String(elt._id) === menuCategoryItemId);

  const meal = menuCategoryItem.item.meals.find((elt) => String(elt._id) === mealId);

  const dishes = await getDishes();

  res.render(path.join(templateDir, 'items/category/combination/add.twig'), {
    menuItem,
    menuItemCategory,
    menuCategoryItem,
    meal,
    dishes
  });
};

/**
 * GET /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId/combination
 * Categories edit
 */
const menuItemCategoriesOfferEditCombination = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
      populate: {
        path: 'meals.dishes.dish',
        model: Dish
      }
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);
  const dishes = await getDishes({}, 'createdAt', 'desc');

  const menuCategoryItem = menuItemCategory.menuCategoryItems.find((elt) => String(elt._id) === menuCategoryItemId);

  res.render(path.join(templateDir, 'items/category/combination/list.twig'), {
    menuItem,
    menuItemCategory,
    dishes,
    menuCategoryItem
  });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/combined/edit/:menuCategoryItemId
 * Categories edit
 */
const postMenuItemCategoriesOfferCombinedEdit = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId } = req.params;

  const { title, description, price, type } = req.body;

  console.log({ title, description, price, type });

  const validationErrors = [];

  if (price === undefined || validator.isEmpty(price)) validationErrors.push('Le prix est obligatoire');
  if (title === undefined || validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  await updateMenuCategoryItem({
    id: menuCategoryItemId,
    title,
    description,
    price
  });


  updateSimpleMenuCategoryItem({
    id,
    categoryId,
    menuCategoryItemId
  });

  return res.json({ success: true });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId
 * Categories edit
 */
const postMenuItemCategoriesOfferEdit = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId } = req.params;

  const { order, type, title, description, dish, price } = req.body;

  console.log({ order, type, title, description, dish, price, id, categoryId, menuCategoryItemId });

  const validationErrors = [];

  if (price === undefined || validator.isEmpty(price)) validationErrors.push('Le prix est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const menuCategoryItem = await updateMenuCategoryItem({
    id: menuCategoryItemId,
    title,
    description,
    price
  });

  const meal = {
    link: 'UNIQUE',
    dishes: [
      {
        dish
      }
    ]
  };

  menuCategoryItem.meals = meal;

  await menuCategoryItem.save();

  updateSimpleMenuCategoryItem({
    id,
    categoryId,
    menuCategoryItemId
  });

  return res.json({ success: true });
};

/**
 * GET /admin/menus/items/:id/categories/:categoryId/offer/edit/:menuCategoryItemId
 * Categories edit
 */
const menuItemCategoriesOfferEdit = async (req, res, next) => {
  const { id, categoryId, menuCategoryItemId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);
  const dishes = await getDishes({}, 'createdAt', 'desc');

  const menuCategoryItem = menuItemCategory.menuCategoryItems.find((elt) => String(elt._id) === menuCategoryItemId);

  res.render(path.join(templateDir, 'items/category/add-offer.twig'), {
    menuItem,
    menuItemCategory,
    dishes,
    menuCategoryItem
  });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/add
 * Categories edit
 */
const postMenuItemCategoriesOfferAdd = async (req, res, next) => {
  const { id, categoryId } = req.params;

  const { order, type, title, description, dish, price } = req.body;

  const validationErrors = [];

  if (price === undefined || validator.isEmpty(price)) validationErrors.push('Le prix est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const menuCategoryItem = await createMenuCategoryItem({
    title,
    description,
    price,
    type
  });

  const meal = {
    link: 'UNIQUE',
    dishes: [
      {
        dish
      }
    ]
  };

  addMeal(menuCategoryItem._id, meal);

  addSimpleMenuCategoryItem({
    id,
    categoryId,
    menuCategoryItemId: menuCategoryItem._id
  });

  return res.json({ success: true });
};

/**
 * POST /admin/menus/items/:id/categories/:categoryId/offer/combined/add
 */
const postMenuItemCategoriesOfferCombinedAdd = async (req, res, next) => {
  const { id, categoryId } = req.params;

  const { title, description, price, type } = req.body;

  console.log({ title, description, price, type });

  const validationErrors = [];

  if (title === undefined || validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');
  if (price === undefined || validator.isEmpty(price)) validationErrors.push('Le prix est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const menuCategoryItem = await createMenuCategoryItem({
    title,
    description,
    price,
    type
  });

  addSimpleMenuCategoryItem({
    id,
    categoryId,
    menuCategoryItemId: menuCategoryItem._id
  });

  return res.json({ success: true });
};

/**
 * GET /admin/menus/items/:id/categories/edit/:categoryId
 * Categories edit
 */
const menuItemCategoriesEdit = async (req, res, next) => {
  const { id, categoryId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
      populate: {
        path: 'meals.dishes.dish',
        model: Dish
      }
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);
  //console.log('menuItemCategory menuCategoryItems ', menuItemCategory.menuCategoryItems[0].item.meals[0].dishes[0].dish.title);

  res.render(path.join(templateDir, 'items/category/edit.twig'), {
    menuItem,
    menuItemCategory
  });
};

/**
 * GET /admin/menus/items/:id/categories/delete/:categoryId
 * Categories delete
 */
const menuItemCategoriesDelete = async (req, res, next) => {
  const { id, categoryId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
      populate: {
        path: 'meals.dishes.dish',
        model: Dish
      }
    });

  //const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);

  await deleteMenuCategoryItem({ id, categoryId });

  //console.log('menuItemCategory menuCategoryItems ', menuItemCategory.menuCategoryItems[0].item.meals[0].dishes[0].dish.title);

  res.redirect(`/admin/menus/items/edit/${id}`);
};

/**
 * GET /admin/menus/items/:id/categories/:categoryId/offer/add
 * Categories edit
 */
const menuItemCategoriesOfferAdd = async (req, res, next) => {
  const { id, categoryId } = req.params;

  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuItemCategory = menuItem.items.find((elt) => String(elt._id) === categoryId);
  const dishes = await getDishes({}, 'createdAt', 'desc');

  res.render(path.join(templateDir, 'items/category/add-offer.twig'), {
    menuItem,
    menuItemCategory,
    dishes
  });
};

/**
 * POST /admin/menus/items/categories/add/:id
 * Categories create
 */
const menuItemCategoriesAdd = async (req, res, next) => {
  const { id } = req.params;

  const { category, order } = req.body;

  const validationErrors = [];

  if (category === undefined || validator.isEmpty(category)) validationErrors.push('La categorie est obligatoire');
  if (order === undefined || validator.isEmpty(order)) validationErrors.push("L'ordre est obligatoire");

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  addMenuItemCategory({
    id,
    category,
    order
  });

  return res.json({ success: true });
};

/**
 * GET /admin/menus/items/edit/:id
 * Menu items edit page
 */
const getMenuItemsEdit = async (req, res) => {
  const { id } = req.params;

  // handle not found MenuCategory
  const menuItem = await MenuItem
    .findById(id)
    .populate({
      path: 'items.category',
      model: MenuCategory,
    })
    .populate({
      path: 'items.menuCategoryItems.item',
      model: MenuCategoryItem,
    });

  const menuCategories = await getMenuCategories({}, 'createdAt', 'desc');

  res.render(path.join(templateDir, 'items/edit.twig'), {
    title: 'Edition du menu du',
    menuItem,
    menuCategories
  });
};

/**
 * GET /admin/menus/items/list
 * Categories add page
 */
const getMenuItemsList = async (req, res) => {
  const menuItems = await getMenuItems({}, 'availableAt', 'desc');

  res.render(path.join(templateDir, 'items/list.twig'), {
    title: 'Liste des menus',
    menuItems,
  });
};

/**
 * GET /admin/menus/items/add
 * Menu items add page
 */
const getMenuItemsAdd = async (req, res) => {
  res.render(path.join(templateDir, 'items/add.twig'), {
    title: 'Paramétrer le menu',
  });
};

/**
 * POST /admin/menus/items/create
 * Menu items create
 */
const createMenuItem = async (req, res, next) => {
  // Todo: handle duplicate key
  const { availableAt } = req.body;

  const validationErrors = [];

  if (availableAt === undefined && validator.isEmpty(availableAt)) validationErrors.push('La date est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  createMenuItemRepo({ availableAt });

  return res.json({ success: true });
};

export {
  getMenuItemsList,
  getMenuItemsAdd,
  createMenuItem,
  getMenuItemsEdit,
  menuItemCategoriesAdd,
  menuItemCategoriesEdit,
  menuItemCategoriesDelete,
  menuItemCategoriesOfferAdd,
  postMenuItemCategoriesOfferAdd,
  menuItemCategoriesOfferEdit,
  postMenuItemCategoriesOfferEdit,
  postMenuItemCategoriesOfferCombinedAdd,
  postMenuItemCategoriesOfferCombinedEdit,
  menuItemCategoriesOfferEditCombination,
  menuItemCategoriesOfferEditCombinationAdd,
  postMenuItemCategoriesOfferEditCombinationAdd,
  menuItemCategoriesOfferEditCombinationEdit,
  postMenuItemCategoriesOfferEditCombinationEdit,
  menuItemCategoriesOfferEditCombinationEditDishesAdd,
  menuItemCategoriesOfferEditCombinationEditDishesEdit
};
