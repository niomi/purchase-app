import path from 'path';
import validator from 'validator';
import Dish from '../../../../models/Dish';
import {
  createDish, updateDish, getDishes
} from '../../../../repositories/dishRepository';

const templateDir = 'modules/backoffice/menu/views';

/**
 * POST /admin/menus/dishes/list
 * Dishes list
 */
const postDishesList = async (req, res) => {
  const dishes = await getDishes({}, 'createdAt', 'desc');

  res.json(dishes);
};

/**
 * GET /admin/menus/dishes/list
 * Dishes list
 */
const getDishesList = async (req, res) => {
  res.render(path.join(templateDir, 'dish/list.twig'), {
    title: 'Liste des recettes'
  });
};

/**
 * POST /admin/menus/dishes/create
 * Dishes create
 */
const createDishes = async (req, res, next) => {
  const {
    title, description, ingredients, allergens, type, quantity
  } = req.body;

  const validationErrors = [];

  if (validator.isEmpty(title)) validationErrors.push('Le titre est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  createDish({
    title,
    description,
    photo: req.file,
    ingredients,
    allergens,
    quantity,
    type
  });

  return res.json({ success: true });
};

/**
 * POST /admin/menus/dishes/update/:id
 */
const updateDishes = async (req, res, next) => {
  const { id } = req.params;
  const {
    title, description, ingredients, allergens, type, quantity
  } = req.body;

  updateDish({
    id,
    title,
    description,
    photo: req.file,
    ingredients,
    allergens,
    quantity,
    type
  });

  return res.json({ success: true });
};

/**
 * GET /admin/menus/dishes/edit/:id
 * Dishes edit page
 */
const getDishesEdit = async (req, res) => {
  const { id } = req.params;

  // handle not found
  const dish = await Dish.findById(id);

  res.render(path.join(templateDir, 'dish/add.twig'), {
    title: 'Modifier une recette',
    dish
  });
};

/**
 * GET /admin/menus/dishes/add
 * Dishes add page
 */
const getDishesAdd = async (req, res) => {
  res.render(path.join(templateDir, 'dish/add.twig'), {
    title: 'Ajouter une recette',
  });
};

export {
  getDishesEdit,
  getDishesAdd,
  updateDishes,
  postDishesList,
  getDishesList,
  createDishes
};
