import path from 'path';
import moment from 'moment';
import {
  getOrders
} from '../../../repositories/orderRepository';
import Order from '../../../models/Order';

const templateDir = 'modules/backoffice/order/views';

/**
 * POST /admin/orders/list
 * Admin login page
 */
const postOrdersList = async (req, res) => {
  const orders = await getOrders({}, 'deliveryInfo.window.beginDate', 'desc');

  const ordersFormatted = orders.map((order) => {
    const newOrder = { ...order.toJSON() };
    newOrder.createdAtFormatted = moment(order.createdAt).format('DD/MM/YYYY HH:mm:ss');
    newOrder.deliveryAtFormatted = `${moment(order.deliveryInfo.window.beginDate).format('DD/MM/YYYY HH:mm')} - ${moment(order.deliveryInfo.window.endDate).format('HH:mm')}`;
    return newOrder;
  });

  res.json(ordersFormatted);
};

/**
 * GET /admin/orders/list
 * Admin login page
 */
const getOrdersList = async (req, res) => {
  res.render(path.join(templateDir, 'list.twig'), {
    title: 'Liste des commandes'
  });
};

/**
 * GET /admin/orders/details/:id
 * Admin login page
 */
const getOrderDetails = async (req, res) => {
  const { id } = req.params;

  const order = await Order.findById(id);

  res.render(path.join(templateDir, 'details.twig'), {
    title: 'Détail de la commande',
    order
  });
};

export {
  getOrdersList,
  postOrdersList,
  getOrderDetails
};
