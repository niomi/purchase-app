import path from 'path';
import passport from 'passport';
import validator from 'validator';
import moment from 'moment';
import AdminUser from '../../../models/AdminUser';
import {
  newAdminUser, updateAdminUser as updateAdminUserRepo, getAdminUsers
} from '../../../repositories/adminUserRepository';

const templateDir = 'modules/backoffice/user/views';

/**
 * POST /admin/users/list
 * Admin login page
 */
const postUsersList = async (req, res) => {
  const users = await getAdminUsers({}, 'createdAt', 'desc');

  const usersFormatted = users.map((user) => {
    const newUser = { ...user.toJSON() };
    newUser.createdAtFormatted = moment(user.createdAt).format('DD/MM/YYYY');
    return newUser;
  });

  res.json(usersFormatted);
};

/**
 * GET /admin/users/list
 * Admin login page
 */
const getUsersList = async (req, res) => {
  res.render(path.join(templateDir, 'users/list.twig'), {
    title: 'Liste des utilisateurs'
  });
};

/**
 * POST /admin/users/create
 * Admin login page
 */
const createAdminUser = async (req, res, next) => {
  const {
    email, username, password, job, firstName, lastName, phoneNumber
  } = req.body;

  const validationErrors = [];

  if (validator.isEmpty(email)) validationErrors.push('Le prénom est obligatoire');
  if (validator.isEmpty(lastName)) validationErrors.push('Le nom est obligatoire');
  if (validator.isEmpty(firstName)) validationErrors.push('Le prénom est obligatoire');
  if (validator.isEmpty(username)) validationErrors.push("Le nom d'utilisateur est obligatoire");
  if (validator.isEmpty(password)) validationErrors.push('Le mot de passe est obligatoire');

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  const adminUser = newAdminUser({
    email,
    username,
    phoneNumber,
    password,
    firstName,
    lastName,
    job
  });

  AdminUser.findOne({ $or: [{ email: email.toLowerCase() }, { username: username.toLowerCase() }] }, (err, existingUser) => {
    if (err) { return next(err); }
    if (existingUser) {
      const error = {
        code: 'user_already_exist',
        message: "Un compte existe déjà avec cette adresse email ou ce nom d'utilisateur"
      };

      console.log(error);

      return res.json({ error });
    }
    adminUser.save((err) => {
      if (err) {
        console.log(err);

        const error = {
          code: 'user_save_error',
          message: err.errmsg
        };

        return res.json({ error });
      }

      return res.json({ success: true });
    });
  });
};

/**
 * POST /admin/users/update/:id
 * Admin login page
 */
const updateAdminUser = async (req, res, next) => {
  const { id } = req.params;
  const {
    email, username, password, job, firstName, lastName, activated, phoneNumber
  } = req.body;

  updateAdminUserRepo({
    id,
    email,
    username,
    phoneNumber,
    password,
    firstName,
    lastName,
    activated,
    job
  });

  return res.json({ success: true });
};

/**
 * GET /admin/users/edit/:id
 * Admin login page
 */
const getUsersEdit = async (req, res) => {
  const { id } = req.params;

  // handle not found
  const adminUserItem = await AdminUser.findById(id);

  res.render(path.join(templateDir, 'users/add.twig'), {
    title: 'Modifier un utilisateur',
    adminUserItem
  });
};

/**
 * GET /admin/users/add
 * Admin login page
 */
const getUsersAdd = async (req, res) => {
  res.render(path.join(templateDir, 'users/add.twig'), {
    title: 'Ajouter un utilisateur',
  });
};

/**
 * GET /admin/login
 * Admin login page
 */
const getLogin = async (req, res) => {
  if (res.locals.adminUser && req.isAuthenticated()) {
    return res.redirect('/admin');
  }

  res.render(path.join(templateDir, 'login.twig'), {
    title: 'Connexion'
  });
};

/**
 * POST /admin/login
 * Sign in using email and password.
 */
const postLogin = (req, res, next) => {
  const validationErrors = [];
  if (validator.isEmpty(req.body.username)) validationErrors.push("Veuillez saisir un nom d'utilisateur");
  if (validator.isEmpty(req.body.password)) validationErrors.push("Veuillez saisir votre mot de passe.");

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    const error = {
      code: 'error_fields_empty',
      message: validationErrors.join(' ')
    };

    return res.json({ error });
  }

  passport.authenticate('admin-local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      req.flash('formErrors', info);

      const error = {
        code: 'error_invalid_credentials',
        message: info.msg
      };

      return res.json({ error });
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
      res.json({ success: true });
    });
  })(req, res, next);
};

/**
 * GET /admin/logout
 * Log out.
 */
const getLogout = (req, res) => {
  req.logout();
  req.session.destroy((err) => {
    if (err) console.log('Error : Failed to destroy the session during logout.', err);
    res.user = null;
    res.redirect('/admin');
  });
};

export {
  getLogin,
  postLogin,
  getUsersAdd,
  createAdminUser,
  updateAdminUser,
  getUsersList,
  postUsersList,
  getUsersEdit,
  getLogout
};
