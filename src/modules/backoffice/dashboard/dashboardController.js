import path from 'path';

const templateDir = 'modules/backoffice/dashboard/views';

/**
 * GET /admin/dashboard
 * Dashboard page
 */
const dashboard = async (req, res) => {
  res.render(path.join(templateDir, 'index.twig'), {
    title: 'Dashboard'
  });
};

export {
  dashboard
};
