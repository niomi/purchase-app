import validator from 'validator';
import dotenv from 'dotenv';
import Tookan from 'tookan-api';
import moment from 'moment';

dotenv.config({ path: '.env' });

/**
 * Tookan create task
 */
const tookanCreateTask = async (params) => {
  const client = new Tookan.Client({ api_key: process.env.TOOKAN_API_KEY });

  const {
    orderId,
    customerUsername,
    customerPhone,
    customerAddress,
    deliveryWindow
  } = params;

  const options = {
    order_id: orderId,
    job_description: 'Livraison de la commande',
    customer_username: customerUsername,
    customer_phone: `+33${customerPhone}`,
    customer_address: customerAddress,
    job_delivery_datetime: moment(deliveryWindow.endDate).format('YYYY-MM-DD hh:mm:ss'),
    auto_assignment: '0',
    has_pickup: '0',
    has_delivery: '1',
    layout_type: '0',
    tracking_link: 1,
    timezone: '-120',
    notify: 1
  };

  const response = await client.createTask(options);
  if (response.status !== 200) {
    // todo: handle this case
  }
};


export {
  tookanCreateTask
};
