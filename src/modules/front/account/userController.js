
import path from 'path';
import { promisify } from 'util';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import passport from 'passport';
import validator from 'validator';
import mailChecker from 'mailchecker';
import sgMail from '@sendgrid/mail';
import dotenv from 'dotenv';
import User from '../../../models/User';
import Order from '../../../models/Order';
import {
  newUser, updateUser, updateAddress, deleteAddress
} from '../../../repositories/userRepository';
import {
  getOrders
} from '../../../repositories/orderRepository';

const _ = require('lodash');

dotenv.config({ path: '.env' });

const templateDir = 'modules/front/account/views';

const randomBytesAsync = promisify(crypto.randomBytes);

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * GET /account/orders
 * Login page
 */
const getOrdersAccount = async (req, res) => {
  const { user } = req;
  const orders = await getOrders({ customer: user._id, status: { $ne: 'INIT' } }, 'createdAt', 'desc');

  res.render(path.join(templateDir, 'account/orders.twig'), {
    title: 'Mes commandes',
    orders
  });
};

/**
 * GET /account/orders/:orderId
 * Login page
 */
const getOrdersDetails = async (req, res) => {
  const { user } = req;
  const { orderId } = req.params;

  const order = await Order.findById(orderId);

  if (!order) {
    return res.redirect('/account/orders');
  }

  if (!order || order.status === "INIT" || String(order.customer) !== String(user._id)) {
    return res.redirect('/account/orders');
  }

  res.render(path.join(templateDir, 'account/order-details.twig'), {
    title: 'Détails de la commande',
    order
  });
};

/**
 * GET /login
 * Login page
 */
const getLogin = (req, res) => {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render(path.join(templateDir, 'login.twig'), {
    title: 'Connexion'
  });
};

/**
 * POST /login
 * Sign in using email and password.
 */
const postLogin = (req, res, next) => {
  const validationErrors = [];
  if (!validator.isEmail(req.body.email)) validationErrors.push({ msg: 'Veuillez saisir une adresse email valide.' });
  if (validator.isEmpty(req.body.password)) validationErrors.push({ msg: 'Veuillez saisir votre mot de passe.' });

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);

    return res.redirect('/login');
  }
  req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false });

  passport.authenticate('front-local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      console.error(info);
      req.flash('formErrors', info);
      return res.redirect('/login');
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
      console.info('Success! You are logged in.');
      res.redirect(req.session.returnTo || '/');
    });
  })(req, res, next);
};

/**
 * GET /signup
 * Signup page.
 */
const getSignup = (req, res) => {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }
  res.render(path.join(templateDir, 'signup.twig'), {
    title: 'Inscription'
  });
};

/**
 * GET /account/addresses
 */
const getAddressesAccount = (req, res) => {
  res.render(path.join(templateDir, 'account/addresses.twig'), {
    title: 'Mes adresses',
    GCP_API_Key: process.env.GOOGLE_CLOUD_PLATFORM_API_KEY
  });
};

/**
 * POST /account/addresses/update
 */
const postUpdateAddress = async (req, res) => {
  let error;

  // todo : add lastUsedAt
  const {
    id, formattedAddress, streetNumber, route, locality, administrativeArea, postalCode, country,
    additionalInformation, companyName, instruction, phoneNumber, firstName, lastName, alias
  } = req.body;

  const validationErrors = [];
  if (validator.isEmpty(formattedAddress)) validationErrors.push({ msg: "L'adresse est obligatoire'" });

  if (validationErrors.length) {
    error = {
      code: 'user_invalid_fields',
      messages: validationErrors
    };

    return res.json({ error });
  }

  const shippingAddress = {
    id,
    formattedAddress,
    streetNumber,
    route,
    locality,
    administrativeArea,
    postalCode,
    country,
    additionalInformation,
    companyName,
    phoneNumber,
    alias,
    instruction,
    firstName,
    lastName
  };

  await updateAddress(req.user._id, shippingAddress);

  res.json({ success: 'succeeded' });
};

/**
 * POST /account/addresses/delete
 */
const postDeleteAddress = async (req, res) => {
  const { id } = req.body;

  await deleteAddress(req.user._id, id);

  res.json({ success: 'succeeded' });
};

/**
 * GET /account/api/addresses/:id
 */
const getApiAddressesAccount = (req, res) => {
  const { id } = req.params;

  const address = req.user.shippingAddresses.find((address) => String(address._id) === id);

  res.json(address);
};

/**
 * GET /account/profile
 */
const getProfileAccount = (req, res) => {
  res.render(path.join(templateDir, 'account/profile.twig'), {
    title: 'Mon profil'
  });
};

/**
 * GET /account/jackpot
 */
const getJackpotAccount = (req, res) => {
  console.log(req.user);
  res.render(path.join(templateDir, 'account/jackpot.twig'), {
    title: 'Ma cagnotte'
  });
};

/**
 * POST /account/profile/update
 */
const postUpdateProfileAccount = async (req, res) => {
  let error;

  const {
    firstName, lastName, phoneNumber, password, confirmPassword, professionalActivity
  } = req.body;

  const validationErrors = [];

  if (validator.isEmpty(firstName)) validationErrors.push({ msg: 'Le prénom est obligatoire' });
  if (password || confirmPassword) {
    if (!validator.isLength(password, { min: 6 })) validationErrors.push({ msg: 'Le mot de passe doit avoir au moins 8 caractères' });
    if (password !== confirmPassword) {
      validationErrors.push({ msg: 'Les mots de passes ne sont pas identiques' });
    }
  }

  if (validationErrors.length) {
    error = {
      code: 'user_invalid_fields',
      messages: validationErrors
    };

    return res.json({ error });
  }

  let input = {
    id: req.user._id,
    profile: {
      firstName,
      lastName,
      professionalActivity
    },
    phoneNumber,
    deliveryAmount: professionalActivity === 'STUDENT' ? 0 : process.env.DELIVERY_AMOUNT_DEFAULT
  };

  if (password) {
    input = {
      ...input,
      password
    };
  }

  await updateUser(input);

  res.json({ success: 'succeeded' });
};

/**
 * GET /account/maintenance
 */
const getMaintenanceAccount = (req, res) => {
  res.render(path.join(templateDir, 'account/maintenance.twig'), {
    title: 'En maintenance'
  });
};

/**
 * POST /signup
 * Create a new local account.
 */
const postSignup = (req, res, next) => {
  const validationErrors = [];
  if (validator.isEmpty(req.body.firstName)) validationErrors.push({ msg: 'Veuillez saisir votre prénom.' });
  if (req.body.phoneNumber && validator.isEmpty(req.body.phoneNumber)) validationErrors.push({ msg: 'Veuillez saisir votre numéro de téléphone mobile.' });
  if (!validator.isEmail(req.body.email)) validationErrors.push({ msg: 'Veuillez saisir une adresse email valide.' });
  if (!validator.isLength(req.body.password, { min: 6 })) validationErrors.push({ msg: 'Votre mot de passe doit avoir au minimum 6 caractères.' });

  if (validationErrors.length) {
    console.error(req.body, '', validationErrors);
    req.flash('formErrors', validationErrors);
    return res.redirect('/signup');
  }

  req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false });
  console.log('Create new user :', req.body);

  const user = newUser({
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  });

  User.findOne({ email: req.body.email }, (err, existingUser) => {
    if (err) { return next(err); }
    if (existingUser) {
      console.warn('Un compte existe déjà avec cette adresse email.');
      req.flash('formErrors', { msg: 'Un compte existe déjà avec cette adresse email.' });
      return res.redirect('/signup');
    }
    user.save((err) => {
      if (err) { return next(err); }
      req.logIn(user, (err) => {
        if (err) {
          return next(err);
        }
        res.redirect('/');
      });
    });
  });
};

/**
 * GET /logout
 * Log out.
 */
const getLogout = (req, res) => {
  req.logout();
  req.session.destroy((err) => {
    if (err) console.log('Error : Failed to destroy the session during logout.', err);
    req.user = null;
    res.redirect('/');
  });
};

/**
 * GET /password-recovery
 * Login page
 */
const getPasswordRecovery = (req, res) => {
  res.render(path.join(templateDir, 'account/password-recovery.twig'), {
    title: 'Mot de passe oublié'
  });
};

/**
 * POST /forgot-password
 * Create a random token, then the send user an email with a reset link.
 */
const postForgotPassword = (req, res, next) => {
  const validationErrors = [];
  if (validator.isEmpty(req.body.email) || !validator.isEmail(req.body.email)) validationErrors.push({ msg: 'Veuillez saisir une adresse email valide' });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/password-recovery');
  }
  req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false });

  const createRandomToken = randomBytesAsync(16)
    .then((buf) => buf.toString('hex'));

  const setRandomToken = (token) =>
    User
      .findOne({ email: req.body.email })
      .then((user) => {
        if (!user) {
          req.flash('errors', { msg: "Aucun compte n'existe avec cette adresse email. Veuillez vous inscrire si vous n'avez pas de compte." });
        } else {
          user.passwordResetToken = token;
          user.passwordResetExpires = Date.now() + 3600000; // 1 hour
          user = user.save();
        }
        return user;
      });

  const sendForgotPasswordEmail = (user) => {
    if (!user) { return; }
    const token = user.passwordResetToken;

    const msg = {
      to: user.email,
      from: `NIOMI <${process.env.EMAIL_SENDER_HELLO}>`,
      subject: 'Réinitialisation de votre mot de passe NIOMI',
      html: `Vous recevez ce mail car vous (ou une autre personne) a demandé la réinitialisation du mot de passe de votre compte.<br><br>
        Veuillez cliquer sur le lien suivant ou collez-le dans la barre d'adresse de votre navigateur pour terminer le processus:<br>
        ${process.env.BASE_URL}/reset-password/${token}<br><br>
        Si vous ne l'avez pas demandé, veuillez ignorer ce mail et votre mot de passe restera inchangé.`
    };

    return sgMail.send(msg)
      .then(() => {
        req.flash('info', { msg: `Un message de réinitialisation de mot de passe a été envoyé à ${user.email}.` });
      })
      .catch((err) => {
        console.log('ERROR: Could not send forgot password email after security downgrade.\n', err);
        req.flash('errors', { msg: "Une erreur est survenue durant l'envoi du message de réinitialisation de votre mot de passe. Veuillez réessayer ultérieurement ou contactez nous par messagerie." });
        return err;
      });
  };

  createRandomToken
    .then(setRandomToken)
    .then(sendForgotPasswordEmail)
    .then(() => res.redirect('/password-recovery'))
    .catch(next);
};

/**
 * GET /reset-password/:token
 * Reset Password page.
 */
const getResetPassword = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }
  const validationErrors = [];
  if (!validator.isHexadecimal(req.params.token)) validationErrors.push({ msg: 'Token invalide.  Veuillez réesayer.' });
  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/password-recovery');
  }

  User
    .findOne({ passwordResetToken: req.params.token })
    .where('passwordResetExpires').gt(Date.now())
    .exec((err, user) => {
      if (err) { return next(err); }
      if (!user) {
        req.flash('errors', { msg: 'Le lien est invalide ou a expiré.' });
        return res.redirect('/password-recovery');
      }
      res.render(path.join(templateDir, 'account/reset-password.twig'), {
        title: 'Réinitialisation du mot de passe',
        token: req.params.token
      });
    });
};

/**
 * POST /reset-password/:token
 * Process the reset password request.
 */
const postResetPassword = (req, res, next) => {
  const validationErrors = [];
  if (!validator.isLength(req.body.password, { min: 6 })) validationErrors.push({ msg: 'Votre mot de passe doit avoir au minimum 6 caractères.' });
  if (req.body.password !== req.body.confirm) validationErrors.push({ msg: 'Les mots de passes ne sont pas identiques' });
  if (!validator.isHexadecimal(req.params.token)) validationErrors.push({ msg: 'Token invalide.  Veuillez réesayer.' });

  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('back');
  }

  const resetPassword = () =>
    User
      .findOne({ passwordResetToken: req.params.token })
      .where('passwordResetExpires').gt(Date.now())
      .then((user) => {
        if (!user) {
          req.flash('errors', { msg: 'Le token est invalide ou a expiré.' });
          return res.redirect('back');
        }
        user.password = req.body.password;
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        return user.save().then(() => new Promise((resolve, reject) => {
          req.logIn(user, (err) => {
            if (err) { return reject(err); }
            resolve(user);
          });
        }));
      });

  const sendResetPasswordEmail = (user) => {
    if (!user) { return; }

    const msg = {
      to: user.email,
      from: `NIOMI <${process.env.EMAIL_SENDER_HELLO}>`,
      subject: 'Votre mot de passe NIOMI a été changé',
      html: `Bonjour,<br>Nous vous confirmons que le mot de passe de votre compte ${user.email} a été changé.\n`
    };
    return sgMail.send(msg)
      .then(() => {
        req.flash('success', { msg: 'Votre mot de passe a été changé' });
      })
      .catch((err) => {
        console.log('ERROR: Could not send password reset confirmation email after security downgrade.\n', err);
      });
  };

  resetPassword()
    .then(sendResetPasswordEmail)
    .then(() => { if (!res.finished) res.redirect('back'); })
    .catch((err) => next(err));
};

/**
 * GET /account/verify/:token
 * Verify email address
 */
exports.getVerifyEmailToken = (req, res, next) => {
  if (req.user.emailVerified) {
    req.flash('info', { msg: 'The email address has been verified.' });
    return res.redirect('/account');
  }

  const validationErrors = [];
  if (req.params.token && (!validator.isHexadecimal(req.params.token))) validationErrors.push({ msg: 'Invalid Token.  Please retry.' });
  if (validationErrors.length) {
    req.flash('errors', validationErrors);
    return res.redirect('/account');
  }

  if (req.params.token === req.user.emailVerificationToken) {
    User
      .findOne({ email: req.user.email })
      .then((user) => {
        if (!user) {
          req.flash('errors', { msg: 'There was an error in loading your profile.' });
          return res.redirect('back');
        }
        user.emailVerificationToken = '';
        user.emailVerified = true;
        user = user.save();
        req.flash('info', { msg: 'Thank you for verifying your email address.' });
        return res.redirect('/account');
      })
      .catch((error) => {
        console.log('Error saving the user profile to the database after email verification', error);
        req.flash('errors', { msg: 'There was an error when updating your profile.  Please try again later.' });
        return res.redirect('/account');
      });
  } else {
    req.flash('errors', { msg: 'The verification link was invalid, or is for a different account.' });
    return res.redirect('/account');
  }
};

/**
 * GET /account/verify
 * Verify email address
 */
exports.getVerifyEmail = (req, res, next) => {
  if (req.user.emailVerified) {
    req.flash('info', { msg: 'The email address has been verified.' });
    return res.redirect('/account');
  }

  if (!mailChecker.isValid(req.user.email)) {
    req.flash('errors', { msg: 'The email address is invalid or disposable and can not be verified.  Please update your email address and try again.' });
    return res.redirect('/account');
  }

  const createRandomToken = randomBytesAsync(16)
    .then((buf) => buf.toString('hex'));

  const setRandomToken = (token) => {
    User
      .findOne({ email: req.user.email })
      .then((user) => {
        user.emailVerificationToken = token;
        user = user.save();
      });
    return token;
  };

  const sendVerifyEmail = (token) => {
    let transporter = nodemailer.createTransport({
      service: 'SendGrid',
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD
      }
    });
    const mailOptions = {
      to: req.user.email,
      from: 'hackathon@starter.com',
      subject: 'Please verify your email address on Hackathon Starter',
      text: `Thank you for registering with hackathon-starter.\n\n
        This verify your email address please click on the following link, or paste this into your browser:\n\n
        http://${req.headers.host}/account/verify/${token}\n\n
        \n\n
        Thank you!`
    };
    return transporter.sendMail(mailOptions)
      .then(() => {
        req.flash('info', { msg: `An e-mail has been sent to ${req.user.email} with further instructions.` });
      })
      .catch((err) => {
        if (err.message === 'self signed certificate in certificate chain') {
          console.log('WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.');
          transporter = nodemailer.createTransport({
            service: 'SendGrid',
            auth: {
              user: process.env.SENDGRID_USER,
              pass: process.env.SENDGRID_PASSWORD
            },
            tls: {
              rejectUnauthorized: false
            }
          });
          return transporter.sendMail(mailOptions)
            .then(() => {
              req.flash('info', { msg: `An e-mail has been sent to ${req.user.email} with further instructions.` });
            });
        }
        console.log('ERROR: Could not send verifyEmail email after security downgrade.\n', err);
        req.flash('errors', { msg: 'Error sending the email verification message. Please try again shortly.' });
        return err;
      });
  };

  createRandomToken
    .then(setRandomToken)
    .then(sendVerifyEmail)
    .then(() => res.redirect('/account'))
    .catch(next);
};

export {
  getLogin,
  postLogin,
  postSignup,
  getSignup,
  getLogout,
  getProfileAccount,
  getMaintenanceAccount,
  postUpdateProfileAccount,
  getAddressesAccount,
  postUpdateAddress,
  postDeleteAddress,
  getApiAddressesAccount,
  postForgotPassword,
  getPasswordRecovery,
  getResetPassword,
  postResetPassword,
  getOrdersAccount,
  getOrdersDetails,
  getJackpotAccount
};
