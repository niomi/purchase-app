import path from 'path';
import validator from 'validator';
import moment from 'moment';
import dotenv from 'dotenv';
import axios from 'axios';
import {
  checkQuantityByDish
} from '../../../repositories/Menu/menuCategoryItemRepository';
import MenuCategoryItem from '../../../models/Menu/MenuCategoryItem';
import Dish from '../../../models/Dish';
import Order from '../../../models/Order';
import User from '../../../models/User';
import {
  createShoppingCart, addItemToShoppingCart, removeItemToShoppingCartFromIndex, linkOrderToShoppingCart
} from '../../../repositories/shoppingCartRepository';
import {
  createOrder, updateOrder, validatePaymentOrder, updatePaymentMethodOrder, getOrders
} from '../../../repositories/orderRepository';
import { tookanCreateTask } from '../../api/apiController';

dotenv.config({ path: '.env' });

const templateDir = 'modules/front/cart/views';

/**
 * POST /checkout/payment/order/coupon-code/check
 * Checkout page
 */
const postCheckCouponCodeOrder = async (req, res) => {
  const { couponCode, orderId } = req.body;

  const order = await Order.findById(orderId);

  // check if only one order
  if (order.couponCode) {
    const error = {
      code: 'error_code_coupon_invalid',
      messages: "Ce code promotionnel n'est pas cumulable"
    };

    return res.json({ error });
  }

  if (couponCode.toUpperCase() === 'AKWABA') {
    const orders = await getOrders({ customer: req.user._id, status: { $eq: 'DELIVERED' } });

    if (orders.length >= 1) {
      const error = {
        code: 'error_code_coupon_invalid',
        messages: 'Ce code promotionnel est invalide'
      };

      return res.json({ error });
    }
    // code = 'AKWABA'
    // type = 'Fixed cart discount' (or 'Percentage discount')
    // amount = 3€ (description, usage/limit, date d'expiration)
    // add the reference
    const newSubTotalAmount = order.subTotalAmount - 3;
    updateOrder({ id: orderId, subTotalAmount: newSubTotalAmount, couponCode: couponCode.toUpperCase() });

    return res.json({ status: 'succeeded' });
  }

  const error = {
    code: 'error_code_coupon_invalid',
    messages: 'Ce code promotionnel est invalide'
  };

  return res.json({ error });
};

/**
 * POST /checkout/paygreen/payment/return
 * Complete order page
 */
const paygreenPaymentReturn = async (req, res, next) => {
  // Todo: handle error
  const { result, orderId } = req.query;

  const order = await Order.findById(orderId);
  const user = await User.findById(order.customer);

  req.logIn(user, (err) => {
    if (err) { return next(err); }
  });

  if (result !== "SUCCESSED") {
    return res.redirect('/checkout');
  }

  const params = {
    orderId: order._id,
    customerUsername: order.deliveryInfo.firstName,
    customerPhone: order.deliveryInfo.phoneNumber,
    customerAddress: order.deliveryInfo.address,
    deliveryWindow: order.deliveryInfo.window
  };

  tookanCreateTask(params);

  await updateOrder({ id: orderId, status: 'PROCESSING' });

  delete req.session.shoppingCart;

  return res.redirect(`/checkout/complete/order/${orderId}`);
};

/**
 * GET /checkout/complete/order/:id/:paymentMethod
 */
const completeOrderWithPayment = async (req, res) => {
  const { id, paymentMethod } = req.params;

  console.log({ id, paymentMethod });

  const order = await Order.findById(id);

  await updatePaymentMethodOrder(id, paymentMethod);

  await updateOrder({ id, status: 'PROCESSING' });

  delete req.session.shoppingCart;

  const params = {
    orderId: order._id,
    customerUsername: order.deliveryInfo.firstName,
    customerPhone: order.deliveryInfo.phoneNumber,
    customerAddress: order.deliveryInfo.address,
    deliveryWindow: order.deliveryInfo.window
  };

  tookanCreateTask(params);

  res.render(path.join(templateDir, 'checkout/complete.twig'), {
    title: 'Commande validée',
    order
  });
};

/**
 * GET /checkout/complete/order/:id
 */
const completeOrder = async (req, res) => {
  const { id } = req.params;

  const order = await Order.findById(id);

  res.render(path.join(templateDir, 'checkout/complete.twig'), {
    title: 'Commande validée',
    order
  });
};

/**
 * POST /checkout/paygreen/payment/notify
 */
const paygreenPaymentNotify = async (req, res) => {
  const { pid, orderId, result } = req.query;

  if (result === "SUCCESSED") {
    await validatePaymentOrder(orderId, { pid }, 'PAYGREEN');
  }

  res.json();
};

/**
 * POST /checkout/payment/order/init
 * Checkout page
 */
const initPaymentOrder = async (req, res) => {
  const { orderId, totalAmount, paymentMethod } = req.body;

  await updatePaymentMethodOrder(orderId, paymentMethod);

  const url = `https://${process.env.PAYGREEN_BASE_URL}/api/${process.env.PAYGREEN_ID}/payins/transaction/cash`;

  const config = {
    headers: { Authorization: `Bearer ${process.env.PAYGREEN_PKEY}`, 'Content-Type': 'application/json', Accept: 'application/json' }
  };

  const returnedUrl = `${process.env.BASE_URL}/checkout/paygreen/payment/return`;
  const notifiedUrl = `${process.env.BASE_URL}/checkout/paygreen/payment/notify`;

  const parameters = {
    orderId,
    amount: totalAmount,
    currency: 'EUR',
    paymentType: paymentMethod,
    returned_url: returnedUrl,
    notified_url: notifiedUrl,
    buyer: {
      id: req.user._id,
      lastName: req.user.profile.lastName,
      firstName: req.user.profile.firstName,
      email: req.user.email,
      country: 'FR'
    },
    //ttl: 'P7DT0H0M'
    ttl: 'PT40M'
  };

  axios.post(url, parameters, config)
    .then((response) => {
      return res.json(response.data);
    })
    .catch((error) => {console.log(error);
      error = {
        code: 'error_init_payment',
        messages: 'Une erreur est survenue lors du paiement'
      };
      return res.json({ error });
    });
};

/**
 * POST /checkout/payment/order/validate
 * Checkout page
 */
const postValidatePaymentOrder = async (req, res) => {
  const { orderId, paymentMethod } = req.body;
  // todo: add check on type and orderId
  await validatePaymentOrder(orderId, paymentMethod);

  delete req.session.shoppingCart;

  res.json({ status: 'succeeded' });
};

/**
 * GET /checkout/payment
 * Checkout page
 */
const getPayment = async (req, res) => {
  const { shoppingCart } = req.session;

  if (!shoppingCart || !shoppingCart.items || shoppingCart.items.length < 1) {
    return res.redirect('/');
  }

  const order = await Order.findById(shoppingCart.orderId);

  if (!order) {
    return res.redirect('/');
  }

  res.render(path.join(templateDir, 'checkout/payment.twig'), {
    title: 'Paiement',
    order
  });
};

const getDeliveryWindows = () => {
  const slot1 = {
    number: 1,
    beginDate: moment().set({ "hour": 11, "minute": 45}).format('YYYY-MM-DD HH:mm'),
    endDate: moment().set({ "hour": 12, "minute": 15}).format('YYYY-MM-DD HH:mm'),
  };

  const slot2 = {
    number: 2,
    beginDate: moment().set({ "hour": 12, "minute": 15}).format('YYYY-MM-DD HH:mm'),
    endDate: moment().set({ "hour": 12, "minute": 45}).format('YYYY-MM-DD HH:mm'),
  };

  const slot3 = {
    number: 3,
    beginDate: moment().set({ "hour": 12, "minute": 45}).format('YYYY-MM-DD HH:mm'),
    endDate: moment().set({ "hour": 13, "minute": 15}).format('YYYY-MM-DD HH:mm'),
  };

  const slot4 = {
    number: 4,
    beginDate: moment().set({ "hour": 13, "minute": 15}).format('YYYY-MM-DD HH:mm'),
    endDate: moment().set({ "hour": 13, "minute": 45}).format('YYYY-MM-DD HH:mm'),
  };

  return [slot1, slot2, slot3, slot4];
};

/**
 * POST /checkout/payment
 */
const postPayment = async (req, res) => {
  const validationErrors = [];

  const { selectedAddressId, deliveryWindow, credit } = req.body;

  // todo: valider adresse de livraison par à la zone de chalandise
  if (selectedAddressId === undefined || validator.isEmpty(selectedAddressId)) validationErrors.push({ msg: 'Veuillez saisir votre adresse de livraison' });
  if (deliveryWindow === undefined || validator.isEmpty(deliveryWindow)) validationErrors.push({ msg: 'Veuillez saisir un créneau de livraison' });
  if (credit && (credit < 0 || credit > req.user.jackpot.credit)) validationErrors.push({ msg: "Le montant de l'avoir à utiliser n'est pas valide" });

  if (validationErrors.length) {
    req.flash('formErrors', validationErrors);

    return res.redirect('/checkout');
  }

  const address = req.user.shippingAddresses.find((address) => String(address._id) === selectedAddressId);
  //const deliveryAuthorizedPostalCodes = process.env.DELIVERY_AUTHORIZED_POSTAL_CODES;
  //const deliveryAuthorizedPostalCodesList = deliveryAuthorizedPostalCodes.split(',');

  //if (!deliveryAuthorizedPostalCodesList.includes(String(address.postalCode))) validationErrors.push({ msg: `Désolé, nous ne livrons pas encore à cette adresse. Nos zones de livraison: ${deliveryAuthorizedPostalCodes}.`});

  /*if (validationErrors.length) {
    req.flash('formErrors', validationErrors);

    return res.redirect('/checkout');
  }*/

  const deliveryWindowDate = getDeliveryWindows().find((slot) => slot.number == deliveryWindow);

  let { shoppingCart } = req.session;

  if (!shoppingCart || !shoppingCart.items || shoppingCart.items.length < 1) {
    return res.redirect('/');
  }

  const { orderId } = shoppingCart;
  let oldOrder;
  if (orderId) {
    oldOrder = await Order.findById(orderId);
  }

  // add check on status, available : INIT

  const menuCategoryItems = [];
  let subTotalAmount = 0;
  let totalQuantity = 0;
  for (let i = 0; i < shoppingCart.items.length; i++) {
    const shoppingCartItem = shoppingCart.items[i];

    // To refactor
    // eslint-disable-next-line no-await-in-loop
    const menuCategoryItem = await MenuCategoryItem
      .findById(shoppingCartItem.menuCategoryItemId)
      .populate({
        path: 'meals.dishes.dish',
        model: Dish
      });

    const { amount, quantity } = shoppingCartItem;
    subTotalAmount += amount;
    totalQuantity += quantity;

    const items = [];

    menuCategoryItem.meals.forEach((meal) => {
      meal.dishes.forEach((dish) => {
        if (shoppingCartItem.items.includes(String(dish._id))) {
          const item = {
            id: dish.dish.id,
            title: dish.dish.title,
          };

          items.push(item);
        }
      });
    });

    const order = {
      id: shoppingCartItem.menuCategoryItemId,
      title: menuCategoryItem.title,
      quantity,
      amount,
      items
    };

    menuCategoryItems.push(order);
  }

  if (credit) {
    subTotalAmount = credit ? (subTotalAmount - parseFloat(credit)).toFixed(2) : subTotalAmount;

    if ((subTotalAmount + shoppingCart.deliveryAmount) < 0) validationErrors.push({ msg: "Le montant de l'avoir à utiliser est trop grand" });

    if (validationErrors.length) {
      req.flash('formErrors', validationErrors);

      return res.redirect('/checkout');
    }
  }


  const inputOrder = {
    id: orderId || null,
    menuCategoryItems,
    subTotalAmount,
    deliveryAmount: shoppingCart.deliveryAmount,
    discountRate: shoppingCart.discountRate,
    discountAmount: shoppingCart.discountRate ? (subTotalAmount * shoppingCart.discountRate) / 100 : 0,
    creditAmount: credit || 0,
    totalQuantity,
    deliveryInfo: {
      address: address.formattedAddress,
      additionalInformation: address.additionalInformation,
      companyName: address.companyName,
      firstName: address.firstName,
      lastName: address.lastName,
      phoneNumber: address.phoneNumber,
      instruction: address.instruction,
      window: {
        beginDate: moment(deliveryWindowDate.beginDate).format('YYYY-MM-DD HH:mm'),
        endDate: moment(deliveryWindowDate.endDate).format('YYYY-MM-DD HH:mm'),
      },
    },
    customer: req.user._id,
    couponCode: null
  };

  if (oldOrder) {
    oldOrder = await updateOrder(inputOrder);
  } else {
    const newOrder = await createOrder(inputOrder);
    shoppingCart = await linkOrderToShoppingCart(shoppingCart._id, newOrder._id);
  }

  req.session.shoppingCart = shoppingCart;

  return res.redirect('/checkout/payment');
};

const getActiveDeliveryWindows = () => {
  const activeSlots = getDeliveryWindows().filter((slot) => moment(slot.endDate).diff(moment(), 'minutes') > process.env.DELIVERY_WINDOW_DELAY);

  return activeSlots;
};

/**
 * GET /checkout
 * Checkout page
 */
const checkout = async (req, res) => {
  const { shoppingCart } = req.session;

  if (!shoppingCart || !shoppingCart.items || shoppingCart.items.length < 1) {
    return res.redirect('/');
  }

  req.session.shoppingCart = shoppingCart;

  const orders = [];
  let subTotalAmount = 0;
  for (let i = 0; i < shoppingCart.items.length; i++) {
    const shoppingCartItem = shoppingCart.items[i];

    // To refactor
    // eslint-disable-next-line no-await-in-loop
    const menuCategoryItem = await MenuCategoryItem
      .findById(shoppingCartItem.menuCategoryItemId)
      .populate({
        path: 'meals.dishes.dish',
        model: Dish
      });

    const { amount } = shoppingCartItem;
    subTotalAmount += amount;

    const items = [];

    menuCategoryItem.meals.forEach((meal) => {
      meal.dishes.forEach((dish) => {
        if (shoppingCartItem.items.includes(String(dish._id))) {
          const item = {
            title: dish.dish.title,
          };

          items.push(item);
        }
      });
    });

    const order = {
      id: shoppingCartItem._id,
      quantity: shoppingCartItem.quantity,
      title: menuCategoryItem.title,
      amount,
      items
    };

    orders.push(order);
  }

  res.render(path.join(templateDir, 'checkout/checkout.twig'), {
    title: 'Livraison',
    orders,
    deliveryAmount: shoppingCart.deliveryAmount,
    subTotalAmount,
    GCP_API_Key: process.env.GOOGLE_CLOUD_PLATFORM_API_KEY,
    deliveryWindows: getActiveDeliveryWindows(),
    discountRate: shoppingCart.discountRate,
    discountAmount: shoppingCart.discountRate ? (subTotalAmount * shoppingCart.discountRate) / 100 : 0
  });
};

/**
 * POST /checkout/update-basket-quantity
 * Update basket quantity
 */
const updateBasketQuantity = async (req, res) => {
  let { shoppingCart } = req.session;

  const { itemId, quantity } = req.body;

  const index = shoppingCart.items.findIndex((item) => item._id === itemId);

  const item = shoppingCart.items[index];

  let itemAmount = 0;
  if (quantity > 0) {
    itemAmount = (item.amount * quantity) / item.quantity;
    item.quantity = quantity;
    item.amount = itemAmount;

    shoppingCart = await addItemToShoppingCart(shoppingCart._id, item, index);
  } else {
    shoppingCart = await removeItemToShoppingCartFromIndex(shoppingCart._id, index);
  }

  let subTotalAmount = 0;
  let totalQuantity = 0;
  if (shoppingCart && shoppingCart.items && shoppingCart.items.length > 0) {
    for (let i = 0; i < shoppingCart.items.length; i++) {
      const shoppingCartItem = shoppingCart.items[i];

      const { amount } = shoppingCartItem;

      totalQuantity += shoppingCartItem.quantity;

      subTotalAmount += amount;
    }
  }

  req.session.shoppingCart = shoppingCart;

  const result = {
    newAmount: itemAmount,
    deliveryAmount: shoppingCart.deliveryAmount,
    subTotalAmount,
    totalQuantity,
    discountRate: shoppingCart.discountRate,
    discountAmount: shoppingCart.discountRate ? (subTotalAmount * shoppingCart.discountRate) / 100 : 0
  };

  return res.json(result);
};

/**
 * POST /cart/checkMenuCategoryItemQuantity
 * Check menu category item quantity
 */
const checkMenuCategoryItemQuantity = async (req, res) => {
  const { menuCategoryItemId, quantity } = req.body;

  const result = await checkQuantityByDish(menuCategoryItemId, quantity);

  return res.json(result);
};

/**
 * POST /cart/addMenuQuantity
 * Check menu category item quantity
 */
const addMenuQuantity = async (req, res) => {
  const { menuCategoryItemId, quantity, additionalPrice } = req.body;

  const menuCategoryItem = await MenuCategoryItem.findById(menuCategoryItemId);
  if (!menuCategoryItem) {
    throw new Error(`Impossible de trouver l'élément de categorie du menu avec id: ${menuCategoryItemId}`);
  }

  const price = (menuCategoryItem.price + parseInt(additionalPrice, 10)) * quantity;
  const result = {
    price
  };

  return res.json(result);
};

// todo: move to utils
const arrayMatch = (arr1, arr2) => {
  if (arr1.length !== arr2.length) {
    return false;
  }

  for (let i = 0; i < arr1.length; i++) {
    if (!arr2.includes(arr1[i])) {
      return false;
    }
  }

  return true;
};

const checkItemCart = (cart, newItem) => {
  for (let i = 0; i < cart.length; i++) {
    if (newItem.menuCategoryItemId === cart[i].menuCategoryItemId) {
      if (!newItem.items && (!cart[i].items || cart[i].items.length === 0)) {
        return {
          isNew: false, index: i, quantity: cart[i].quantity, amount: cart[i].amount
        };
      }

      if (newItem.items && cart[i].items && newItem.items.length === cart[i].items.length) {
        const isArrayMatch = arrayMatch(newItem.items, cart[i].items);

        if (isArrayMatch) {
          return {
            isNew: false, index: i, quantity: cart[i].quantity, amount: cart[i].amount
          };
        }
      }
    }
  }

  return { isNew: true };
};

/**
 * POST /cart/addToCart
 * Handle add cart
 */
const addToCart = async (req, res) => {
  const {
    menuCategoryItemId, quantity, amount, items
  } = req.body;

  const newItemCart = {
    menuCategoryItemId, quantity, amount, items
  };

  let { shoppingCart } = req.session;

  let isNewItemCart = true;
  let newItemCartIndex;
  let newItemCartQuantity;
  let newItemCartAmount;
  if (shoppingCart && shoppingCart.items && shoppingCart.items.length > 0) {
    const checkItemCartRest = checkItemCart(shoppingCart.items, newItemCart);
    isNewItemCart = checkItemCartRest.isNew;
    newItemCartIndex = checkItemCartRest.index;
    newItemCartAmount = checkItemCartRest.amount;
    newItemCartQuantity = checkItemCartRest.quantity;
  }

  if (isNewItemCart) {
    const id = shoppingCart ? shoppingCart._id : '';
    const deliveryAmount = req.user && req.user.deliveryAmount !== undefined ? req.user.deliveryAmount : process.env.DELIVERY_AMOUNT_DEFAULT;
    const discountRate = req.user && req.user.profile && req.user.profile.professionalActivity === 'STUDENT' ? process.env.DISCOUNT_RATE_STUDENT : 0;
    shoppingCart = await createShoppingCart(newItemCart, id, deliveryAmount, discountRate);
  } else {
    newItemCart.quantity = parseInt(newItemCart.quantity, 10) + parseInt(newItemCartQuantity, 10);
    newItemCart.amount = parseFloat(newItemCart.amount, 10) + parseFloat(newItemCartAmount, 10);
    shoppingCart = await addItemToShoppingCart(shoppingCart._id, newItemCart, newItemCartIndex);
  }

  req.session.shoppingCart = shoppingCart;

  return res.json(req.session.shoppingCart.items);
};

export {
  addMenuQuantity,
  addToCart,
  checkMenuCategoryItemQuantity,
  checkout,
  updateBasketQuantity,
  getPayment,
  postPayment,
  postValidatePaymentOrder,
  completeOrder,
  initPaymentOrder,
  paygreenPaymentNotify,
  paygreenPaymentReturn,
  completeOrderWithPayment,
  postCheckCouponCodeOrder
};
