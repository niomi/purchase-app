import path from 'path';
import dotenv from 'dotenv';
import mailchimp from '@mailchimp/mailchimp_marketing';
import sgMail from '@sendgrid/mail';
import {
  getMenuCategories
} from '../../../repositories/Menu/menuCategoryRepository';
import {
  getMenuItems
} from '../../../repositories/Menu/menuItemRepository';
import moment from 'moment';

dotenv.config({ path: '.env' });

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

mailchimp.setConfig({
  apiKey: process.env.MAILCHIMP_API_KEY,
  server: process.env.MAILCHIMP_SERVER_PREFIX,
});

const templateDir = 'modules/front/home/views';

/**
 * GET /
 * Home page
 */
const home = async (req, res) => {
  const menuItem = await getMenuItems({ availableAt: moment().format('YYYY-MM-DD') });

  res.render(path.join(templateDir, 'index.twig'), {
    title: 'Accueil',
    menuItem: menuItem ? menuItem[0] : null
  });
};

/**
 * POST /mailchimp/subscribers/add
 * Home page
 */
const mailchimpSubscribersAdd = async (req, res) => {
  const { email } = req.body;

  // Paliatif mailchimp qui ne fonctionne pas
  const msg = {
    to: 'as.landry@gmail.com',
    from: `NIOMI <${process.env.EMAIL_SENDER_HELLO}>`,
    subject: '[NIOMI] Inscription à la newsletter',
    html: `L'utilisateur ${email} souhaite s'incrire à la newsletter NIOMI`
  };

  sgMail.send(msg).then(() => {
    console.log('Un message de réinitialisation de mot de passe a été envoyé à as.landry@gmail.com.');
  })
  .catch((err) => {
    console.log('Une erreur est survenue durant l"envoie de mail pour ceux qui souhaitent s"inscrire à la newsletter.\n', err);
  });

  // todo: fix mailchimp
  /*await mailchimp.lists.addListMember(process.env.MAILCHIMP_MAIN_LIST_ID, {
    email_address: email,
    status: "subscribed",
    tags: [{ name: 'Visitor', status: 'active' }]
  });*/

  res.redirect('/');
};

/**
 * GET /blog
 * About us
 */
const blog = async (req, res) => {
  res.render(path.join(templateDir, 'blog/grid.twig'), {
    title: 'Blog'
  });
};

/**
 * GET /blog/conseils-alimentaires-hiver
 * About us
 */
const conseilsAlimentairesHiverArticle = async (req, res) => {
  res.render(path.join(templateDir, 'blog/articles/conseils-alimentaires-hiver.twig'), {
    title: 'Blog'
  });
};

/**
 * GET /about-us
 * About us
 */
const aboutUs = async (req, res) => {
  res.render(path.join(templateDir, 'about-us.twig'), {
    title: 'A propos'
  });
};

/**
 * GET /our-engagement
 * Our engagement
 */
const ourEngagements = async (req, res) => {
  res.render(path.join(templateDir, 'our-engagements.twig'), {
    title: 'Nos engagements'
  });
};

/**
 * GET /privacy-policy
 * Privacy policy
 */
const privacyPolicy = async (req, res) => {
  res.render(path.join(templateDir, 'privacy-policy.twig'), {
    title: 'Politique de confidentialité'
  });
};

/**
 * GET /process-stop-covid
 * Process stop covid
 */
const processStopCovid = async (req, res) => {
  res.render(path.join(templateDir, 'process-stop-covid.twig'), {
    title: 'Process NIOMI'
  });
};

export {
  home,
  aboutUs,
  blog,
  conseilsAlimentairesHiverArticle,
  ourEngagements,
  privacyPolicy,
  mailchimpSubscribersAdd,
  processStopCovid
};
