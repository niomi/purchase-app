import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import timeZone from 'mongoose-timezone';

const adminUserSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true },
  username: { type: String, unique: true, required: true },
  password: String,
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  phoneNumber: { type: String },
  job: { type: String },
  activated: {
    type: Boolean,
    default: true,
    required: [true]
  },
  isAdmin: {
    type: Boolean,
    default: true,
    required: [true]
  }
}, { timestamps: true });

/**
 * Password hash middleware.
 */
adminUserSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
adminUserSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

adminUserSchema.plugin(timeZone);

const AdminUser = mongoose.model('AdminUser', adminUserSchema);

export default AdminUser;
