import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import autoIncrement from 'mongoose-auto-increment';
import timeZone from 'mongoose-timezone';

autoIncrement.initialize(mongoose.connection);

const professionalActivities = Object.freeze({
  EMPLOYEE: 'EMPLOYEE',
  STUDENT: 'STUDENT',
  OTHERS: 'OTHERS'
});

const userSchema = new mongoose.Schema({
  reference: { type: Number, default: 0 },
  email: { type: String, unique: true },
  phoneNumber: { type: String },
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,
  emailVerificationToken: String,
  emailVerified: Boolean,

  facebook: String,
  google: String,
  tokens: Array,

  profile: {
    firstName: { type: String, required: true },
    lastName: { type: String },
    gender: String,
    professionalActivity: {
      type: String,
      enum: Object.values(professionalActivities),
      required: false
    }
  },

  shippingAddresses: [{
    formattedAddress: { type: String, required: true },
    streetNumber: { type: Number },
    route: { type: String },
    locality: { type: String },
    administrativeArea: { type: String },
    postalCode: { type: Number },
    country: { type: String },
    additionalInformation: { type: String },
    companyName: { type: String },
    firstName: { type: String, required: true },
    lastName: { type: String },
    alias: { type: String },
    instruction: String,
    phoneNumber: { type: String, required: true },
    lastUsedAt: { type: Date, default: Date.now },
    deleted: { type: Boolean, default: false }
  }],
  jackpot: {
    credit: { type: Number, default: 0 }
  },
  deliveryAmount: { type: Number }
}, { timestamps: true });

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

/**
 * Configure autoincrement fields
 */
userSchema.plugin(autoIncrement.plugin, {
  model: 'User',
  field: 'reference',
  startAt: 1,
  incrementBy: 1
});

userSchema.plugin(timeZone);

const User = mongoose.model('User', userSchema);

export default User;
