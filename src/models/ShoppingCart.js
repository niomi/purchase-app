import mongoose from 'mongoose';
import timeZone from 'mongoose-timezone';

const statutes = Object.freeze({
  IN_PROGRESS: 'IN_PROGRESS',
  ORDERED: 'ORDERED',
  CANCELED: 'CANCELED',
});

const shoppingCartSchema = new mongoose.Schema({
  items: [
    {
      menuCategoryItemId: { type: String, required: true },
      quantity: { type: Number },
      amount: { type: Number },
      items: [{
        type: String
      }]
    }
  ],
  status: {
    type: String,
    enum: Object.values(statutes),
    default: 'IN_PROGRESS',
    required: true
  },
  deliveryAmount: { type: Number, default: 1.5 },
  discountRate: { type: Number },
  orderId: { type: String },
}, { timestamps: true });

shoppingCartSchema.plugin(timeZone);

const ShoppingCartSchema = mongoose.model('ShoppingCart', shoppingCartSchema);

export default ShoppingCartSchema;
