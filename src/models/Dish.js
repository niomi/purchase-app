import mongoose from 'mongoose';
import timeZone from 'mongoose-timezone';

const types = Object.freeze({
  STARTER: 'STARTER',
  MAIN_COURSE: 'MAIN_COURSE',
  DESSERT: 'DESSERT',
  DRINK: 'DRINK',
  OTHERS: 'OTHERS'
});

const dishSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String },
  photo: { type: String },
  ingredients: { type: String },
  quantity: { type: Number },
  allergens: { type: String },
  type: {
    type: String,
    enum: Object.values(types),
    default: 'MAIN_COURSE',
    required: true
  }
}, { timestamps: true });

dishSchema.plugin(timeZone);

const DishSchema = mongoose.model('Dish', dishSchema);

export default DishSchema;
