import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import timeZone from 'mongoose-timezone';

autoIncrement.initialize(mongoose.connection);

// Create a payment model
const statutes = Object.freeze({
  INIT: 'INIT',
  PROCESSING: 'PROCESSING',
  DELIVERED: 'DELIVERED',
  CANCELLED: 'CANCELLED',
  FAILED: 'FAILED',
  REFUSED: 'REFUSED',
});

const providers = Object.freeze({
  PAYGREEN: 'PAYGREEN',
  EDENRED: 'EDENRED'
});

const pickUpStatutes = Object.freeze({
  AWAITING_PICKUP_ASSIGNMENT: 'AWAITING_PICKUP_ASSIGNMENT',
  PICKUP_ASSIGNED: 'PICKUP_ASSIGNED',
  DELIVERED: 'DELIVERED',
});

const paymentStatutes = Object.freeze({
  WAITING_PAYMENT: 'WAITING_PAYMENT',
  PAID: 'PAID',
  FAILED: 'FAILED',
  CANCELLED: 'CANCELLED',
  PARTIALLY_REFUNDED: 'PARTIALLY_REFUNDED',
  REFUNDED: 'REFUNDED',
});

const paymentMethods = Object.freeze({
  CB: 'CB',
  TRD: 'TRD', // Titres Restaurant Dématérialisés
  RESTOFLASH: 'RESTOFLASH',
  LUNCHR: 'LUNCHR',
  CASH: 'CASH',
  TRP: 'TRP', // Titres Restaurant papier
  PAYPAL: 'PAYPAL',
  LYDIA: 'LYDIA',
  SWILE_TPE: 'SWILE_TPE',
  EDENRED_TPE: 'EDENRED_TPE',
});

const orderSchema = new mongoose.Schema({
  menuCategoryItems: [
    {
      id: { type: String },
      quantity: { type: Number },
      title: { type: String },
      amount: { type: Number },
      items: [{
        id: { type: String },
        title: { type: String },
      }]
    }
  ],
  subTotalAmount: { type: Number },
  deliveryAmount: { type: Number },
  creditAmount: { type: Number },
  discountRate: { type: Number },
  discountAmount: { type: Number },
  vat: { type: Number, default: 20 },
  totalQuantity: { type: Number },
  status: {
    type: String,
    enum: Object.values(statutes),
    default: 'INIT',
    required: true
  },
  paymentStatus: {
    type: String,
    enum: Object.values(paymentStatutes),
    default: 'WAITING_PAYMENT',
    required: true
  },
  payment: {
    method: {
      type: String,
      enum: Object.values(paymentMethods),
    },
    data: mongoose.Schema.Types.Mixed,
    provider: {
      type: String,
      enum: Object.values(providers)
    },
    purchaseAt: Date
  },
  couponCode: {
    type: String,
  },
  pickUpAssignment: {
    to: String,
    at: Date,
    confirmedAt: Date,
    deliveredAt: Date,
    status: {
      type: String,
      enum: Object.values(pickUpStatutes),
      default: 'AWAITING_PICKUP_ASSIGNMENT',
      required: true
    },
  },
  deliveryInfo: {
    address: String,
    additionalInformation: String,
    companyName: { type: String },
    firstName: { type: String, required: true },
    lastName: { type: String },
    phoneNumber: { type: String, required: true },
    instruction: String,
    window: {
      beginDate: Date,
      endDate: Date
    },
  },
  customer: { type: mongoose.Types.ObjectId, ref: 'User', required: true },
}, { timestamps: true });

orderSchema.plugin(autoIncrement.plugin, 'Order', {
  startAt: 1,
  incrementBy: 1
});

orderSchema.plugin(timeZone);

const OrderSchema = mongoose.model('Order', orderSchema);

export default OrderSchema;
