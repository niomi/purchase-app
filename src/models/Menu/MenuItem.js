import mongoose from 'mongoose';
import timeZone from 'mongoose-timezone';

const menuItemSchema = new mongoose.Schema({
  availableAt: { type: Date, required: true, unique: true },
  items: [
    {
      category: { type: mongoose.Types.ObjectId, ref: 'MenuCategory' },
      order: { type: Number },
      menuCategoryItems: [
        {
          item: { type: mongoose.Types.ObjectId, ref: 'MenuCategoryItem' },
          order: { type: Number }
        }
      ],
    }
  ]
}, { timestamps: true });

menuItemSchema.plugin(timeZone);

const MenuItem = mongoose.model('MenuItem', menuItemSchema);

export default MenuItem;
