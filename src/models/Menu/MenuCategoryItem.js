import mongoose from 'mongoose';
import timeZone from 'mongoose-timezone';

const types = Object.freeze({
  SIMPLE: 'SIMPLE',
  COMBINED: 'COMBINED'
});

const linkTypes = Object.freeze({
  EXCLUSIVE: 'EXCLUSIVE',
  INCLUSIVE: 'INCLUSIVE',
  UNIQUE: 'UNIQUE'
});

const menuCategoryItemSchema = new mongoose.Schema({
  title: { type: String },
  description: { type: String },
  photo: { type: String },
  price: { type: Number },
  isActive: {
    type: Boolean,
    default: true,
    required: [true]
  },
  meals: [
    {
      title: { type: String },
      order: { type: Number },
      link: {
        type: String,
        enum: Object.values(linkTypes),
        default: 'UNIQUE',
        required: true
      },
      dishes: [
        {
          dish: { type: mongoose.Types.ObjectId, ref: 'Dish' },
          additionalPrice: { type: Number }
        }
      ]
    }
  ],
  type: {
    type: String,
    enum: Object.values(types),
    default: 'SIMPLE',
    required: true
  }
}, { timestamps: true });

menuCategoryItemSchema.plugin(timeZone);

const MenuCategoryItemSchema = mongoose.model('MenuCategoryItem', menuCategoryItemSchema);

export default MenuCategoryItemSchema;
