import mongoose from 'mongoose';
import timeZone from 'mongoose-timezone';

const menuCategorySchema = new mongoose.Schema({
  title: { type: String },
  subtitle: { type: String },
  photo: { type: String },
  order: { type: Number }, // Todo: à supprimer
  activated: {
    type: Boolean,
    default: true,
    required: [true]
  },
  items: [ // à déplacer
    {
      item: { type: mongoose.Types.ObjectId, ref: 'MenuCategoryItem' },
      order: { type: Number }
    }
  ]
}, { timestamps: true });

menuCategorySchema.plugin(timeZone);

const MenuCategory = mongoose.model('MenuCategory', menuCategorySchema);

export default MenuCategory;
