import mongoose from 'mongoose';
import dotenv from 'dotenv';
import {
  createDish, findDishByConditions
} from '../repositories/dishRepository';

import {
  createMenuCategory, addMenuCategoryItem, findMenuCategoryByConditions
} from '../repositories/Menu/menuCategoryRepository';

import {
  createMenuCategoryItem, findMenuCategoryItemByConditions, addMeal
} from '../repositories/Menu/menuCategoryItemRepository';

dotenv.config({ path: '.env' });
/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.');
  process.exit();
});

// DISH
/* 
createDish({
  title: 'Thieb au poisson',
  description: 'Riz cuisiné dans la sauce tomate avec des légumes, poisson.',
  photo: 'img/thieb_poisson.jpeg',
  ingredients: 'Riz, Gombo, Sel, Ail, Poivre, Guedj, Yet, Darnes de mérou',
  allergens: 'Gluten',
  type: 'MAIN_COURSE'
});

createDish({
  title: 'Yassa Bowl',
  description: 'Riz blanc, sauce yassa et poêlé de légumes et poulet',
  photo: 'img/yassa.jpeg',
  ingredients: 'Riz, Oignon, Moutarde, Citron, Poivron, Sel, Ail, Poivre',
  allergens: 'Gluten',
  type: 'MAIN_COURSE'
});

createDish({
  title: 'Alloco + Brochettes de poulet',
  description: 'Bananes plantains frites',
  photo: 'img/alloco_brochettes.png',
  ingredients: 'Riz, Oignon, Huile, Persil, Sel, Ail, Poivre',
  type: 'MAIN_COURSE'
});

createDish({
  title: 'Pastels',
  description: 'Description pastels',
  photo: '',
  ingredients: 'Farine de blé, Oeuf',
  type: 'STARTER'
});

createDish({
  title: 'Accras',
  description: 'Description accras',
  photo: '',
  ingredients: 'Farine de blé, Morue',
  allergens: 'Poisson',
  type: 'STARTER'
});

createDish({
  title: 'Thiakry',
  description: 'Thiakry',
  photo: 'img/tiakry.jpg',
  ingredients: 'Mil, Sucre, Lait',
  allergens: 'Lait',
  type: 'DESSERT'
});

createDish({
  title: 'Bissap rouge',
  description: 'Jus frais',
  photo: 'img/jus_bissap_rouge.jpeg',
  ingredients: 'Hibiscus rouge, Sucre',
  type: 'DRINK'
});

createDish({
  title: 'Bissap blanc',
  description: 'Jus frais',
  photo: 'img/jus_bissap_blanc.jpeg',
  ingredients: 'Hibiscus blanc, Sucre',
  type: 'DRINK'
});

createDish({
  title: 'Alloco',
  description: 'Bananes plantains frites',
  photo: 'img/allocos.jpeg',
  ingredients: 'Bananes plantains, Sel',
  type: 'EXTRA'
});

createDish({
  title: 'Sauce piquante',
  description: 'Piment',
  photo: '',
  ingredients: 'Piment',
  type: 'SPICE'
});

createDish({
  title: 'Crumble Mbourake',
  description: 'Crumble Mbourake',
  photo: '',
  ingredients: 'Pommes, Sucre',
  allergens: 'Lait',
  type: 'DESSERT'
});
*/

// MENU CATEGORY
/*
createMenuCategory({
  title: 'Formules',
  order: 1
});
createMenuCategory({
  title: 'Plats',
  order: 2
});
createMenuCategory({
  title: 'Entrées',
  order: 3
});
createMenuCategory({
  title: 'Desserts',
  order: 4
});
createMenuCategory({
  title: 'Boissons',
  order: 5
});
createMenuCategory({
  title: 'Suppléments',
  order: 6
});
*/

// MENU CATEGORY ITEM
/*
createMenuCategoryItem({
  title: 'Formule gourmande',
  description: '1 Entrée au choix + 1 plat au choix + 1 dessert au choix',
  price: 12,
  type: 'COMBINED'
});

createMenuCategoryItem({
  title: 'Formule saveur',
  description: '1 Entrée au choix + 1 plat au choix',
  price: 10.5,
  type: 'COMBINED'
});

createMenuCategoryItem({
  title: 'Formule délice',
  description: '1 plat au choix + 1 dessert au choix',
  price: 10.5,
  type: 'COMBINED'
});

createMenuCategoryItem({
  title: 'Thieb au poisson',
  description: 'Riz cuisiné dans la sauce tomate avec des légumes, poisson.',
  price: 9.5,
  type: 'SIMPLE'
});
createMenuCategoryItem({
  title: 'Yassa bowl',
  description: 'Riz blanc, légumes',
  price: 8.5,
  type: 'SIMPLE'
});

createMenuCategoryItem({
  title: 'Alloco + Brochettes',
  description: 'Bananes de plantain frite accompagnés de tendres morceaux de poulet',
  price: 8.5,
  type: 'SIMPLE'
});
*/

// RELATION MENU CATEGORY ET MENU CATEGORIE ITEM
/*
const formules = findMenuCategoryByConditions({ title: 'Formules' });
formules.then(async (formules) => {
  const formuleGourmande = await findMenuCategoryItemByConditions({ title: 'Formule gourmande' });
  const formuleSaveur = await findMenuCategoryItemByConditions({ title: 'Formule saveur' });
  const formuleDelice = await findMenuCategoryItemByConditions({ title: 'Formule délice' });

  addMenuCategoryItem(formules._id, formuleGourmande._id, 1);
  addMenuCategoryItem(formules._id, formuleSaveur._id, 2);
  addMenuCategoryItem(formules._id, formuleDelice._id, 3);
});
*/

/*
const plats = findMenuCategoryByConditions({ title: 'Plats' });
plats.then(async (plats) => {
  const thiebAuPoisson = await findMenuCategoryItemByConditions({ title: 'Thieb au poisson' });
  const yassaBowl = await findMenuCategoryItemByConditions({ title: 'Yassa bowl' });
  const allocoBrochettes = await findMenuCategoryItemByConditions({ title: 'Alloco + Brochettes' });

  addMenuCategoryItem(plats._id, thiebAuPoisson._id, 1);
  addMenuCategoryItem(plats._id, yassaBowl._id, 2);
  addMenuCategoryItem(plats._id, allocoBrochettes._id, 3);
});
*/
/*
const formuleGourmande = findMenuCategoryItemByConditions({ title: 'Formule gourmande' });
formuleGourmande.then(async (formuleGourmande) => {
  const pastels = await findDishByConditions({ title: 'Pastels' });
  const acrras = await findDishByConditions({ title: 'Accras' });

  const meal = {
    title: 'Entrée au choix',
    order: 1,
    link: 'EXCLUSIVE',
    dishes: [
      {
        dish: pastels._id,
        additionalPrice: 0
      },
      {
        dish: acrras._id,
        additionalPrice: 0
      }
    ]
  };

  addMeal(formuleGourmande._id, meal);
});

const formuleGourmande = findMenuCategoryItemByConditions({ title: 'Formule gourmande' });
formuleGourmande.then(async (formuleGourmande) => {
  const thiebAuPoisson = await findDishByConditions({ title: 'Thieb au poisson' });
  const yassaBowl = await findDishByConditions({ title: 'Yassa Bowl' });

  const dishMeal = {
    title: 'Plat au choix',
    order: 2,
    link: 'EXCLUSIVE',
    dishes: [
      {
        dish: thiebAuPoisson._id,
        additionalPrice: 1
      },
      {
        dish: yassaBowl._id,
        additionalPrice: 0
      }
    ]
  };

  const crumble = await findDishByConditions({ title: 'Crumble Mbourake' });
  const thiakry = await findDishByConditions({ title: 'Thiakry' });

  const dessertMeal = {
    title: 'Dessert au choix',
    order: 3,
    link: 'EXCLUSIVE',
    dishes: [
      {
        dish: crumble._id,
        additionalPrice: 0
      },
      {
        dish: thiakry._id,
        additionalPrice: 0
      }
    ]
  };

  const saucePiquante = await findDishByConditions({ title: 'Sauce piquante' });

  const spiceMeal = {
    title: 'Sauce piquante',
    order: 4,
    link: 'INCLUSIVE',
    dishes: [
      {
        dish: saucePiquante._id,
        additionalPrice: 0
      }
    ]
  };

  addMeal(formuleGourmande._id, dishMeal);
  addMeal(formuleGourmande._id, dessertMeal);
  addMeal(formuleGourmande._id, spiceMeal);
});

const thiebAuPoisson = findMenuCategoryItemByConditions({ title: 'Thieb au poisson' });
thiebAuPoisson.then(async (thiebAuPoisson) => {
  const thiebAuPoissonDish = await findDishByConditions({ title: 'Thieb au poisson' });
  console.log(thiebAuPoisson, thiebAuPoissonDish);
  const meal = {
    link: 'UNIQUE',
    dishes: [
      {
        dish: thiebAuPoissonDish._id,
        additionalPrice: 0
      }
    ]
  };

  addMeal(thiebAuPoisson._id, meal);
});
*/
