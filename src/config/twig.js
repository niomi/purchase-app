import twig from 'twig';

const twigConfig = () => {
  twig.extendFilter('sortByField', (value, params) => {
    return value.sort((a, b) => a[params[0]] - b[params[0]]);
  });
};

export default twigConfig;
